from __future__ import annotations

import asyncio
import os
import socket
import stat
import textwrap
from typing import (
    TYPE_CHECKING,
    Tuple,
)

import paramiko

from . import prepend_environment_to_command

if TYPE_CHECKING:
    from credentials import Secret


# TODO: Use in virtual machines and hypervisors

async def execute_local_script_on_remote(
        localFilePath: str,
        remoteFilePath: str,
        sshHandler: SshHandler,
        sftpHandler: SftpHandler,
        environment: dict = None,
        environmentQuoteTypes: dict = None,
) -> Tuple[str, str]:

    if environment is None:
        environment = {}

    # Transfer script to remote server
    with sftpHandler as sftp:
        sftp.put(
            localpath=localFilePath,
            remotepath=remoteFilePath,
        )
        # stat.S_IRWXU − Read, write, and execute by owner.
        # stat.S_IWGRP − Write by group.
        # stat.S_IRWXG − Read, write, and execute by group.
        # stat.S_IWOTH − Write by others.
        # stat.S_IRWXO − Read, write, and execute by others.
        sftp.chmod(
            remoteFilePath,
            stat.S_IRWXU | stat.S_IWGRP | stat.S_IWOTH,
        )

    # We manually declare environment variables here
    # because passing the environment to exec_command()
    # below won't always work because the SSH server does
    # not necessarily accept environment variables. See
    # AcceptEnv setting in the server's SSH config file.
    command = f'{remoteFilePath}'
    command = prepend_environment_to_command(command, environment, environmentQuoteTypes)
    with sshHandler as ssh:
        stdin, stdout, stderr = ssh.exec_command(command)
        while not stdout.channel.exit_status_ready():
            await asyncio.sleep(0.5)
        output = str(stdout.read(), encoding='UTF-8')
        errorOutput = str(stderr.read(), encoding='UTF-8')
        if stdout.channel.exit_status != 0:
            MSG = (
                f'The following command has failed with '
                f'status {stdout.channel.exit_status} on {sshHandler.ipAddress}:\n'
                f'{textwrap.indent(command, prefix=" " * 4)}\n'
                f'stdout:\n'
                f'{textwrap.indent(output, prefix=" " * 4)}\n'
                f'stderr:\n'
                f'{textwrap.indent(errorOutput, prefix=" " * 4)}'
            )
            raise RemoteScriptExecutionError(MSG)
        return output, errorOutput


class RemoteScriptExecutionError(Exception):
    pass


class SshHandler:

    def __init__(
            self,
            ipAddress: str,
            username: str,
            secret: Secret = None,
            privateKeySecret: Secret = None,
    ):
        self.ipAddress = ipAddress
        self.username = username
        self.secret = secret
        self.privateKeySecret = privateKeySecret
        self._sshClient = None

    def __enter__(self) -> paramiko.SSHClient:
        self._sshClient = paramiko.SSHClient()
        self._sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            if self.secret is None:
                raise paramiko.BadAuthenticationType('', '')
            assert self.secret.userName == self.username
            self._sshClient.connect(
                self.ipAddress,
                username=self.username,
                password=self.secret.value,
            )
        except paramiko.BadAuthenticationType:
            self._sshClient.connect(
                self.ipAddress,
                username=self.username,
            )
        except paramiko.PasswordRequiredException as e:
            if 'PKEY_SERVICE_NAME' not in os.environ \
                    or 'PKEY_USER_NAME' not in os.environ:
                raise EnvironmentError(
                    'Missing PKEY_SERVICE_NAME and/or PKEY_USER_NAME.')
            if self.privateKeySecret is None:
                raise paramiko.PasswordRequiredException(
                    f'Need to provide private key '
                    f'passphrase for host {socket.gethostname()}.'
                ) from e
            self._sshClient.connect(
                self.ipAddress,
                username=self.username,
                passphrase=self.privateKeySecret.value,
            )
        return self._sshClient

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._sshClient.close()
        self._sshClient = None


class SftpHandler:

    def __init__(self, sshHandler: SshHandler):
        self.sshHandler = sshHandler
        self._sftpClient = None
        self._sshClient = None

    def __enter__(self) -> paramiko.SFTPClient:
        self._sshClient = self.sshHandler.__enter__()
        self._sftpClient = self._sshClient.open_sftp()
        return self._sftpClient

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._sftpClient.close()
        self._sftpClient = None
        self.sshHandler.__exit__(exc_type, exc_val, exc_tb)
        self._sshClient = None
