from ._regexp_patterns import (
    EMAIL_PATTERN,
    OPEN_SSH_PUBLIC_KEY_PATTERN,
)
from ._retrying import (
    async_retry,
    retry,
)
from ._yaml_schema_validation import YamlSchemaValidator


def prepend_environment_to_command(
        command: str,
        environment: dict,
        environmentQuoteTypes: dict = None,
):

    if not environment:
        return command

    if environmentQuoteTypes is None:
        environmentQuoteTypes = {}

    # Cast values to strings
    for key, value in environment.copy().items():
        environment[key] = str(value)

    assert all('"' not in value for name, value in environment.items())

    assert all(
        quoteType in ['single', 'double']
        for quoteType in environmentQuoteTypes.values()
    )

    environmentStrings = []
    for name, value in environment.items():
        try:
            if environmentQuoteTypes[name] == 'single':
                environmentStrings.append(f'export {name}=\'{value}\'')
            elif environmentQuoteTypes[name] == 'double':
                environmentStrings.append(f'export {name}="{value}"')
        except KeyError:
            environmentStrings.append(f'export {name}="{value}"')

    environmentString = ' && '.join(environmentStrings)

    return ' && '.join([environmentString, command])
