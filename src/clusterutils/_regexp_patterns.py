import re

OPEN_SSH_PUBLIC_KEY_PATTERN = re.compile(r'ssh-rsa AAAA[0-9A-Za-z+/]+[=]{0,3} ([^@]+@[^@]+)')

EMAIL_PATTERN = re.compile(
    r'''
    ^
    [a-zA-Z0-9_.\-]*?
    @
    [a-zA-Z0-9_.\-]*?
    \.
    [a-zA-Z]*?
    $
    ''', re.VERBOSE
)

MAC_ADDRESS_PATTERN = re.compile(
    r'''
    (?:[A-Fa-f0-9]{2}:){5}
    (?:[A-Fa-f0-9]{2})
    ''', re.VERBOSE
)
