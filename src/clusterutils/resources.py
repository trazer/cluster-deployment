from __future__ import annotations

from abc import (
    ABC,
    abstractmethod,
)
from collections import MutableMapping
from operator import attrgetter
from typing import (
    List,
    Tuple,
    Type,
    Union,
)


class Resources(ABC, MutableMapping):

    RESOURCE_KEY_VALIDATORS: List[ResourceKeyValidator] = []

    KEY_NAME: Tuple[str] = ()

    def __init__(
            self,
            parentResource: Union[Type[Resource], None],
    ):
        self.parentResource = parentResource
        self.__resources = {}

    def __getitem__(self, key):
        return self.__resources[key]

    def __setitem__(self, key, resource):

        assert key is not None

        assert all(
            isinstance(resource_, type(resource))
            for resource_ in resource.resources.values()
        )

        if self.__does_resource_exist(resource):
            raise DuplicatedResourceError(key)

        if key != attrgetter(*self.KEY_NAME)(resource):
            raise KeyError(key)

        self.__resources[key] = resource

    def __does_resource_exist(self, resource):

        for resourceKeyValidator in self.RESOURCE_KEY_VALIDATORS:

            # Make sure attributes exist
            try:
                _ = resourceKeyValidator.get_attribute_values(resource)
            except AttributeError as e:
                raise AttributeError(
                    f'"{resourceKeyValidator.attributeNames}" '
                    f'not in "{resource}".'
                ) from e

            # Check if current key does not already exist
            keyAsTuple = resourceKeyValidator.get_key_as_tuple(resource)
            keyAsTuples = resourceKeyValidator.get_key_as_tuples(self)
            if keyAsTuple in keyAsTuples:
                msg = (
                    f'{resourceKeyValidator.get_key_as_tuple(resource)} '
                    f'already exists for {resource}.'
                )
                raise DuplicatedResourceError(msg)

    def __delitem__(self, key):
        del self.__resources[key]

    def values(self):
        yield from self.__resources.values()

    def __iter__(self):
        return iter(self.__resources)

    def __len__(self):
        return len(self.__resources)

    @abstractmethod
    def __str__(self):
        pass


class Resource(ABC):

    def __init__(
            self,
            parentResource: Union[Type[Resource], None],
            resources: Type[Resources],
    ):
        self.parentResource = parentResource
        self.resources = resources

    @abstractmethod
    def __str__(self):
        pass


class ResourceKeyValidator:

    def __init__(
            self,
            resourceIterableTree: str,
            attributeNames: Tuple[str, ...],
    ):
        self.resourceIterableTree: List[str] = resourceIterableTree.split('.')
        self.attributeNames = attributeNames

    def get_attribute_values(self, resource: Resource) -> list:
        """
        :raises AttributeError any attribute name does not exist in resource
        """
        return [
            attrgetter(attributeName)(resource)
            for attributeName in self.attributeNames
        ]

    def get_key_as_tuple(self, resource: Resource) -> tuple:
        return attrgetter(*self.attributeNames)(resource)

    def get_key_as_tuples(self, resources: Resources) -> List[tuple]:
        keys = []
        for resource in self._recursive_resources_iterator(resources, self.resourceIterableTree):
            keys.append(self.get_key_as_tuple(resource))
        return keys

    @classmethod
    def _recursive_resources_iterator(cls, obj, resourceIterableTree):

        if len(resourceIterableTree) == 0:
            yield obj
            return
        else:
            resourceIterable = resourceIterableTree[0]

        if resourceIterable.endswith('()'):
            for childObj in getattr(obj, resourceIterable.rstrip('()'))():
                yield from cls._recursive_resources_iterator(childObj, resourceIterableTree[1:])
        else:
            childObj = getattr(obj, resourceIterable)
            yield from cls._recursive_resources_iterator(childObj, resourceIterableTree[1:])


class DuplicatedResourceError(Exception):
    pass
