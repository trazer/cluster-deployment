import os
import sys
from tempfile import TemporaryDirectory
from typing import List

from pykwalify.core import Core
from ruamel.yaml import (
    round_trip_dump,
    round_trip_load,
)
from ruamel.yaml.comments import (
    CommentedMap,
    CommentedSeq,
)


class YamlSchemaValidator(Core):

    def __init__(
            self,
            *args,
            source_file: str = None,
            schema_files: List[str] = None,
            prefixToIgnore='.',
            **kwargs,
    ):

        self.__prefixToIgnore = prefixToIgnore

        with TemporaryDirectory() as temporaryFolderPath:

            if source_file is not None:
                resolvedSourceFile = self.__resolve_yaml_file(temporaryFolderPath, source_file)
            else:
                resolvedSourceFile = None

            if schema_files is not None:
                resolvedSchemaFiles = []
                for schema_file in schema_files:
                    resolvedSchemaFile = self.__resolve_yaml_file(temporaryFolderPath, schema_file)
                    resolvedSchemaFiles.append(resolvedSchemaFile)
            else:
                resolvedSchemaFiles = None

            super().__init__(
                *args,
                source_file=resolvedSourceFile,
                schema_files=resolvedSchemaFiles,
                **kwargs,
            )

    def __resolve_yaml_file(self, temporaryFolderPath: str, filePath: str) -> str:

        # From Python 3.7, dict preserves order. We could use
        # OrderedDict, but ruamel.yaml put !!odict tags everywhere
        # when dumping to yaml and openAPI code generator doesn't like it.
        if not (sys.version_info[0] == 3 and sys.version_info[1] == 7):
            raise RuntimeError('Only works with Python 3.7+.')

        with open(filePath, 'rt') as file:

            resolvedYaml = round_trip_load(file)
            assert isinstance(resolvedYaml, dict)
            resolvedYaml = _to_ordered_dict(resolvedYaml)

            # Only top-level key beginning with
            # "prefixToIgnore" are ignored
            for key in resolvedYaml.copy().keys():
                if key.startswith(self.__prefixToIgnore):
                    del resolvedYaml[key]

            # Build resolved file path
            fileName = os.path.split(filePath)[1]
            resolvedYamlFilePath = os.path.join(
                temporaryFolderPath,
                fileName,
            )

            with open(resolvedYamlFilePath, 'wt') as resolvedFile:
                round_trip_dump(resolvedYaml, stream=resolvedFile, tags=False)

        return resolvedYamlFilePath


def _to_ordered_dict(resolvedYaml):
    assert isinstance(resolvedYaml, CommentedMap)
    convertedResolvedYaml = dict()
    _recursive_build_ordered_dict(resolvedYaml, convertedResolvedYaml)
    return convertedResolvedYaml


def _recursive_build_ordered_dict(mapOrSeq, convertedResolvedYaml):

    if not isinstance(mapOrSeq, (CommentedSeq, CommentedMap)):
        return

    if isinstance(mapOrSeq, CommentedMap):
        for key, value in mapOrSeq.items():
            if isinstance(value, CommentedSeq):
                convertedResolvedYaml[key] = []
            elif isinstance(value, CommentedMap):
                convertedResolvedYaml[key] = dict()
            else:
                convertedResolvedYaml[key] = value
            _recursive_build_ordered_dict(value, convertedResolvedYaml[key])

    elif isinstance(mapOrSeq, CommentedSeq):
        for item in mapOrSeq:
            convertedResolvedYaml.append(item)
            if isinstance(item, CommentedSeq):
                convertedResolvedYaml[-1] = []
            elif isinstance(item, CommentedMap):
                convertedResolvedYaml[-1] = dict()
            _recursive_build_ordered_dict(item, convertedResolvedYaml[-1])
