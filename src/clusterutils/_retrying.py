import asyncio
import random
import time
from typing import (
    Callable,
    List,
    Type,
    Union,
)

# TODO: Find a way to remove code duplication

_MAXIMUM_NUMBER_OF_RETRIES_DEFAULT = 10
_MINIMUM_BACKOFF_TIME_DEFAULT = 500
_MAXIMUM_BACKOFF_TIME_DEFAULT = 5000
_BACKOFF_TIME_INCREMENT_DEFAULT = 1000
_EXCEPTIONS_DEFAULT = None
_RETRY_CALLBACK_MESSAGE_DEFAULT = None


def async_retry(
        *,
        maximumNumberOfRetries: int = _MAXIMUM_NUMBER_OF_RETRIES_DEFAULT,
        minimumBackoffTime: int = _MINIMUM_BACKOFF_TIME_DEFAULT,
        maximumBackoffTime: int = _MAXIMUM_BACKOFF_TIME_DEFAULT,
        backoffTimeIncrement: int = _BACKOFF_TIME_INCREMENT_DEFAULT,
        exceptions: Union[List[Type[Exception]], List[Exception], None] = _EXCEPTIONS_DEFAULT,
        retryCallbackMessage: Union[Callable, None] = _RETRY_CALLBACK_MESSAGE_DEFAULT,
):
    """
    :param maximumNumberOfRetries: Maximum number of retries
    :param minimumBackoffTime: Minimum (initial) backoff time in milliseconds
    :param maximumBackoffTime: Maximum backoff time in milliseconds
    :param backoffTimeIncrement: Maximum backoff time increment in milliseconds
    :param exceptions: A list of exceptions or None
    :param retryCallbackMessage: A callback function to print log when retrying
    """

    minimumBackoffTime: float = minimumBackoffTime / 1000
    maximumBackoffTime: float = maximumBackoffTime / 1000
    backoffTimeIncrement: float = backoffTimeIncrement / 1000

    def decorator(func):

        async def wrapper(*args, **kwargs):

            if exceptions is None:
                return await func(*args, **kwargs)

            waitTime = minimumBackoffTime
            numberOfRetries = 1
            while True:
                try:
                    outputs = await func(*args, **kwargs)
                except tuple(exceptions) as exception:
                    if numberOfRetries > maximumNumberOfRetries:
                        raise MaximumNumberOfRetriesExceededError() from exception
                    waitTime = _get_wait_time(
                        waitTime,
                        backoffTimeIncrement,
                        maximumBackoffTime,
                    )
                    _process_retry_callback_message(
                        retryCallbackMessage,
                        numberOfRetries,
                        maximumNumberOfRetries,
                        waitTime
                    )
                    await asyncio.sleep(waitTime)
                    numberOfRetries += 1
                else:
                    return outputs

        return wrapper

    return decorator


def retry(
        *,
        maximumNumberOfRetries: int = _MAXIMUM_NUMBER_OF_RETRIES_DEFAULT,
        minimumBackoffTime: int = _MINIMUM_BACKOFF_TIME_DEFAULT,
        maximumBackoffTime: int = _MAXIMUM_BACKOFF_TIME_DEFAULT,
        backoffTimeIncrement: int = _BACKOFF_TIME_INCREMENT_DEFAULT,
        exceptions: Union[List[Type[Exception]], List[Exception], None] = _EXCEPTIONS_DEFAULT,
        retryCallbackMessage: Union[Callable, None] = _RETRY_CALLBACK_MESSAGE_DEFAULT,
):
    """
    :param maximumNumberOfRetries: Maximum number of retries
    :param minimumBackoffTime: Minimum (initial) backoff time in milliseconds
    :param maximumBackoffTime: Maximum backoff time in milliseconds
    :param backoffTimeIncrement: Maximum backoff time increment in milliseconds
    :param exceptions: A list of exceptions or None
    :param retryCallbackMessage: A callback function to print log when retrying
    """

    minimumBackoffTime: float = minimumBackoffTime / 1000
    maximumBackoffTime: float = maximumBackoffTime / 1000
    backoffTimeIncrement: float = backoffTimeIncrement / 1000

    def decorator(func):

        def wrapper(*args, **kwargs):

            if exceptions is None:
                return func(*args, **kwargs)

            waitTime = minimumBackoffTime
            numberOfRetries = 1
            while True:
                try:
                    outputs = func(*args, **kwargs)
                except tuple(exceptions) as exception:
                    if numberOfRetries > maximumNumberOfRetries:
                        raise MaximumNumberOfRetriesExceededError() from exception
                    waitTime = _get_wait_time(
                        waitTime,
                        backoffTimeIncrement,
                        maximumBackoffTime,
                    )
                    _process_retry_callback_message(
                        retryCallbackMessage,
                        numberOfRetries,
                        maximumNumberOfRetries,
                        waitTime
                    )
                    time.sleep(waitTime)
                    numberOfRetries += 1
                else:
                    return outputs

        return wrapper

    return decorator


def _process_retry_callback_message(
        retryCallbackMessage: Union[Callable, None],
        numberOfRetries: int,
        maximumNumberOfRetries: int,
        waitTime: float,
) -> None:
    if retryCallbackMessage is not None:
        retryCallbackMessage(numberOfRetries, maximumNumberOfRetries, waitTime)
    else:
        print(f'Retry {numberOfRetries} of {maximumNumberOfRetries}. '
              f'Waiting {waitTime * 1000:.0f}ms')


def _get_wait_time(
        waitTime: float,
        backoffTimeIncrement: float,
        maximumBackoffTime: float,
) -> float:
    waitTime += random.random() * backoffTimeIncrement
    waitTime = min(waitTime, maximumBackoffTime)
    return waitTime


class MaximumNumberOfRetriesExceededError(Exception):
    pass
