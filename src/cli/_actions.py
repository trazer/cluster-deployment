import argparse
import os


class _FilePathsAction(argparse.Action):

    def __call__(self, parser, namespace, values, option_string=None):

        assert isinstance(values, list)
        assert len(values) == 1, 'Multiple paths not supported at this time.'
        assert all(isinstance(value, str) for value in values)

        for i, value in enumerate(values):
            values[i] = os.path.abspath(values[i])
            if not os.path.isfile(values[i]):
                raise ValueError('"{}" does not exist.'.format(values[i]))

        setattr(namespace, self.dest, values)
