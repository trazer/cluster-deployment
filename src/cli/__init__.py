from credentials import (
    delete_secrets,
    get_keyring_password,
    register_secrets,
)
from gitlab_runner import register_gitlab_runners
from juju_deploy import (
    bootstrap_juju_controllers,
    destroy_juju_controllers,
    install_juju_clients,
)
from maas_deploy import (
    commission_machines,
    configure_maas,
    install_maas_controllers,
)
from proxmox import create_virtual_machines
from ._parsers import (
    GET_KEYRING_PASSWORD_PARSER,
    GITLAB_RUNNERS_REGISTER_PARSER,
    JUJU_CLIENTS_INSTALL_PARSER,
    JUJU_CONTROLLERS_BOOTSTRAP_PARSER,
    JUJU_CONTROLLERS_DESTROY_PARSER,
    MAAS_COMMISSION_PARSER,
    MAAS_CONFIGURE_PARSER,
    MAAS_CONTROLLERS_INSTALL_PARSER,
    SECRETS_DELETE_PARSER,
    SECRETS_REGISTER_PARSER,
    VIRTUAL_MACHINES_CREATE_PARSER,
    parse,
)


def _main():

    arguments = parse()

    if arguments.parserName == GET_KEYRING_PASSWORD_PARSER:
        get_keyring_password()

    elif arguments.parserName == SECRETS_REGISTER_PARSER:
        register_secrets(
            arguments.secretsFilePaths,
            arguments.validate,
            arguments.force,
        )

    elif arguments.parserName == SECRETS_DELETE_PARSER:
        delete_secrets()

    elif arguments.parserName == VIRTUAL_MACHINES_CREATE_PARSER:
        create_virtual_machines(
            arguments.virtualMachinesFilePaths,
            arguments.skipExisting,
            arguments.stopIfOn,
        )

    elif arguments.parserName == MAAS_CONTROLLERS_INSTALL_PARSER:
        install_maas_controllers(
            arguments.virtualMachinesFilePaths,
            arguments.maasControllersFilePaths,
            arguments.forceReinstall,
        )

    elif arguments.parserName == MAAS_CONFIGURE_PARSER:
        configure_maas(
            arguments.virtualMachinesFilePaths,
            arguments.maasControllersFilePaths,
            arguments.maasNetworksFilePaths,
            arguments.forceReconfiguration,
        )

    elif arguments.parserName == MAAS_COMMISSION_PARSER:
        commission_machines(
            arguments.virtualMachinesFilePaths,
            arguments.maasControllersFilePaths,
            arguments.maasNetworksFilePaths,
            arguments.maasMachinesFilePaths,
            arguments.noDeploymentTesting,
            arguments.machineNames,
        )

    elif arguments.parserName == JUJU_CLIENTS_INSTALL_PARSER:
        install_juju_clients(
            arguments.virtualMachinesFilePaths,
            arguments.maasControllersFilePaths,
            arguments.maasNetworksFilePaths,
            arguments.jujuControllersFilePaths,
            arguments.forceReinstall,
        )

    elif arguments.parserName == JUJU_CONTROLLERS_BOOTSTRAP_PARSER:
        bootstrap_juju_controllers(
            arguments.virtualMachinesFilePaths,
            arguments.maasControllersFilePaths,
            arguments.maasNetworksFilePaths,
            arguments.jujuControllersFilePaths,
        )

    elif arguments.parserName == JUJU_CONTROLLERS_DESTROY_PARSER:
        destroy_juju_controllers(
            arguments.virtualMachinesFilePaths,
            arguments.maasControllersFilePaths,
            arguments.maasNetworksFilePaths,
            arguments.jujuControllersFilePaths,
        )

    elif arguments.parserName == GITLAB_RUNNERS_REGISTER_PARSER:
        register_gitlab_runners(
            arguments.virtualMachinesFilePaths,
            arguments.maasControllersFilePaths,
            arguments.maasNetworksFilePaths,
            arguments.maasMachinesFilePaths,
            arguments.runnersFilePaths,
        )

    else:
        raise NotImplementedError(arguments.parserName)
