import argparse
import sys
import textwrap
from collections import namedtuple

from ._actions import _FilePathsAction

FULL_HELP_PARSER = 'FULL_HELP_PARSER'

GET_KEYRING_PASSWORD_PARSER = 'GET_KEYRING_PASSWORD_PARSER'

SECRETS_DELETE_PARSER = 'SECRETS_DELETE_PARSER'
SECRETS_REGISTER_PARSER = 'SECRETS_REGISTER_PARSER'

SECRET_REGISTER_PARSER = 'SECRET_REGISTER_PARSER'
SECRET_DELETE_PARSER = 'SECRET_DELETE_PARSER'

VIRTUAL_MACHINES_CREATE_PARSER = 'VIRTUAL_MACHINES_CREATE_PARSER'
VIRTUAL_MACHINES_DELETE_PARSER = 'VIRTUAL_MACHINES_DELETE_PARSER'

VIRTUAL_MACHINE_CREATE_PARSER = 'VIRTUAL_MACHINE_CREATE_PARSER'
VIRTUAL_MACHINE_DELETE_PARSER = 'VIRTUAL_MACHINE_DELETE_PARSER'

MAAS_CONTROLLERS_INSTALL_PARSER = 'MAAS_CONTROLLERS_INSTALL_PARSER'

MAAS_CONFIGURE_PARSER = 'MAAS_CONFIGURE_PARSER'

MAAS_COMMISSION_PARSER = 'MAAS_COMMISSION_PARSER'

JUJU_CLIENTS_INSTALL_PARSER = 'JUJU_CLIENTS_INSTALL_PARSER'
JUJU_CLIENTS_UNINSTALL_PARSER = 'JUJU_CLIENTS_UNINSTALL_PARSER'

JUJU_CONTROLLERS_BOOTSTRAP_PARSER = 'JUJU_CONTROLLERS_BOOTSTRAP_PARSER'
JUJU_CONTROLLERS_DESTROY_PARSER = 'JUJU_CONTROLLERS_DESTROY_PARSER'

GITLAB_RUNNERS_REGISTER_PARSER = 'GITLAB_RUNNERS_REGISTER_PARSER'

_PARSERS = [
    FULL_HELP_PARSER,
    GET_KEYRING_PASSWORD_PARSER,
    SECRETS_DELETE_PARSER,
    SECRETS_REGISTER_PARSER,
    SECRET_REGISTER_PARSER,
    SECRET_DELETE_PARSER,
    VIRTUAL_MACHINES_CREATE_PARSER,
    VIRTUAL_MACHINES_DELETE_PARSER,
    VIRTUAL_MACHINE_CREATE_PARSER,
    VIRTUAL_MACHINE_DELETE_PARSER,
    MAAS_CONTROLLERS_INSTALL_PARSER,
    MAAS_CONFIGURE_PARSER,
    MAAS_COMMISSION_PARSER,
    JUJU_CLIENTS_INSTALL_PARSER,
    JUJU_CLIENTS_UNINSTALL_PARSER,
    JUJU_CONTROLLERS_BOOTSTRAP_PARSER,
    JUJU_CONTROLLERS_DESTROY_PARSER,
    GITLAB_RUNNERS_REGISTER_PARSER,
]

assert len(set(_PARSERS)) == len(_PARSERS)


_COMMON_PARAMETERS = {
    'VIRTUAL_MACHINES_FILE_PATHS': None,
    'MAAS_CONTROLLERS_FILE_PATHS': None,
    'MAAS_NETWORKS_FILE_PATHS': None,
    'MAAS_MACHINES_FILE_PATHS': None,
    'JUJU_CONTROLLERS_FILE_PATHS': None,
}


def parse():

    _define_common_parameters()

    # ------------------------------------------------

    parser = argparse.ArgumentParser(
        prog='cluster',
        description='manage & deploy cluster',
    )

    subparsers = parser.add_subparsers(
        help='commands to manage & deploy cluster',
    )

    # ------------------------------------------------

    fullHelpParser = subparsers.add_parser(
        'full-help',
        help='print complete help then exit',
    )
    fullHelpParser.set_defaults(parserName=FULL_HELP_PARSER)

    # ------------------------------------------------

    getKeyringPasswordParser = subparsers.add_parser(
        'get-keyring-password',
        help='command to get keyring password and write it '
             'to stdout so that it ca be stored into an environment '
             'variable',
    )
    _get_keyring_password_parser(getKeyringPasswordParser)

    # ------------------------------------------------

    secretsParser = subparsers.add_parser(
        'secrets',
        help='commands to manage secrets',
    )
    _secrets_parser(secretsParser)

    secretParser = subparsers.add_parser(
        'secret',
        help='commands to manage a secret',
    )
    _secret_parser(secretParser)

    # ------------------------------------------------

    virtualMachinesParser = subparsers.add_parser(
        'virtual-machines',
        help='commands to manage virtual machines',
    )
    _virtual_machines_parser(virtualMachinesParser)

    virtualMachineParser = subparsers.add_parser(
        'virtual-machine',
        help='commands to manage a virtual machine',
    )
    _virtual_machine_parser(virtualMachineParser)

    # ------------------------------------------------

    maasControllersParser = subparsers.add_parser(
        'maas-controllers',
        help='commands to install MAAS controllers',
    )
    _maas_controllers_parser(maasControllersParser)

    # ------------------------------------------------

    maasConfigureParser = subparsers.add_parser(
        'maas-configure',
        help='commands to configure MAAS',
    )
    _maas_configure_parser(maasConfigureParser)

    # ------------------------------------------------

    maasCommissionParser = subparsers.add_parser(
        'maas-commission',
        help='commands to commission machines in MAAS',
    )
    _maas_commission_parser(maasCommissionParser)

    # ------------------------------------------------

    jujuClientsParser = subparsers.add_parser(
        'juju-clients',
        help='commands to manage Juju clients',
    )
    _juju_clients_parser(jujuClientsParser)

    # ------------------------------------------------

    jujuControllersParser = subparsers.add_parser(
        'juju-controllers',
        help='commands to manage Juju controllers',
    )
    _juju_controllers_parser(jujuControllersParser)

    # ------------------------------------------------

    gitlabRunnersParser = subparsers.add_parser(
        'gitlab-runners',
        help='commands to manage GitLab runners',
    )
    _gitlab_runners_parser(gitlabRunnersParser)

    # ------------------------------------------------

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)

    if len(sys.argv) == 2 \
            and sys.argv[1] == fullHelpParser.prog.split(' ')[-1]:
        _print_full_help(parser)
        sys.exit(0)

    return parser.parse_args()


# ------------------------------------------------


def _define_common_parameters():

    CommonParameters = namedtuple('CommonParameters', 'args kwargs')

    assert 'VIRTUAL_MACHINES_FILE_PATHS' in _COMMON_PARAMETERS
    _COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'] = CommonParameters(
        args=('virtualMachinesFilePaths', ),
        kwargs={
            'metavar': 'VIRTUAL_MACHINES_FILE_PATHS',
            'action': _FilePathsAction,
            'nargs': 1,
            'help': 'file path to the virtual machines configuration files',
        },
    )

    assert 'MAAS_CONTROLLERS_FILE_PATHS' in _COMMON_PARAMETERS
    _COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'] = CommonParameters(
        args=('maasControllersFilePaths', ),
        kwargs={
            'metavar': 'MAAS_CONTROLLERS_FILE_PATHS',
            'action': _FilePathsAction,
            'nargs': 1,
            'help': 'file path to the MAAS controllers configuration files',
        },
    )

    assert 'MAAS_NETWORKS_FILE_PATHS' in _COMMON_PARAMETERS
    _COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'] = CommonParameters(
        args=('maasNetworksFilePaths', ),
        kwargs={
            'metavar': 'MAAS_NETWORKS_FILE_PATHS',
            'action': _FilePathsAction,
            'nargs': 1,
            'help': 'file path to the MAAS networks configuration files',
        },
    )

    assert 'MAAS_MACHINES_FILE_PATHS' in _COMMON_PARAMETERS
    _COMMON_PARAMETERS['MAAS_MACHINES_FILE_PATHS'] = CommonParameters(
        args=('maasMachinesFilePaths', ),
        kwargs={
            'metavar': 'MAAS_MACHINES_FILE_PATHS',
            'action': _FilePathsAction,
            'nargs': 1,
            'help': 'file path to the MAAS\' machines configuration files',
        },
    )

    assert 'JUJU_CONTROLLERS_FILE_PATHS' in _COMMON_PARAMETERS
    _COMMON_PARAMETERS['JUJU_CONTROLLERS_FILE_PATHS'] = CommonParameters(
        args=('jujuControllersFilePaths', ),
        kwargs={
            'metavar': 'JUJU_CONTROLLERS_FILE_PATHS',
            'action': _FilePathsAction,
            'nargs': 1,
            'help': 'file path to the Juju controllers configuration files',
        },
    )


# ------------------------------------------------


def _print_full_help(parser):

    print(parser.format_help())

    _recursive_print_subparser_help(parser)

    print('Summary of commands:')
    print(textwrap.indent(parser.prog, '\t'.expandtabs()))
    for level, subparser in _iter_subparsers(parser):
        print(textwrap.indent(subparser.prog, '\t'.expandtabs()))


def _recursive_print_subparser_help(parser):

    for level, subparser in _iter_subparsers(parser):
        print('{}:'.format(subparser.prog))
        print(textwrap.indent(subparser.format_help(), '\t'.expandtabs()))


def _iter_subparsers(parser, level=0):

    level += 1

    subparsersActions = [
        action
        for action in parser._actions
        if isinstance(action, argparse._SubParsersAction)
    ]

    for subparsersAction in subparsersActions:
        for choice, subparser in subparsersAction.choices.items():
            yield level, subparser
            yield from _iter_subparsers(subparser, level)


# ------------------------------------------------


def _get_keyring_password_parser(getKeyringPasswordParser):

    getKeyringPasswordParser.set_defaults(parserName=GET_KEYRING_PASSWORD_PARSER)


# ------------------------------------------------


def _secrets_parser(secretsParser):

    secretsSubparsers = secretsParser.add_subparsers(
        help='manage secrets',
    )

    _secrets_register_parser(secretsSubparsers)

    _secrets_delete_parser(secretsSubparsers)


def _secrets_register_parser(secretsSubparsers):

    secretsRegisterParser = secretsSubparsers.add_parser(
        'register',
        help='register secrets',
    )

    secretsRegisterParser.set_defaults(parserName=SECRETS_REGISTER_PARSER)

    secretsRegisterParser.add_argument(
        'secretsFilePaths',
        metavar='SECRETS_FILE_PATHS',
        action=_FilePathsAction,
        nargs=1,
        help='file path to the secret configuration file',
    )

    secretsRegisterParser.add_argument(
        '--validate',
        dest='validate',
        action='store_true',
        default=False,
        help='validate secrets using provided method',
    )

    secretsRegisterParser.add_argument(
        '--force',
        dest='force',
        action='store_true',
        default=False,
        help='re-ask secrets even if already in cache',
    )


def _secrets_delete_parser(secretsSubparsers):

    secretsDeleteParser = secretsSubparsers.add_parser(
        'delete',
        help='delete secrets',
    )

    secretsDeleteParser.set_defaults(parserName=SECRETS_DELETE_PARSER)


# ------------------------------------------------


def _secret_parser(secretParser):

    secretSubparsers = secretParser.add_subparsers(
        help='manage a secret',
    )

    _secret_register_parser(secretSubparsers)

    _secret_delete_parser(secretSubparsers)


def _secret_register_parser(secretSubparsers):

    secretRegisterParser = secretSubparsers.add_parser(
        'register',
        help='register a secret',
    )

    secretRegisterParser.set_defaults(parserName=SECRET_REGISTER_PARSER)

    secretRegisterParser.add_argument(
        'secretsFilePaths',
        metavar='SECRETS_FILE_PATHS',
        action=_FilePathsAction,
        nargs=1,
        help='file path to the secret configuration file',
    )

    secretRegisterParser.add_argument(
        'service',
        metavar='SERVICE',
        help='name of the service',
    )

    secretRegisterParser.add_argument(
        'username',
        metavar='USERNAME',
        help='name of the username',
    )

    secretRegisterParser.add_argument(
        '--validate',
        dest='validate',
        action='store_true',
        default=False,
        help='validate secret using provided method',
    )


def _secret_delete_parser(secretSubparsers):

    secretDeleteParser = secretSubparsers.add_parser(
        'delete',
        help='delete a secret',
    )

    secretDeleteParser.set_defaults(parserName=SECRET_DELETE_PARSER)

    secretDeleteParser.add_argument(
        'service',
        metavar='SERVICE',
        help='name of the service',
    )

    secretDeleteParser.add_argument(
        'username',
        metavar='USERNAME',
        help='name of the username',
    )


# ------------------------------------------------


def _virtual_machines_parser(virtualMachinesParser):

    virtualMachinesSubparsers = virtualMachinesParser.add_subparsers(
        help='manage virtual machines',
    )

    _virtual_machines_create_parser(virtualMachinesSubparsers)

    _virtual_machines_delete_parser(virtualMachinesSubparsers)


def _virtual_machines_create_parser(virtualMachinesSubparsers):

    virtualMachinesCreateParsers = virtualMachinesSubparsers.add_parser(
        'create',
        help='create virtual machines',
    )

    virtualMachinesCreateParsers.set_defaults(parserName=VIRTUAL_MACHINES_CREATE_PARSER)

    virtualMachinesCreateParsers.add_argument(
        *_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].kwargs,
    )

    virtualMachinesCreateParsers.add_argument(
        '--skip-existing',
        dest='skipExisting',
        action='store_true',
        default=False,
        help='skip existing virtual machines',
    )

    virtualMachinesCreateParsers.add_argument(
        '--stop-if-on',
        dest='stopIfOn',
        action='store_true',
        default=False,
        help='stop virtual machine if it on',
    )


def _virtual_machines_delete_parser(virtualMachinesSubparsers):

    virtualMachinesDeleteParsers = virtualMachinesSubparsers.add_parser(
        'delete',
        help='delete virtual machines',
    )

    virtualMachinesDeleteParsers.add_argument(
        *_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].kwargs,
    )

    virtualMachinesDeleteParsers.add_argument(
        '--stop-if-on',
        dest='stopIfOn',
        action='store_true',
        default=False,
        help='stop virtual machine if it on',
    )

    virtualMachinesDeleteParsers.set_defaults(parserName=VIRTUAL_MACHINES_DELETE_PARSER)


# ------------------------------------------------


def _virtual_machine_parser(virtualMachineParser):

    virtualMachineSubparsers = virtualMachineParser.add_subparsers(
        help='manage a virtual machine',
    )

    _virtual_machine_create_parser(virtualMachineSubparsers)

    _virtual_machine_delete_parser(virtualMachineSubparsers)


def _virtual_machine_create_parser(virtualMachineSubparsers):

    virtualMachineCreateParsers = virtualMachineSubparsers.add_parser(
        'create',
        help='create a virtual machine',
    )

    virtualMachineCreateParsers.set_defaults(parserName=VIRTUAL_MACHINE_CREATE_PARSER)

    virtualMachineCreateParsers.add_argument(
        *_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].kwargs,
    )

    virtualMachineCreateParsers.add_argument(
        'virtualMachineNames',
        metavar='VIRTUAL_MACHINE_NAMES',
        help='name of the virtual machine',
    )

    virtualMachineCreateParsers.add_argument(
        '--skip-existing',
        dest='skipExisting',
        action='store_true',
        default=False,
        help='skip existing virtual machines',
    )

    virtualMachineCreateParsers.add_argument(
        '--stop-if-on',
        dest='stopIfOn',
        action='store_true',
        default=False,
        help='stop virtual machine if it on',
    )


def _virtual_machine_delete_parser(virtualMachineSubparsers):

    virtualMachineDeleteParsers = virtualMachineSubparsers.add_parser(
        'delete',
        help='delete a virtual machine',
    )

    virtualMachineDeleteParsers.set_defaults(parserName=VIRTUAL_MACHINE_DELETE_PARSER)

    virtualMachineDeleteParsers.add_argument(
        *_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].kwargs,
    )

    virtualMachineDeleteParsers.add_argument(
        'virtualMachineName',
        metavar='VIRTUAL_MACHINE_NAME',
        help='name of the virtual machine',
    )

    virtualMachineDeleteParsers.add_argument(
        '--stop-if-on',
        dest='stopIfOn',
        action='store_true',
        default=False,
        help='stop virtual machine if it on',
    )


# ------------------------------------------------


def _maas_controllers_parser(maasControllersParser):

    maasControllersSubparsers = maasControllersParser.add_subparsers(
        help='manage maas',
    )

    _maas_controllers_install(maasControllersSubparsers)


def _maas_controllers_install(maasControllersSubparsers):

    maasControllersInstallParsers = maasControllersSubparsers.add_parser(
        'install',
        help='install MAAS controllers',
    )

    maasControllersInstallParsers.set_defaults(parserName=MAAS_CONTROLLERS_INSTALL_PARSER)

    maasControllersInstallParsers.add_argument(
        *_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].kwargs,
    )

    maasControllersInstallParsers.add_argument(
        *_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].kwargs,
    )

    maasControllersInstallParsers.add_argument(
        '--force-reinstall',
        dest='forceReinstall',
        action='store_true',
        default=False,
        help='force re-installation',
    )


# ------------------------------------------------


def _maas_configure_parser(maasConfigureParser):

    maasConfigureParser.set_defaults(parserName=MAAS_CONFIGURE_PARSER)

    maasConfigureParser.add_argument(
        *_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].kwargs,
    )

    maasConfigureParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].kwargs,
    )

    maasConfigureParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].kwargs,
    )

    maasConfigureParser.add_argument(
        '--force-reconfiguration',
        dest='forceReconfiguration',
        action='store_true',
        default=False,
        help='force reconfiguration',
    )


# ------------------------------------------------


def _maas_commission_parser(maasCommissionParser):

    maasCommissionParser.set_defaults(parserName=MAAS_COMMISSION_PARSER)

    maasCommissionParser.add_argument(
        *_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].kwargs,
    )

    maasCommissionParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].kwargs,
    )

    maasCommissionParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].kwargs,
    )

    maasCommissionParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_MACHINES_FILE_PATHS'].kwargs,
    )

    maasCommissionParser.add_argument(
        '--no-deployment-testing',
        dest='noDeploymentTesting',
        action='store_true',
        default=False,
        help='test deployment',
    )

    maasCommissionParser.add_argument(
        '--machine-name',
        dest='machineNames',
        action='append',
        default=[],
        help='Commission only specific machines',
    )


# ------------------------------------------------


def _juju_clients_parser(jujuClientsParser):

    jujuClientsSubparsers = jujuClientsParser.add_subparsers(
        help='',
    )

    _juju_clients_install_parser(jujuClientsSubparsers)

    _juju_clients_uninstall_parser(jujuClientsSubparsers)


def _juju_clients_install_parser(jujuClientsSubparsers):

    jujuClientsInstallParser = jujuClientsSubparsers.add_parser(
        'install',
        help='',
    )

    jujuClientsInstallParser.set_defaults(parserName=JUJU_CLIENTS_INSTALL_PARSER)

    jujuClientsInstallParser.add_argument(
        *_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].kwargs,
    )

    jujuClientsInstallParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].kwargs,
    )

    jujuClientsInstallParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].kwargs,
    )

    jujuClientsInstallParser.add_argument(
        *_COMMON_PARAMETERS['JUJU_CONTROLLERS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['JUJU_CONTROLLERS_FILE_PATHS'].kwargs,
    )

    jujuClientsInstallParser.add_argument(
        '--force-reinstall',
        dest='forceReinstall',
        action='store_true',
        default=False,
        help='force re-installation',
    )


def _juju_clients_uninstall_parser(jujuClientsSubparsers):

    jujuClientsUninstallParser = jujuClientsSubparsers.add_parser(
        'uninstall',
        help='',
    )

    jujuClientsUninstallParser.set_defaults(parserName=JUJU_CLIENTS_UNINSTALL_PARSER)

    jujuClientsUninstallParser.add_argument(
        *_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].kwargs,
    )

    jujuClientsUninstallParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].kwargs,
    )

    jujuClientsUninstallParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].kwargs,
    )

    jujuClientsUninstallParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].kwargs,
    )

    jujuClientsUninstallParser.add_argument(
        *_COMMON_PARAMETERS['JUJU_CONTROLLERS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['JUJU_CONTROLLERS_FILE_PATHS'].kwargs,
    )


# ------------------------------------------------


def _juju_controllers_parser(jujuControllersParser):

    jujuControllersSubparsers = jujuControllersParser.add_subparsers(
        help='',
    )

    _juju_controllers_bootstrap_parser(jujuControllersSubparsers)

    _juju_controllers_destroy_parser(jujuControllersSubparsers)


def _juju_controllers_bootstrap_parser(jujuControllersSubparsers):

    jujuControllersBootstrapParser = jujuControllersSubparsers.add_parser(
        'bootstrap',
        help='',
    )

    jujuControllersBootstrapParser.set_defaults(parserName=JUJU_CONTROLLERS_BOOTSTRAP_PARSER)

    jujuControllersBootstrapParser.add_argument(
        *_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].kwargs,
    )

    jujuControllersBootstrapParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].kwargs,
    )

    jujuControllersBootstrapParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].kwargs,
    )

    jujuControllersBootstrapParser.add_argument(
        *_COMMON_PARAMETERS['JUJU_CONTROLLERS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['JUJU_CONTROLLERS_FILE_PATHS'].kwargs,
    )


def _juju_controllers_destroy_parser(jujuControllersSubparsers):

    jujuControllersDestroyParser = jujuControllersSubparsers.add_parser(
        'destroy',
        help='',
    )

    jujuControllersDestroyParser.set_defaults(parserName=JUJU_CONTROLLERS_DESTROY_PARSER)

    jujuControllersDestroyParser.add_argument(
        *_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].kwargs,
    )

    jujuControllersDestroyParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].kwargs,
    )

    jujuControllersDestroyParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].kwargs,
    )

    jujuControllersDestroyParser.add_argument(
        *_COMMON_PARAMETERS['JUJU_CONTROLLERS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['JUJU_CONTROLLERS_FILE_PATHS'].kwargs,
    )


# ------------------------------------------------


def _gitlab_runners_parser(gitlabRunnersParser):

    gitlabRunnersSubparsers = gitlabRunnersParser.add_subparsers(
        help='',
    )

    _gitlab_runners_register_parser(gitlabRunnersSubparsers)


def _gitlab_runners_register_parser(gitlabRunnersSubparsers):

    gitlabRunnersRegisterParser = gitlabRunnersSubparsers.add_parser(
        'register',
        help='',
    )

    gitlabRunnersRegisterParser.set_defaults(parserName=GITLAB_RUNNERS_REGISTER_PARSER)

    gitlabRunnersRegisterParser.add_argument(
        *_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['VIRTUAL_MACHINES_FILE_PATHS'].kwargs,
    )

    gitlabRunnersRegisterParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_CONTROLLERS_FILE_PATHS'].kwargs,
    )

    gitlabRunnersRegisterParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_NETWORKS_FILE_PATHS'].kwargs,
    )

    gitlabRunnersRegisterParser.add_argument(
        *_COMMON_PARAMETERS['MAAS_MACHINES_FILE_PATHS'].args,
        **_COMMON_PARAMETERS['MAAS_MACHINES_FILE_PATHS'].kwargs,
    )

    gitlabRunnersRegisterParser.add_argument(
        'runnersFilePaths',
        metavar='RUNNERS_FILE_PATHS',
        action=_FilePathsAction,
        nargs=1,
        help='file path to the GitLab runners configuration file',
    )
