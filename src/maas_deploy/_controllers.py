from __future__ import annotations

import aiohttp.client_exceptions
import json
import os
import re
from abc import (
    ABC,
    abstractmethod,
)
from maas.client.enum import LinkMode
from typing import (
    Generator,
    TYPE_CHECKING,
    Union,
)

from clusterutils import async_retry
from clusterutils.resources import (
    Resource,
    ResourceKeyValidator,
    Resources,
)
from proxmox import (
    Hypervisors,
    VirtualMachine,
)
from ._utils import ensure_only_one_interface_for_api_server

if TYPE_CHECKING:
    import maas.client.viscera.controllers
    import maas.client.viscera.interfaces
    from ._maas_server import MaasServer
    from proxmox import NetworkDevice


_CURRENT_PATH = os.path.split(__file__)[0]
_SCRIPTS_FOLDER_NAME = '_scripts'


class BaseController(ABC):

    @property
    @abstractmethod
    async def actualController(self) -> Union[RackController, RegionController]:
        pass

    @property
    @abstractmethod
    async def allServicesRunning(self) -> bool:
        pass


class RegionControllers(Resources):

    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='iterate_region_controllers()',
            attributeNames=('name', ),
        )
    ]

    KEY_NAME = ('name', )

    def __init__(self, parentResource, maasServer: MaasServer):
        super().__init__(parentResource)
        self._maasServer = maasServer

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        if self.numberOfPrimaryRegions > 1:
            raise ValueError()

    @property
    def primaryRegionController(self) -> RegionController:
        for regionController in self.values():
            if regionController.primary:
                return regionController
        raise RuntimeError()

    @property
    def numberOfPrimaryRegions(self) -> int:
        return len([
            regionController
            for regionController in self.values()
            if regionController.primary
        ])

    def from_config(self, config: dict):
        for regionControllerConfig in config['virtual_machines']:
            virtualMachine = self._hypervisors.get_virtual_machine_by_name(
                regionControllerConfig['name']
            )
            regionController = RegionController(
                virtualMachine=virtualMachine,
                regionControllers=self,
            )
            regionController.from_config(regionControllerConfig)
            self[regionController.name] = regionController

    def iterate_region_controllers(self) -> Generator[RegionController]:
        yield from self.values()

    @property
    def _hypervisors(self) -> Hypervisors:
        return self._maasServer.hypervisors

    def __str__(self):
        return f'region controllers'


class RegionController(Resource, BaseController):

    def __init__(
            self,
            virtualMachine: VirtualMachine,
            regionControllers: RegionControllers,
    ):
        super().__init__(
            parentResource=None,
            resources=regionControllers,
        )
        self.virtualMachine = virtualMachine
        self.interfaces = RegionInterfaces(
            parentResource=self
        )
        self.name: str = None
        self.primary: bool = None

    @property
    async def allServicesRunning(self) -> bool:
        raise NotImplementedError()

    @property
    async def exists(self) -> bool:
        try:
            _ = await self.actualRegionController
        except RuntimeError:
            return False
        else:
            return True

    @property
    async def actualController(self) -> maas.client.viscera.controllers.RegionController:
        return await self.actualRegionController

    @property
    async def actualRegionController(self) -> maas.client.viscera.controllers.RegionController:
        client = await self.maasServer.client
        for actualRegionController in await client.region_controllers.list():
            if self.name == actualRegionController.hostname:
                return actualRegionController
        raise RuntimeError()

    @property
    def apiServerInterface(self) -> RegionInterface:
        for interface in self.interfaces.values():
            if interface.usedForApiServer:
                return interface
        raise RuntimeError()

    @property
    def maasServer(self) -> MaasServer:
        from ._maas_server import MaasServer
        assert isinstance(self.resources._maasServer, MaasServer)
        return self.resources._maasServer

    def from_config(self, config: dict):

        self.name = config['name']

        assert isinstance(config['primary'], bool)
        self.primary = config['primary']

        ensure_only_one_interface_for_api_server(config)

        for interfaceConfig in config['interfaces']:
            interface = RegionInterface(
                parentResource=self,
                resources=self.interfaces,
            )
            interface.from_config(interfaceConfig)
            interfaceKey = (
                interface.networkDeviceName,
                interface.networkInterfaceName,
            )
            self.interfaces[interfaceKey] = interface

    def iterate_interfaces(self) -> Generator[RegionInterface]:
        yield from self.interfaces.values()

    def __str__(self):
        return f'region controller {self.name}'


class RackControllers(Resources):

    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='iterate_rack_controllers()',
            attributeNames=('name', ),
        )
    ]

    KEY_NAME = ('name', )

    def __init__(self, parentResource, maasServer: MaasServer):
        super().__init__(parentResource)
        self._maasServer = maasServer

    def from_config(self, config: dict):
        for rackControllerConfig in config['virtual_machines']:
            virtualMachine = self._hypervisors.get_virtual_machine_by_name(
                rackControllerConfig['name']
            )
            rackController = RackController(
                virtualMachine=virtualMachine,
                rackControllers=self,
            )
            rackController.from_config(rackControllerConfig)
            self[rackController.name] = rackController

    def iterate_rack_controllers(self) -> Generator[RackController]:
        yield from self.values()

    @property
    def _hypervisors(self) -> Hypervisors:
        return self._maasServer.hypervisors

    def __str__(self):
        return f'rack controllers'


class RackController(Resource, BaseController):

    def __init__(
            self,
            virtualMachine: VirtualMachine,
            rackControllers: RackControllers,
    ):
        super().__init__(
            parentResource=None,
            resources=rackControllers,
        )
        self.virtualMachine = virtualMachine
        self.interfaces = RackInterfaces(
            parentResource=self
        )
        self.name: str = None

    @property
    async def imagesAreSynced(self) -> bool:
        actualRackController = await self.actualRackController
        virtualMachine = self.maasServer.apiServerVirtualMachine
        SCRIPT_FILE_NAME = 'list-boot-images-on-rack.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            SCRIPT_FILE_NAME,
        )
        environment = {
            'ADMIN_USER_NAME': self.maasServer.adminUserName,
            'RACK_SYSTEM_ID': actualRackController.system_id,
        }
        output, errorOutput = await virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{SCRIPT_FILE_NAME}',
            environment=environment,
        )
        output = output.strip('\n')
        output = json.loads(output)
        return output['status'] == 'synced'

    @property
    async def allServicesRunning(self) -> bool:
        SCRIPT_FILE_NAME = 'check-service-status.sh'
        SERVICE_NAMES = [
            'maas-dhcpd.service',
            # 'maas-dhcpd6.service',
            'maas-http.service',
            'maas-proxy.service',
            'maas-rackd.service',
            'maas-syslog.service',
        ]
        environment = {
            'ROOT_PASSWORD': self.maasServer.apiServerVirtualMachine.secret.value,
        }
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            SCRIPT_FILE_NAME,
        )
        for serviceName in SERVICE_NAMES:
            environment['SERVICE_NAME'] = serviceName
            output, errorOutput = await self.virtualMachine.execute_local_script_on_remote_async(
                localFilePath=localFilePath,
                remoteFilePath=f'/tmp/{SCRIPT_FILE_NAME}',
                environment=environment,
            )
            if output.strip('\n') != 'active':
                return False
        return True

    @property
    async def exists(self) -> bool:
        try:
            _ = await self.actualRackController
        except RuntimeError:
            return False
        else:
            return True

    @property
    async def actualController(self) -> maas.client.viscera.controllers.RackController:
        return await self.actualRackController

    @property
    async def actualRackController(self) -> maas.client.viscera.controllers.RackController:
        client = await self.maasServer.client
        for actualRackController in await client.rack_controllers.list():
            if self.name == actualRackController.hostname:
                return actualRackController
        raise RuntimeError()

    @property
    def maasServer(self) -> MaasServer:
        from ._maas_server import MaasServer
        assert isinstance(self.resources._maasServer, MaasServer)
        return self.resources._maasServer

    def from_config(self, config: dict):

        self.name = config['name']

        for interfaceConfig in config['interfaces']:
            interface = RackInterface(
                parentResource=self,
                resources=self.interfaces,
            )
            interface.from_config(interfaceConfig)
            interfaceKey = (
                interface.networkDeviceName,
                interface.networkInterfaceName,
            )
            self.interfaces[interfaceKey] = interface

    def iterate_interfaces(self) -> Generator[RackInterface]:
        yield from self.interfaces.values()

    def __str__(self):
        return f'rack controller {self.name}'


class RegionInterfaces(Resources):

    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='regionController.iterate_interfaces()',
            attributeNames=('networkDeviceName', ),
        ),
        ResourceKeyValidator(
            resourceIterableTree='regionController.iterate_interfaces()',
            attributeNames=('networkInterfaceName', ),
        ),
    ]

    KEY_NAME = (
        'networkDeviceName',
        'networkInterfaceName',
    )

    @property
    def regionController(self) -> RegionController:
        assert isinstance(self.parentResource, RegionController)
        return self.parentResource

    def __str__(self):
        return f'interfaces of {self.regionController}'


class RackInterfaces(Resources):

    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='rackController.iterate_interfaces()',
            attributeNames=('networkDeviceName', ),
        ),
        ResourceKeyValidator(
            resourceIterableTree='rackController.iterate_interfaces()',
            attributeNames=('networkInterfaceName', ),
        ),
    ]

    KEY_NAME = (
        'networkDeviceName',
        'networkInterfaceName',
    )

    @property
    def rackController(self) -> RackController:
        assert isinstance(self.parentResource, RackController)
        return self.parentResource

    def __str__(self):
        return f'interfaces of {self.rackController}'


class BaseInterface(Resource, ABC):

    def __init__(self, parentResource, resources):
        super().__init__(
            parentResource=parentResource,
            resources=resources,
        )
        self.networkDeviceName: str = None
        self.networkInterfaceName: str = None
        self.fabricName: str = None
        self.vid: int = None

    def from_config(self, config: dict) -> None:
        self.networkDeviceName = config['network_device_name']
        self.networkInterfaceName = config['network_interface_name']
        self.fabricName = config['fabric_name']
        assert isinstance(config['vid'], int) and 0 <= config['vid'] <= 4096
        self.vid = config['vid']

    def __str__(self):
        raise NotImplementedError()

    @property
    @abstractmethod
    def _controller(self) -> Union[RegionController, RackController]:
        pass

    async def configure(self) -> None:

        actualInterface = await self.actualInterface

        actualVlan = actualInterface.vlan
        if not actualVlan.loaded:
            await actualVlan.refresh()

        actualFabric = actualVlan.fabric
        if not actualFabric.loaded:
            await actualFabric.refresh()

        if actualFabric.name != self.fabricName \
                or actualVlan.vid != self.vid:
            fabrics = self._controller.maasServer.fabrics
            fabric = fabrics.by_name(self.fabricName)
            vlan = fabric.vlans.by_vid(self.vid)
            print(f'Configuring {self} with {vlan}.')
            actualInterface.vlan = await vlan.actualVlan
            await actualInterface.save()

        actualInterfaceLinks = actualInterface.links
        for actualInterfaceLink in actualInterfaceLinks:
            for networkInterface in self._networkDevice.networkInterfaces.values():
                if actualInterfaceLink.ip_address == networkInterface.ipAddress:
                    break
            else:
                raise ValueError(f'Unexpected {actualInterfaceLink.ip_address} '
                                 f'on {self}.')

        for networkInterface in self._networkDevice.networkInterfaces.values():
            for actualInterfaceLink in actualInterfaceLinks:
                if actualInterfaceLink.ip_address == networkInterface.ipAddress:
                    break
            else:
                raise ValueError(f'{networkInterface.ip_address} '
                                 f'not found on {self}.')

    @property
    async def exists(self) -> bool:
        try:
            _ = await self.actualInterface
        except RuntimeError:
            return False
        else:
            return True

    @property
    async def actualInterface(self) -> maas.client.viscera.interfaces.Interface:
        actualController = await self._controller.actualController
        for actualInterface in actualController.interfaces:
            if self._networkDevice.actualMacAddress.upper() == actualInterface.mac_address.upper():
                assert self._actual_interface_numbering_matches_virtual_machine_network_device(
                    actualInterface
                )
                return actualInterface
        raise RuntimeError()

    @property
    def _networkDevice(self) -> NetworkDevice:
        networkDevices = self._controller.virtualMachine.networkDevices
        return networkDevices.by_name(self.networkDeviceName)

    def _actual_interface_numbering_matches_virtual_machine_network_device(
            self,
            actualInterface: maas.client.viscera.interfaces.Interface,
    ) -> bool:
        ACTUAL_INTERFACE_PATTERN = re.compile(r'^eth(?P<index>[0-9]+)$')
        NETWORK_DEVICE_PATTERN = re.compile(r'^net(?P<index>[0-9]+)$')
        NETWORK_INTERFACE_PATTERN = re.compile(r'^ipconfig(?P<index>[0-9]+)$')
        # Compare with index of networkDevice and networkInterface
        # because it's not clear what cloud-init/MAAS uses when
        # numbering the "eth*" interfaces.
        index1 = re.search(ACTUAL_INTERFACE_PATTERN, actualInterface.name).group('index')
        index2 = re.search(NETWORK_DEVICE_PATTERN, self.networkDeviceName).group('index')
        index3 = re.search(NETWORK_INTERFACE_PATTERN, self.networkInterfaceName).group('index')
        return int(index1) == int(index2) == int(index3)


class RegionInterface(BaseInterface):

    def __init__(self, parentResource, resources):
        super().__init__(
            parentResource=parentResource,
            resources=resources,
        )
        self.usedForApiServer: bool = None

    def from_config(self, config: dict) -> None:
        super().from_config(config)
        assert isinstance(config['used_for_api_server'], bool)
        self.usedForApiServer = config['used_for_api_server']

    @property
    def _controller(self) -> RegionController:
        return self.regionController

    @property
    def regionController(self) -> RegionController:
        assert isinstance(self.parentResource, RegionController)
        return self.parentResource

    def __str__(self):
        return (
            f'interface '
            f'{self.networkDeviceName}/'
            f'{self.networkInterfaceName} on '
            f'{self.regionController}'
        )


class RackInterface(BaseInterface):

    def __init__(self, parentResource, resources):
        super().__init__(
            parentResource=parentResource,
            resources=resources,
        )

    @property
    def _controller(self) -> RackController:
        return self.rackController

    @property
    def rackController(self) -> RackController:
        assert isinstance(self.parentResource, RackController)
        return self.parentResource

    def __str__(self):
        return (
            f'interface '
            f'{self.networkDeviceName}/'
            f'{self.networkInterfaceName} on '
            f'{self.rackController}'
        )
