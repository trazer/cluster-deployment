import asyncio
import functools
import time
from typing import List

from proxmox import (
    Hypervisors,
    load_virtual_machines_from_config,
)
from ._controllers import RackController
from ._maas_server import (
    MaasServer,
    _load_config,
    load_maas_server,
)
from ._machines import (
    Machine,
    Machines,
)


def commission_machines(
        virtualMachinesFilePaths: List[str],
        maasControllersFilePaths: List[str],
        maasNetworksFilePaths: List[str],
        maasMachinesFilePaths: List[str],
        noDeploymentTesting: bool,
        machineNames: List[str],
) -> None:

    hypervisors = Hypervisors()
    load_virtual_machines_from_config(
        virtualMachinesFilePaths,
        hypervisors,
    )

    maasServer = MaasServer(hypervisors)
    load_maas_server(
        maasServer,
        maasControllersFilePaths,
        maasNetworksFilePaths,
    )

    machines = Machines(maasServer)
    _load_machines(
        machines,
        maasMachinesFilePaths,
    )

    if len(machineNames) == 0:
        machineNames = [machine.hostname for machine in machines.values()]

    _check_that_machine_names_exist(machines, machineNames)

    asyncio.run(_ensure_all_services_are_active_on_racks(
        maasServer,
    ))

    asyncio.run(_ensure_images_are_synced_on_racks(
        maasServer,
    ))

    asyncio.run(_commission_machines(
        maasServer,
        noDeploymentTesting,
        machines,
        machineNames,
    ))


def _load_machines(
        machines: Machines,
        maasControllersFilePaths: List[str],
) -> None:

    machinesConfig = {}
    _load_config(
        filePaths=maasControllersFilePaths,
        schemaFileName='machines_schema.yaml',
        config=machinesConfig,
    )

    machines.from_config(
        machinesConfig=machinesConfig,
    )


# ------------------------------------------------


def _check_that_machine_names_exist(
        machines: Machines,
        machineNames: List[str],
) -> None:
    for machineName in machineNames:
        try:
            _ = machines[machineName]
        except KeyError:
            raise KeyError(
                f'Machine {machineName} specified in '
                f'--machine-name does not exist.'
            )


# ------------------------------------------------


async def _ensure_all_services_are_active_on_racks(
        maasServer: MaasServer,
) -> None:
    await asyncio.gather(*list(map(
        _ensure_all_services_are_active_on_rack,
        maasServer.rackControllers.values(),
    )))


async def _ensure_all_services_are_active_on_rack(
        rackController: RackController,
) -> None:

    print(f'Checking if all services are running on {rackController}.')

    while not await rackController.allServicesRunning:
        print(f'Some or all services are not running on {rackController}...'
              f'restarting {rackController.virtualMachine}.')
        await rackController.virtualMachine.restart_async()
        # Make sure services have time to start
        await asyncio.sleep(20)

    print(f'All services are running on {rackController}.')


# ------------------------------------------------


async def _ensure_images_are_synced_on_racks(
        maasServer: MaasServer,
) -> None:
    await asyncio.gather(*list(map(
        _ensure_images_are_synced_on_rack,
        maasServer.rackControllers.values(),
    )))


async def _ensure_images_are_synced_on_rack(
        rackController: RackController,
) -> None:

    print(f'Checking if images are synced on {rackController}.')

    while not await rackController.imagesAreSynced:
        print(f'Waiting for images to be synced on {rackController}.')
        await asyncio.sleep(5)

    print(f'Images are synced on {rackController}.')


# ------------------------------------------------


async def _commission_machines(
        maasServer: MaasServer,
        noDeploymentTesting: bool,
        machines: Machines,
        machineNames: List[str],
) -> None:
    await asyncio.gather(*list(map(
        functools.partial(_commission_machine, maasServer, noDeploymentTesting),
        (machine for machine in machines.values() if machine.hostname in machineNames),
    )))


async def _commission_machine(
        maasServer: MaasServer,
        noDeploymentTesting: bool,
        machine: Machine,
) -> None:

    print(f'Checking if {machine} already exists.')
    if not await machine.exists:

        await _power_off_if_on(machine)

        print(f'Powering on {machine} for the first time.')
        await machine.bmc_power_on()

        await _wait_for_discovery_and_reset_if_timeout(machine)

    else:
        print(f'{machine} already exists.')

    await _wait_for_machine_to_be_new_or_ready(machine)

    if not await machine.ready:

        print(f'Setting hostname for {machine}.')
        await machine.set_hostname()

        print(f'Configuring BMC for {machine}.')
        await machine.configure_bmc()

        print(f'Commissioning {machine}.')
        await machine.commission()

        await _wait_for_commissioning_and_retry_if_failure(machine)

    print(f'Configuring {machine}.')
    await machine.configure()

    if not noDeploymentTesting:
        print(f'Testing deployment of {machine}')
        await machine.deploy()
        await _wait_for_successful_deployment_and_retry_if_failure_then_release(machine)

    print(f'{machine} is ready.')


async def _power_off_if_on(machine: Machine) -> None:
    print(f'Checking if {machine} is already on.')
    if await machine.bmcOn:
        print(f'Powering off {machine} first.')
        await machine.bmc_power_off()
        while await machine.bmcOn:
            print(f'Waiting for {machine} to power off.')
            await asyncio.sleep(5)
    else:
        print(f'{machine} is not already on.')


async def _wait_for_discovery_and_reset_if_timeout(machine: Machine):
    DISCOVERY_TIME_OUT = 10 * 60  # 10 minutes
    ts = time.time()
    while not await machine.exists:
        print(f'Waiting for {machine} to be discovered by MAAS.')
        if time.time() - ts > DISCOVERY_TIME_OUT:
            print(f'Discovery timeout for {machine}...resetting.')
            await machine.bmc_power_reset()
            ts = time.time()
        await asyncio.sleep(5)


async def _wait_for_machine_to_be_new_or_ready(machine: Machine) -> None:
    while not (await machine.new or await machine.ready):
        if await machine.failedCommissioning:
            print(f'Initial commissioning has failed for {machine}...continuing')
            break
        print(f'Waiting for {machine} to be new or ready.')
        await asyncio.sleep(5)


async def _wait_for_commissioning_and_retry_if_failure(machine: Machine) -> None:
    while not await machine.ready:
        print(f'Waiting for {machine} to be ready.')
        await asyncio.sleep(5)
        if await machine.failedCommissioning:
            print(f'Failed commissioning for {machine}...retrying.')
            await machine.commission()


async def _wait_for_successful_deployment_and_retry_if_failure_then_release(machine: Machine) -> None:

    NUMBER_OF_RETRIES = 5

    numberOfRetries = 1

    while not await machine.deployed:

        if await machine.failedDeployment \
                and numberOfRetries <= NUMBER_OF_RETRIES:
            print(f'Failed deployment for {machine}...'
                  f'releasing then retrying.')
            await _release_and_wait_and_retry_if_failure(machine)
            print(f'Deploying attempt #{numberOfRetries} of '
                  f'{NUMBER_OF_RETRIES} for {machine}.')
            await machine.deploy()
            numberOfRetries += 1
            continue

        elif await machine.failedDeployment \
                and numberOfRetries > NUMBER_OF_RETRIES:
            raise FailedDeployment(f'{machine}')

        print(f'Waiting for {machine} to be deployed.')
        await asyncio.sleep(5)

    print(f'{machine} is deployed...releasing.')
    await _release_and_wait_and_retry_if_failure(machine)


async def _release_and_wait_and_retry_if_failure(machine: Machine) -> None:

    NUMBER_OF_RETRIES = 5

    numberOfRetries = 1

    print(f'Releasing {machine}.')
    await machine.release()

    while not await machine.ready:

        if await machine.failedReleasing \
                and numberOfRetries <= NUMBER_OF_RETRIES:
            print(f'Failed releasing for {machine}...retrying attempt '
                  f'#{numberOfRetries} of {NUMBER_OF_RETRIES}.')
            await machine.release()
            numberOfRetries += 1
            continue

        elif await machine.failedReleasing \
                and numberOfRetries > NUMBER_OF_RETRIES:
            raise FailedReleasing(f'{machine}')

        print(f'Waiting for {machine} to be released.')
        await asyncio.sleep(5)


class FailedDeployment(Exception):
    pass


class FailedReleasing(Exception):
    pass
