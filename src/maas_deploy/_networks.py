from __future__ import annotations

import os
from abc import (
    ABC,
    abstractmethod,
)
from ipaddress import (
    IPv4Address,
    IPv4Network,
)
from typing import (
    Generator,
    List,
    TYPE_CHECKING,
    Union,
)

from maas.client.enum import IPRangeType
from maas.client.viscera.controllers import RackController as ActualRackController
from maas.client.viscera.fabrics import Fabric as ActualFabric
from maas.client.viscera.ipranges import IPRange as ActualIPRange
from maas.client.viscera.subnets import Subnet as ActualSubnet
from maas.client.viscera.vlans import (
    Vlan as ActualVlan,
    Vlans as ActualVlans,
)

from clusterutils.resources import (
    Resource,
    ResourceKeyValidator,
    Resources,
)
from proxmox import VirtualMachine

if TYPE_CHECKING:
    from ._maas_server import MaasServer


_CURRENT_PATH = os.path.split(__file__)[0]
_SCRIPTS_FOLDER_NAME = '_scripts'


class Fabrics(Resources):

    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='iterate_fabrics()',
            attributeNames=('name', ),
        )
    ]

    KEY_NAME = ('name', )

    def __init__(
            self,
            parentResource,
            maasServer: MaasServer,
    ):
        super().__init__(parentResource)
        self.maasServer = maasServer

    def from_config(self, config: dict):
        for fabricConfig in config['fabrics']:
            fabric = Fabric(fabrics=self)
            fabric.from_config(fabricConfig)
            self[fabric.name] = fabric

    def iterate_fabrics(self) -> Generator[Fabric]:
        yield from self.values()

    def iterate_subnets(self) -> Generator[Subnet]:
        for fabric in self.values():
            for vlan in fabric.vlans.values():
                for subnet in vlan.subnets.values():
                    yield subnet

    @property
    def fabricNames(self) -> List[str]:
        return [fabric.name for fabric in self.values()]

    def by_name(self, name: str) -> Fabric:
        assert self.KEY_NAME == ('name', )
        return self[name]

    def __str__(self):
        return f'fabrics'


class Fabric(Resource):

    def __init__(
            self,
            fabrics: Fabrics,
    ):
        super().__init__(
            parentResource=None,
            resources=fabrics,
        )
        self.name: str = None
        self.vlans = Vlans(
            parentResource=self,
        )

    @property
    async def exists(self) -> bool:
        try:
            _ = await self.actualFabric
        except RuntimeError:
            return False
        else:
            return True

    async def create(self) -> None:
        raise NotImplementedError()

    async def configure(self) -> None:
        await self._create_vlans()
        await self._configure_vlans()

    async def _configure_vlans(self) -> None:
        for vlan in self.vlans.values():
            assert await vlan.exists
            print(f'Configuring {vlan}.')
            await vlan.configure()

    async def _create_vlans(self) -> None:
        for vlan in self.vlans.values():
            print(f'Checking if {vlan} exists.')
            if await vlan.exists:
                continue
            print(f'{vlan} does not exist...creating it.')
            await vlan.create()

    @property
    async def actualFabric(self) -> ActualFabric:
        for actualFabric in await self.maasServer.actualFabrics:
            if actualFabric.name == self.name:
                return actualFabric
        raise RuntimeError()

    @property
    async def actualVlans(self) -> ActualVlans:
        actualFabric = await self.actualFabric
        assert actualFabric.loaded
        actualVlans = actualFabric.vlans
        return actualVlans

    @property
    def maasServer(self) -> MaasServer:
        from ._maas_server import MaasServer
        assert isinstance(self.resources.maasServer, MaasServer)
        return self.resources.maasServer

    def from_config(self, config: dict):

        self.name = config['name']

        for vlanConfig in config['vlans']:
            vlan = Vlan(
                fabric=self,
                vlans=self.vlans,
            )
            vlan.from_config(vlanConfig)
            self.vlans[vlan.vid] = vlan

    def iterate_vlans(self) -> Generator[Vlan]:
        yield from self.vlans.values()

    @property
    def fabrics(self) -> Fabrics:
        assert isinstance(self.resources, Fabrics)
        return self.resources

    def __str__(self):
        return f'fabric {self.name}'


class Vlans(Resources):

    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='fabric.iterate_vlans()',
            attributeNames=('vid', ),
        )
    ]

    KEY_NAME = ('vid', )

    @property
    def fabric(self) -> Fabric:
        assert isinstance(self.parentResource, Fabric)
        return self.parentResource

    @property
    def vids(self) -> List[int]:
        return [vlan.vid for vlan in self.values()]

    def by_vid(self, vid: int) -> Vlan:
        assert self.KEY_NAME == ('vid', )
        return self[vid]

    def __str__(self):
        return f'vlans on {self.fabric}'


class Vlan(Resource):

    def __init__(
            self,
            fabric: Fabric,
            vlans: Vlans,
    ):
        super().__init__(
            parentResource=fabric,
            resources=vlans,
        )
        self.name: str = None
        self.vid: int = None
        self.dhcpOn: bool = None
        self._primaryRackVirtualMachine: Union[None, VirtualMachine] = None
        self._secondaryRackVirtualMachine: Union[None, VirtualMachine] = None
        self.dhcpOn: bool = None
        self.subnets = Subnets(
            parentResource=self,
        )

    def from_config(self, config: dict):

        assert isinstance(config['name'], str)
        self.name = config['name']

        assert isinstance(config['vid'], int)
        self.vid = config['vid']

        assert isinstance(config['dhcp_on'], bool)
        self.dhcpOn = config['dhcp_on']

        KEY_NAMES = ('primary_rack_name', 'secondary_rack_name')
        ATTR_NAMES = ('_primaryRackVirtualMachine', '_secondaryRackVirtualMachine')
        for keyName, attrName in zip(KEY_NAMES, ATTR_NAMES):
            assert isinstance(config[keyName], str)
            if config[keyName]:
                virtualMachine = self.maasServer.hypervisors.get_virtual_machine_by_name(
                    config[keyName]
                )
                setattr(self, attrName, virtualMachine)
            else:
                setattr(self, attrName, None)

        if self.dhcpOn \
                and self._primaryRackVirtualMachine is None \
                and self._secondaryRackVirtualMachine is None:
            raise ValueError()
        elif not self.dhcpOn \
                and (self._primaryRackVirtualMachine is not None
                     or self._secondaryRackVirtualMachine is not None):
            raise ValueError()

        if self._primaryRackVirtualMachine is None \
                and self._secondaryRackVirtualMachine is not None:
            raise ValueError()

        for subnetConfig in config['subnets']:
            subnet = Subnet(
                vlan=self,
                subnets=self.subnets,
            )
            subnet.from_config(subnetConfig)
            self.subnets[subnet.name] = subnet

    @property
    async def exists(self) -> bool:
        try:
            _ = await self.actualVlan
        except RuntimeError:
            return False
        else:
            return True

    async def create(self) -> None:
        actualFabric = await self.fabric.actualFabric
        await actualFabric.vlans.create(
            self.vid,
            name=self.name,
            dhcp_on=self.dhcpOn,
        )

    async def configure(self) -> None:
        await self._create_subnets()
        await self._configure_subnets()
        await self._configure_vlan()

    async def _create_subnets(self) -> None:
        for subnet in self.subnets.values():
            print(f'Checking if {subnet} exists.')
            if await subnet.exists:
                continue
            print(f'{subnet} does not exist...creating it.')
            await subnet.create()

    async def _configure_subnets(self) -> None:
        for subnet in self.subnets.values():
            assert await subnet.exists
            print(f'Configuring {subnet}.')
            await subnet.configure()

    async def _configure_vlan(self) -> None:

        actualVlan = await self.actualVlan

        if actualVlan.name != self.name:
            print(f'Setting name to {self.name} for {self}.')
            actualVlan.name = self.name

        if actualVlan.vid != self.vid:
            print(f'Setting vid to {str(self.vid)} for {self}.')
            actualVlan.vid = self.vid

        await actualVlan.save()

        # We enable/disable dhcp using shell scripts since
        # there appear to be a bug in
        # maas.client.viscera.ObjectFieldRelated#value_to_datum
        # that causes a TypeError when using
        # actualVlan.primary_rack = await self._actualPrimaryRack.
        # The problem is that the aforementioned method says
        # that _actualPrimaryRack is not of type RackController
        # even if it is.
        actualFabric = await self.fabric.actualFabric
        environment = {
            'ROOT_PASSWORD': self.maasServer.apiServerVirtualMachine.secret.value,
            'ADMIN_USER_NAME': self.maasServer.apiServerVirtualMachine.secret.userName,
            'MAAS_URL': self.maasServer.apiUrl,
            'API_KEY': await self.maasServer.apiKey,
            'FABRIC_ID': actualFabric.id,
            'VLAN_VID': actualVlan.vid,
        }
        if await self._need_to_enable_dhcp_with_one_rack(actualVlan):
            scriptFileName = 'enable-dhcp-on-vlan.sh'
            actualPrimaryRack = await self._actualPrimaryRack
            environment['PRIMARY_RACK_SYSTEM_ID'] = actualPrimaryRack.system_id
            assert actualVlan.secondary_rack is None, \
                'Make sure secondary rack will be removed by enable-dhcp-on-vlan.sh'
        elif await self._need_to_enable_dhcp_with_two_racks(actualVlan):
            scriptFileName = 'enable-dhcp-on-vlan.sh'
            actualPrimaryRack = await self._actualPrimaryRack
            actualSecondaryRack = await self._actualSecondaryRack
            environment['PRIMARY_RACK_SYSTEM_ID'] = actualPrimaryRack.system_id
            environment['SECONDARY_RACK_SYSTEM_ID'] = actualSecondaryRack.system_id
        elif await self._need_to_disable_dhcp(actualVlan):
            scriptFileName = 'disable-dhcp-on-vlan.sh'
        else:
            return

        print(f'Setting dhcp_on to {str(self.dhcpOn)} for {self}.')
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            scriptFileName,
        )
        apiServerVirtualMachine = self.maasServer.apiServerVirtualMachine
        output, errorOutput = await apiServerVirtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{scriptFileName}',
            environment=environment,
        )

        assert await self._racks_are_properly_configured(actualVlan)

    async def _need_to_enable_dhcp_with_one_rack(
            self,
            actualVlan: ActualVlan
    ) -> bool:
        if actualVlan.primary_rack is not None:
            actualPrimaryRack = actualVlan.primary_rack
            await actualPrimaryRack.refresh()
        if self.dhcpOn \
                and not actualVlan.dhcp_on \
                and self.primaryRackName is not None \
                and self.secondaryRackName is None:
            return True
        elif self.dhcpOn \
                and actualVlan.dhcp_on \
                and self.primaryRackName is not None \
                and self.secondaryRackName is None \
                and self.primaryRackName != actualPrimaryRack.hostname:
            return True
        else:
            return False

    async def _need_to_enable_dhcp_with_two_racks(
            self,
            actualVlan: ActualVlan
    ) -> bool:
        if actualVlan.primary_rack is not None:
            actualPrimaryRack = actualVlan.primary_rack
            await actualPrimaryRack.refresh()
        if actualVlan.secondary_rack is not None:
            actualSecondaryRack = actualVlan.secondary_rack
            await actualSecondaryRack.refresh()
        if self.dhcpOn \
                and not actualVlan.dhcp_on \
                and self.primaryRackName is not None \
                and self.secondaryRackName is not None:
            return True
        elif self.dhcpOn \
                and not actualVlan.dhcp_on \
                and self.primaryRackName is not None \
                and self.secondaryRackName is not None \
                and (self.primaryRackName != actualPrimaryRack.hostname
                     or self.secondaryRackName != actualSecondaryRack.hostname):
            return True
        else:
            return False

    async def _need_to_disable_dhcp(
            self,
            actualVlan: ActualVlan
    ) -> bool:
        if not self.dhcpOn \
                and actualVlan.dhcp_on:
            return True
        else:
            return False

    async def _racks_are_properly_configured(
            self,
            actualVlan: ActualVlan
    ) -> bool:
        """
        Make sure the primary/secondary racks are properly configured
        """
        await actualVlan.refresh()
        if await self._need_to_enable_dhcp_with_one_rack(actualVlan):
            if actualVlan.primary_rack is not None \
                    and actualVlan.secondary_rack is None:
                return True
            else:
                return False
        elif await self._need_to_enable_dhcp_with_two_racks(actualVlan):
            if actualVlan.primary_rack is not None \
                    and actualVlan.secondary_rack is not None:
                return True
            else:
                return False
        elif await self._need_to_disable_dhcp(actualVlan):
            if actualVlan.primary_rack is None \
                    and actualVlan.secondary_rack is None:
                return True
            else:
                return False
        else:
            return True

    @property
    def fabric(self) -> Fabric:
        assert isinstance(self.parentResource, Fabric)
        return self.parentResource

    @property
    async def actualVlan(self) -> ActualVlan:
        for actualVlan in await self.fabric.actualVlans:
            assert isinstance(actualVlan.vid, int)
            if actualVlan.vid == self.vid:
                return actualVlan
        raise RuntimeError()

    @property
    async def actualSubnets(self) -> List[ActualSubnet]:
        client = await self.maasServer.client
        actualSubnets = []
        for actualSubnet in await client.subnets.list():
            actualFabric_ = actualSubnet.vlan.fabric
            if not actualFabric_.loaded:
                await actualFabric_.refresh()
            if actualFabric_.name != self.fabric.name:
                continue
            if actualSubnet.vlan.vid != self.vid:
                continue
            actualSubnets.append(actualSubnet)
        return actualSubnets

    @property
    async def _actualPrimaryRack(self) -> ActualRackController:
        assert self.primaryRackName is not None
        return await self._getActualRack(self.primaryRackName)

    @property
    async def _actualSecondaryRack(self) -> ActualRackController:
        assert self.secondaryRackName is not None
        return await self._getActualRack(self.secondaryRackName)

    async def _getActualRack(self, name: str) -> ActualRackController:
        for actualRackController in await self.maasServer.actualRackControllers:
            if actualRackController.hostname == name:
                return actualRackController
        raise RuntimeError()

    @property
    def primaryRackName(self) -> Union[None, str]:
        return (
            self._primaryRackVirtualMachine.name
            if self._primaryRackVirtualMachine is not None
            else None
        )

    @property
    def secondaryRackName(self) -> Union[None, str]:
        return (
            self._secondaryRackVirtualMachine.name
            if self._secondaryRackVirtualMachine is not None
            else None
        )

    @property
    def maasServer(self) -> MaasServer:
        from ._maas_server import MaasServer
        assert isinstance(self.fabric.maasServer, MaasServer)
        return self.fabric.maasServer

    def iterate_subnets(self) -> Generator[Subnet]:
        yield from self.subnets.values()

    def __str__(self):
        if self.name:
            return f'vlan {str(self.vid)} ({self.name}) on {self.fabric}'
        else:
            return f'vlan {str(self.vid)} on {self.fabric}'


class Subnets(Resources):

    # Subnet must be unique across all fabrics
    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='vlan.fabric.fabrics.iterate_subnets()',
            attributeNames=('name', ),
        ),
        ResourceKeyValidator(
            resourceIterableTree='vlan.fabric.fabrics.iterate_subnets()',
            attributeNames=('ipCidr', ),
        ),
    ]

    KEY_NAME = ('name', )

    @property
    def vlan(self) -> Vlan:
        assert isinstance(self.parentResource, Vlan)
        return self.parentResource

    @property
    def ipCidrs(self) -> List[str]:
        return [subnet.ipCidr for subnet in self.values()]

    def by_ip_cidr(self, ipCidr: str) -> Subnet:
        ipCidr = IPv4Network(ipCidr, strict=False)
        for subnet in self.values():
            if subnet.ipCidr == ipCidr.compressed:
                return subnet
        raise KeyError(ipCidr)

    def __str__(self):
        return f'subnets on {self.vlan}'


class Subnet(Resource):

    def __init__(
            self,
            vlan: Vlan,
            subnets: Subnets,
    ):
        super().__init__(
            parentResource=vlan,
            resources=subnets,
        )
        self.name: str = None
        self._ipCidr: IPv4Network = None
        self._gatewayIp: Union[IPv4Address, None] = None
        self._dnsServers: List[IPv4Address] = []
        self.reservedIpRanges = ReservedIpRanges(parentResource=self)
        self.dynamicIpRanges = DynamicIpRanges(parentResource=self)

    @property
    async def exists(self) -> bool:
        try:
            _ = await self.actualSubnet
        except RuntimeError:
            return False
        else:
            return True

    async def create(self) -> None:
        client = await self.maasServer.client
        actualVlan = await self.vlan.actualVlan
        await client.subnets.create(
            self.ipCidr,
            actualVlan,
            name=self.name,
            gateway_ip=self.gatewayIp,
            dns_servers=self.dnsServers,
            managed=False,
        )

    async def configure(self) -> None:
        await self._configure_subnet()
        await self._delete_existing_ip_ranges()
        await self._create_ip_ranges()

    async def _configure_subnet(self) -> None:

        actualSubnet = await self.actualSubnet

        if actualSubnet.name != self.name:
            print(f'Setting name to {self.name} for {self}.')
            actualSubnet.name = self.name

        if actualSubnet.cidr != self.ipCidr:
            print(f'Setting cidr to {self.ipCidr} for {self}.')
            actualSubnet.cidr = self.ipCidr

        if actualSubnet.gateway_ip != self.gatewayIp:
            print(f'Setting gateway_ip to {self.gatewayIp} for {self}.')
            actualSubnet.gateway_ip = self.gatewayIp

        # We force managed to False since it makes more sense
        # that the ip range refer to the IPv4 that MAAS can use
        # for the enlisting, commissioning, and deploying.
        if self.vlan.dhcpOn and not actualSubnet.managed:
            print(f'Setting managed to True for {self}.')
            actualSubnet.managed = True
        elif not self.vlan.dhcpOn and actualSubnet.managed:
            print(f'Setting managed to False for {self}.')
            actualSubnet.managed = False

        if sorted(actualSubnet.dns_servers) != sorted(self.dnsServers):
            print(f'Setting dns_servers to {self.dnsServers} for {self}.')
            actualSubnet.dns_servers = self.dnsServers

        await actualSubnet.save()

    async def _delete_existing_ip_ranges(self) -> None:
        client = await self.maasServer.client
        for actualIpRange in await client.ip_ranges.list():
            if actualIpRange.subnet.cidr != self.ipCidr:
                continue
            print(f'Deleting existing ip range '
                  f'{actualIpRange.start_ip}-{actualIpRange.end_ip} '
                  f'on {self}.')
            await actualIpRange.delete()

    async def _create_ip_ranges(self) -> None:

        for reservedIpRange in self.reservedIpRanges.values():
            assert not await reservedIpRange.exists
            assert self.vlan.dhcpOn
            print(f'Creating reserved {reservedIpRange}.')
            await reservedIpRange.create()

        for dynamicIpRange in self.dynamicIpRanges.values():
            assert not await dynamicIpRange.exists
            assert self.vlan.dhcpOn
            print(f'Creating dynamic {dynamicIpRange}.')
            await dynamicIpRange.create()

    def from_config(self, config: dict) -> None:

        assert isinstance(config['name'], str)
        self.name = config['name']

        self._ipCidr = IPv4Network(config['ip_cidr'])

        assert isinstance(config['gateway_ip'], str)
        self._gatewayIp = (
            IPv4Address(config['gateway_ip'])
            if config['gateway_ip'] else None
        )

        assert all(isinstance(dnsServer, str) for dnsServer in config['dns_servers'])
        self._dnsServers = [
            IPv4Address(dnsServer)
            for dnsServer in config['dns_servers']
            if dnsServer
        ]

        for ipRangeConfig in config['ip_ranges']:
            assert isinstance(ipRangeConfig['for_dhcp'], bool)
            if not ipRangeConfig['for_dhcp']:
                continue
            dynamicIpRange = DynamicIpRange(
                subnet=self,
                ipRanges=self.dynamicIpRanges,
            )
            dynamicIpRange.from_config(ipRangeConfig)
            key = (dynamicIpRange.start, dynamicIpRange.end)
            self.dynamicIpRanges[key] = dynamicIpRange

        for ipRangeConfig in _iter_complementary_ip_addresses(config['ip_ranges'], self):
            reservedIpRange = ReservedIpRange(
                subnet=self,
                ipRanges=self.reservedIpRanges,
            )
            reservedIpRange.from_config(ipRangeConfig)
            key = (reservedIpRange.start, reservedIpRange.end)
            self.reservedIpRanges[key] = reservedIpRange

    @property
    async def actualSubnet(self) -> ActualSubnet:
        for actualSubnet in await self.vlan.actualSubnets:
            if actualSubnet.cidr == self.ipCidr:
                return actualSubnet
        raise RuntimeError()

    @property
    def vlan(self) -> Vlan:
        assert isinstance(self.parentResource, Vlan)
        return self.parentResource

    @property
    def dnsServers(self) -> List[str]:
        return [dnsServer.compressed for dnsServer in self._dnsServers]

    @property
    def gatewayIp(self) -> Union[str, None]:
        return (
            self._gatewayIp.compressed
            if self._gatewayIp is not None
            else None
        )

    @property
    def ipCidr(self) -> str:
        return self._ipCidr.compressed

    @property
    def maasServer(self) -> MaasServer:
        from ._maas_server import MaasServer
        assert isinstance(self.vlan.maasServer, MaasServer)
        return self.vlan.maasServer

    def __str__(self):
        return f'subnet {self.ipCidr} ({self.name}) on {self.vlan}'


class IpRanges(Resources, ABC):

    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='values()',
            attributeNames=('start', 'end'),
        ),
        ResourceKeyValidator(
            resourceIterableTree='values()',
            attributeNames=('start', ),
        ),
        ResourceKeyValidator(
            resourceIterableTree='values()',
            attributeNames=('end', ),
        ),
    ]

    KEY_NAME = ('start', 'end')

    def __setitem__(self, key, ipRange: Union[DynamicIpRange, ReservedIpRange]):

        ipAddresses = set(
            ipAddress.compressed
            for ipAddress
            in IPv4Network(self.subnet.ipCidr)
            if IPv4Address(ipRange.start) <= ipAddress <= IPv4Address(ipRange.end)
        )

        for ipRange_ in self.values():

            ipAddresses_ = set(
                ipAddress.compressed
                for ipAddress
                in IPv4Network(self.subnet.ipCidr)
                if IPv4Address(ipRange_.start) <= ipAddress <= IPv4Address(ipRange_.end)
            )

            if ipAddresses.intersection(ipAddresses_):
                raise ValueError(f'Overlap of {ipRange_} '
                                 f'and {ipRange}.')

        super().__setitem__(key, ipRange)

    @property
    def subnet(self) -> Subnet:
        assert isinstance(self.parentResource, Subnet)
        return self.parentResource

    @abstractmethod
    def __str__(self):
        pass


class DynamicIpRanges(IpRanges):

    def __str__(self):
        return f'dynamic ip ranges on {self.subnet}'


class ReservedIpRanges(IpRanges):

    def __str__(self):
        return f'reserved ip ranges on {self.subnet}'


class IpRange(Resource, ABC):

    def __init__(
            self,
            subnet: Subnet,
            ipRanges: IpRanges,
    ):
        super().__init__(
            parentResource=subnet,
            resources=ipRanges,
        )
        self._start: IPv4Address = None
        self._end: IPv4Address = None
        self.forDhcp: bool = None

    @abstractmethod
    async def create(self) -> None:
        pass

    @property
    async def exists(self) -> bool:
        try:
            _ = await self.actualIpRange
        except ActualResourceDoesNotExistError:
            return False
        else:
            return True

    @property
    @abstractmethod
    async def actualIpRange(self) -> ActualIPRange:
        pass

    async def _get_actual_ip_range(self, ipRangeType) -> ActualIPRange:
        client = await self.maasServer.client
        for actualIpRange in await client.ip_ranges.list():
            if actualIpRange.subnet.cidr != self.subnet.ipCidr:
                continue
            if actualIpRange.type != ipRangeType:
                continue
            if actualIpRange.start_ip != self.start:
                continue
            if actualIpRange.end_ip != self.end:
                continue
            return actualIpRange
        raise ActualResourceDoesNotExistError()

    def from_config(self, config) -> None:
        self._start = IPv4Address(config['start'])
        self._end = IPv4Address(config['end'])
        assert isinstance(config['for_dhcp'], bool)
        if isinstance(self, ReservedIpRange) \
                and config['for_dhcp']:
            raise ValueError()
        elif isinstance(self, DynamicIpRange) \
                and not config['for_dhcp']:
            raise ValueError()
        self.forDhcp = config['for_dhcp']

    @property
    def start(self) -> str:
        return self._start.compressed

    @property
    def end(self) -> str:
        return self._end.compressed

    @property
    def subnet(self) -> Subnet:
        assert isinstance(self.parentResource, Subnet)
        return self.parentResource

    @property
    def maasServer(self) -> MaasServer:
        from ._maas_server import MaasServer
        assert isinstance(self.subnet.maasServer, MaasServer)
        return self.subnet.maasServer

    @abstractmethod
    def __str__(self):
        pass


class DynamicIpRange(IpRange):

    async def create(self) -> None:
        client = await self.maasServer.client
        await client.ip_ranges.create(
            self.start,
            self.end,
            type=IPRangeType.DYNAMIC,
            subnet=await self.subnet.actualSubnet,
        )

    @property
    async def actualIpRange(self) -> ActualIPRange:
        return await self._get_actual_ip_range(IPRangeType.DYNAMIC)

    def __str__(self):
        return f'dynamic ip range {self.start}-{self.end} on {self.subnet}'


class ReservedIpRange(IpRange):

    async def create(self) -> None:
        client = await self.maasServer.client
        await client.ip_ranges.create(
            self.start,
            self.end,
            type=IPRangeType.RESERVED,
            subnet=await self.subnet.actualSubnet,
        )

    @property
    async def actualIpRange(self) -> ActualIPRange:
        return await self._get_actual_ip_range(IPRangeType.RESERVED)

    def __str__(self):
        return f'reserved ip range {self.start}-{self.end} on {self.subnet}'


def _iter_complementary_ip_addresses(
    ipRangesConfig: List[dict],
    subnet: Subnet,
) -> List[dict]:

    # TODO: Cleanup this function...

    if not ipRangesConfig:
        return []

    subnetIpAddresses = [
        ipAddress
        for ipAddress
        in IPv4Network(subnet.ipCidr)
    ][1:-1]

    ipRangesConfig = sorted(
        ipRangesConfig,
        key=lambda ipRangeConfig: (IPv4Address(ipRangeConfig['start']),
                                   IPv4Address(ipRangeConfig['end']))
    )

    ipAddresses = []

    for ipRangeConfig in ipRangesConfig:

        startIpAddress = IPv4Address(ipRangeConfig['start'])
        endIpAddress = IPv4Address(ipRangeConfig['end'])

        ipAddresses.extend([
            ipAddress
            for ipAddress
            in subnetIpAddresses
            if startIpAddress <= ipAddress <= endIpAddress
        ])

    complementaryIpAddresses = sorted([
        IPv4Address(ipAddress)
        for ipAddress
        in set(subnetIpAddresses).difference(set(ipAddresses))
    ])

    startIpAddresses = [complementaryIpAddresses[0]]
    endIpAddresses = []
    iterator = zip(
        complementaryIpAddresses[:-1],
        complementaryIpAddresses[1:],
    )
    for complementaryIpAddress1, complementaryIpAddress2 in iterator:
        if complementaryIpAddress1 + 1 != complementaryIpAddress2:
            endIpAddresses.append(complementaryIpAddress1)
            startIpAddresses.append(complementaryIpAddress2)
    endIpAddresses.append(complementaryIpAddresses[-1])

    assert len(startIpAddresses) == len(endIpAddresses)

    complementaryIpRangesConfig = [
        {
            'start': startIpAddress.compressed,
            'end': endIpAddress.compressed,
            'for_dhcp': False,
        }
        for startIpAddress, endIpAddress
        in zip(startIpAddresses, endIpAddresses)
    ]

    return complementaryIpRangesConfig


class ActualResourceDoesNotExistError(Exception):
    pass
