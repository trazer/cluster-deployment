import aiohttp.client_exceptions
import maas.client
import maas.client.facade
import maas.client.viscera.controllers
import maas.client.viscera.fabrics
import os
import re
from ipaddress import IPv4Address
from ruamel.yaml import YAML
from typing import List

from clusterutils import (
    YamlSchemaValidator,
    async_retry,
)
from credentials import Secret
from proxmox import (
    Hypervisors,
    VirtualMachine,
)
from ._controllers import (
    RackControllers,
    RegionControllers,
)
from ._networks import Fabrics
from ._utils import (
    validate_email,
    validate_public_ssh_keys,
)

_CURRENT_PATH = os.path.split(__file__)[0]
_SCHEMA_FOLDER_NAME = '_schemas'
_SCRIPTS_FOLDER_NAME = '_scripts'


class MaasServer:

    def __init__(self, hypervisors: Hypervisors):
        self.hypervisors = hypervisors
        self._adminSecret: Secret = None
        self._adminEmail: str = None
        self._adminPublicSshKeys: List[str] = []
        self._dnsForwarders: List[IPv4Address] = []
        self.regionControllers = RegionControllers(
            parentResource=None,
            maasServer=self,
        )
        self.rackControllers = RackControllers(
            parentResource=None,
            maasServer=self,
        )
        self.fabrics = Fabrics(
            parentResource=None,
            maasServer=self,
        )
        self._client = None

    def from_config(
            self,
            controllerConfig: dict,
            networkConfig: dict,
    ) -> None:

        maasConfig = controllerConfig['maas']

        self._adminSecret = Secret(
            serviceName=maasConfig['admin_credentials']['service_name'],
            userName=maasConfig['admin_credentials']['user_name'],
        )

        validate_email(maasConfig['admin_email'])
        self._adminEmail = maasConfig['admin_email']

        validate_public_ssh_keys(maasConfig['admin_public_ssh_keys'])
        self._adminPublicSshKeys = maasConfig['admin_public_ssh_keys']

        self._dnsForwarders = list(map(IPv4Address, maasConfig['dns_forwarders']))

        self.regionControllers.from_config(
            controllerConfig['region_controllers']
        )
        self._ensure_there_is_one_primary_region_controller()

        self.rackControllers.from_config(
            controllerConfig['rack_controllers']
        )

        self.fabrics.from_config(
            networkConfig
        )

    def _ensure_there_is_one_primary_region_controller(self) -> None:
        if self.regionControllers.numberOfPrimaryRegions != 1:
            raise ValueError()

    @property
    async def client(self) -> maas.client.facade.Client:
        # We reuse the same client connection
        if self._client is None:
            self._client = await maas.client.connect(
                self.apiUrl,
                apikey=await self.apiKey,
            )
        return self._client

    @property
    def apiUrl(self) -> str:
        interface = self.regionControllers.primaryRegionController.apiServerInterface
        networkDevice = self.apiServerVirtualMachine.networkDevices[interface.networkDeviceName]
        networkInterface = networkDevice.networkInterfaces[interface.networkInterfaceName]
        return f'http://{networkInterface.ipAddress}:5240/MAAS'

    @property
    async def apiKey(self) -> str:
        virtualMachine = self.apiServerVirtualMachine
        SCRIPT_FILE_NAME = 'get-api-key.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            SCRIPT_FILE_NAME,
        )
        environment = {
            'ROOT_PASSWORD': self._adminSecret.value,
            'ADMIN_USER_NAME': self._adminSecret.userName,
        }
        output, errorOutput = await virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{SCRIPT_FILE_NAME}',
            environment=environment,
        )
        output = output.strip('\n')
        assert re.search(r'^[A-Za-z0-9]{18}:[A-Za-z0-9]{18}:[A-Za-z0-9]{32}$', output) is not None
        return output

    @property
    def adminUserName(self) -> str:
        return self._adminSecret.userName

    @property
    def apiServerVirtualMachine(self) -> VirtualMachine:
        return self.regionControllers.primaryRegionController.virtualMachine

    @property
    def _dnsForwarderIpAddresses(self):
        return [ipv4address.exploded for ipv4address in self._dnsForwarders]

    @property
    async def actualFabrics(self) -> maas.client.viscera.fabrics.Fabrics:
        client = await self.client
        return await client.fabrics.list()

    @property
    async def actualRackControllers(self) -> maas.client.viscera.controllers.RackControllers:
        client = await self.client
        return await client.rack_controllers.list()


def load_maas_server(
        maasServer: MaasServer,
        maasControllersFilePaths: List[str],
        maasNetworksFilePaths: List[str],
) -> None:

    controllerConfig = {}
    _load_config(
        filePaths=maasControllersFilePaths,
        schemaFileName='maas_controllers_schema.yaml',
        config=controllerConfig,
    )

    networkConfig = {}
    _load_config(
        filePaths=maasNetworksFilePaths,
        schemaFileName='maas_network_schema.yaml',
        config=networkConfig,
    )

    maasServer.from_config(
        controllerConfig=controllerConfig,
        networkConfig=networkConfig,
    )


def _load_config(filePaths: List[str], schemaFileName: str, config: dict) -> None:

    for filePath in filePaths:
        _validate_maas_schema(
            filePath,
            schemaFileName,
        )

    assert len(filePaths) == 1
    yaml = YAML(typ='safe')
    with open(filePaths[0], 'rt') as file:
        config.update(yaml.load(file))


def _validate_maas_schema(filePath: str, schemaFileName: str) -> None:
    schemaFilePath = os.path.join(
        _CURRENT_PATH,
        _SCHEMA_FOLDER_NAME,
        schemaFileName,
    )
    yamlSchemaValidator = YamlSchemaValidator(
        source_file=filePath,
        schema_files=[schemaFilePath]
    )
    yamlSchemaValidator.validate(raise_exception=True)
