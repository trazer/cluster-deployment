import asyncio
import functools
import os
from typing import (
    AsyncGenerator,
    List,
    Union,
)

import maas.client.viscera.fabrics
import maas.client.viscera.subnets
import maas.client.viscera.vlans

from proxmox import (
    Hypervisors,
    load_virtual_machines_from_config,
)
from ._controllers import (
    RackController,
    RegionController,
)
from ._maas_server import (
    MaasServer,
    load_maas_server,
)
from ._networks import Fabric

_CURRENT_PATH = os.path.split(__file__)[0]
_SCHEMA_FOLDER_NAME= '_schemas'
_SCRIPTS_FOLDER_NAME = '_scripts'


def configure_maas(
        virtualMachinesFilePaths: List[str],
        maasControllersFilePaths: List[str],
        maasNetworksFilePaths: List[str],
        forceReconfiguration: bool,
) -> None:

    assert not forceReconfiguration, 'Not implemented'

    hypervisors = Hypervisors()
    load_virtual_machines_from_config(
        virtualMachinesFilePaths,
        hypervisors,
    )

    maasServer = MaasServer(hypervisors)
    load_maas_server(
        maasServer,
        maasControllersFilePaths,
        maasNetworksFilePaths,
    )

    asyncio.run(_install_dependencies_on_controllers(
        maasServer
    ))

    asyncio.run(_delete_unused_subnets(
        maasServer
    ))

    asyncio.run(_create_fabrics(
        maasServer
    ))

    asyncio.run(_configure_existing_fabrics(
        maasServer
    ))

    asyncio.run(_restart_controllers(
        maasServer
    ))

    assert False, 'Fix this workaround!'
    maasServer._client = None

    asyncio.run(_configure_controllers_interfaces(
        maasServer
    ))

    asyncio.run(_delete_unused_fabrics(
        maasServer
    ))


# ------------------------------------------------


async def _install_dependencies_on_controllers(
        maasServer: MaasServer
) -> None:
    controllers = []
    for rackController in maasServer.rackControllers.values():
        controllers.append(rackController)
    await asyncio.gather(*list(map(
        _install_dependencies_on_controller,
        controllers
    )))


async def _install_dependencies_on_controller(
        controller: Union[RegionController, RackController],
) -> None:
    print(f'Installing dependencies on {controller}.')
    SCRIPT_FILE_NAME = 'install-dependencies-on-rack.sh'
    localFilePath = os.path.join(
        _CURRENT_PATH,
        _SCRIPTS_FOLDER_NAME,
        SCRIPT_FILE_NAME,
    )
    environment = {
        'ROOT_PASSWORD': controller.virtualMachine.secret.value,
    }
    output, errorOutput = await controller.virtualMachine.execute_local_script_on_remote_async(
        localFilePath=localFilePath,
        remoteFilePath=f'/tmp/{SCRIPT_FILE_NAME}',
        environment=environment,
    )


# ------------------------------------------------


async def _delete_unused_subnets(
        maasServer: MaasServer
) -> None:
    for actualFabric in await maasServer.actualFabrics:
        await asyncio.gather(*list(map(
            functools.partial(_delete_unused_subnet, maasServer, actualFabric),
            actualFabric.vlans
        )))


async def _delete_unused_subnet(
        maasServer: MaasServer,
        actualFabric: maas.client.viscera.fabrics.Fabric,
        actualVlan: maas.client.viscera.vlans.Vlan,
) -> None:

    print(f'Checking for unused subnets on '
          f'vlan {str(actualVlan.vid)} of '
          f'fabric {actualFabric.name}.')

    MSG = (
        f'Deleting unused subnet {{cidr}} '
        f'on vlan {str(actualVlan.vid)} of '
        f'fabric {actualFabric.name}.'
    )

    ITERATOR_ARGS = (maasServer, actualFabric, actualVlan)

    if actualFabric.name not in maasServer.fabrics.fabricNames:
        async for actualSubnet in _iter_actual_subnets_filtered_by_vlan(*ITERATOR_ARGS):
            print(MSG.format(cidr=actualSubnet.cidr))
            await actualSubnet.delete()

    elif actualFabric.name in maasServer.fabrics.fabricNames \
            and actualVlan.vid not in maasServer.fabrics[actualFabric.name].vlans.vids:
        async for actualSubnet in _iter_actual_subnets_filtered_by_vlan(*ITERATOR_ARGS):
            print(MSG.format(cidr=actualSubnet.cidr))
            await actualSubnet.delete()

    elif actualFabric.name in maasServer.fabrics.fabricNames \
            and actualVlan.vid in maasServer.fabrics[actualFabric.name].vlans.vids:
        fabric = maasServer.fabrics[actualFabric.name]
        vlan = fabric.vlans.by_vid(actualVlan.vid)
        async for actualSubnet in _iter_actual_subnets_filtered_by_vlan(*ITERATOR_ARGS):
            if actualSubnet.cidr in vlan.subnets.ipCidrs:
                continue
            print(MSG.format(cidr=actualSubnet.cidr))
            await actualSubnet.delete()


async def _iter_actual_subnets_filtered_by_vlan(
        maasServer: MaasServer,
        actualFabric: maas.client.viscera.fabrics.Fabric,
        actualVlan: maas.client.viscera.vlans.Vlan,
) -> AsyncGenerator[maas.client.viscera.subnets.Subnet, None]:
    client = await maasServer.client
    for actualSubnet in await client.subnets.list():
        actualFabric_ = actualSubnet.vlan.fabric
        if not actualFabric_.loaded:
            await actualFabric_.refresh()
            assert actualFabric_.loaded
        if actualFabric_.name != actualFabric.name:
            continue
        if actualSubnet.vlan.vid != actualVlan.vid:
            continue
        yield actualSubnet


# ------------------------------------------------


async def _create_fabrics(
        maasServer: MaasServer
) -> None:
    await asyncio.gather(*list(map(
        _create_fabric,
        maasServer.fabrics.values()
    )))


async def _create_fabric(
        fabric: Fabric
) -> None:

    print(f'Checking if {fabric} exists.')
    if await fabric.exists:
        print(f'{fabric} exists.')
        return

    print(f'{fabric} does not exist...creating it.')
    await fabric.create()


# ------------------------------------------------


async def _configure_existing_fabrics(
        maasServer: MaasServer
) -> None:
    await asyncio.gather(*list(map(
        _configure_existing_fabric,
        maasServer.fabrics.values()
    )))


async def _configure_existing_fabric(
        fabric: Fabric
) -> None:
    print(f'Checking if {fabric} exists.')
    if not await fabric.exists:
        return
    print(f'{fabric} exists...configuring it.')
    await fabric.configure()


# ------------------------------------------------


async def _restart_controllers(
        maasServer: MaasServer
) -> None:
    controllers = []
    for rackController in maasServer.rackControllers.values():
        controllers.append(rackController)
    for regionController in maasServer.regionControllers.values():
        controllers.append(regionController)
    await asyncio.gather(*list(map(
        _restart_controller,
        controllers
    )))


async def _restart_controller(
        controller: Union[RegionController, RackController],
) -> None:
    print(f'Restarting {controller}.')
    await controller.virtualMachine.restart_async()


# ------------------------------------------------


async def _configure_controllers_interfaces(
        maasServer: MaasServer
) -> None:
    controllers = []
    for regionController in maasServer.regionControllers.values():
        controllers.append(regionController)
    for rackController in maasServer.rackControllers.values():
        controllers.append(rackController)
    await asyncio.gather(*list(map(
        _configure_controller_interfaces,
        controllers
    )))


async def _configure_controller_interfaces(
        controller: Union[RegionController, RackController],
) -> None:

    print(f'Configuring interfaces of {controller}.')

    if not await controller.exists:
        raise RuntimeError()

    for interface in controller.interfaces.values():
        if not await interface.exists:
            raise RuntimeError()
        await interface.configure()


# ------------------------------------------------


async def _delete_unused_fabrics(
        maasServer: MaasServer
) -> None:
    await asyncio.gather(*list(map(
        functools.partial(_delete_unused_fabric, maasServer),
        await maasServer.actualFabrics
    )))


async def _delete_unused_fabric(
        maasServer: MaasServer,
        actualFabric: maas.client.viscera.fabrics.Fabric,
) -> None:
    try:
        _ = maasServer.fabrics[actualFabric.name]
    except KeyError:
        print(f'Deleting {actualFabric.name}.')
        await actualFabric.delete()
