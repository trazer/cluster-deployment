from __future__ import annotations

import os
import re
from abc import (
    ABC,
    abstractmethod,
)
from ipaddress import IPv4Address
from typing import (
    List,
    Union,
)

from clusterutils import (
    async_retry,
    retry,
)
from credentials import Secret
from proxmox import (
    Hypervisors,
    VirtualMachine,
)
from ._utils import (
    PackageInfo,
    ensure_only_one_interface_for_api_server,
    get_interface_for_api_server,
    validate_email,
    validate_public_ssh_keys,
)

_CURRENT_PATH = os.path.split(__file__)[0]
_SCRIPTS_FOLDER_NAME = '_scripts'


class MaasInstallersBase(ABC):

    @abstractmethod
    def from_config(self, *args) -> None:
        pass

    def __len__(self) -> int:
        return len(self.virtualMachineNames)

    @property
    @abstractmethod
    def virtualMachineNames(self) -> List[str]:
        pass


class MaasRegionControllerInstallers(MaasInstallersBase):

    def __init__(self, hypervisors: Hypervisors):
        self._hypervisors = hypervisors
        self._adminSecret: Secret = None
        self._adminEmail: str = None
        self._adminPublicSshKeys: List[str] = []
        self._dnsForwarders: List[IPv4Address] = []
        self._maasRegionControllerInstallers: List[MaasRegionControllerInstaller] = []

    def from_config(
            self,
            regionControllersConfig: dict,
            maasConfig: dict,
    ) -> None:

        self._adminSecret = Secret(
            serviceName=maasConfig['admin_credentials']['service_name'],
            userName=maasConfig['admin_credentials']['user_name'],
        )

        self.adminEmail = maasConfig['admin_email']

        self.adminPublicSshKeys = maasConfig['admin_public_ssh_keys']

        self.dnsForwarders = maasConfig['dns_forwarders']

        for virtualMachineFromConfig in regionControllersConfig['virtual_machines']:
            _ensure_no_virtual_machine_duplicates(
                virtualMachineFromConfig['name'],
                self.virtualMachineNames
            )
            ensure_only_one_interface_for_api_server(virtualMachineFromConfig)
            interfaceForApiServerFromConfig = get_interface_for_api_server(virtualMachineFromConfig)
            maasRegionControllerInstaller = MaasRegionControllerInstaller(
                maasRegionControllerInstallers=self,
                virtualMachine=self._hypervisors.get_virtual_machine_by_name(virtualMachineFromConfig['name']),
                networkDeviceName=interfaceForApiServerFromConfig['network_device_name'],
                interfaceName=interfaceForApiServerFromConfig['network_interface_name'],
            )
            self._maasRegionControllerInstallers.append(maasRegionControllerInstaller)

    @property
    def adminEmail(self) -> str:
        assert self._adminEmail is not None
        return self._adminEmail

    @adminEmail.setter
    def adminEmail(self, value):
        validate_email(value)
        self._adminEmail = value

    @property
    def adminUserName(self):
        return self._adminSecret.userName

    @property
    def adminPassword(self):
        return self._adminSecret.value

    @property
    def adminPublicSshKeys(self) -> List[str]:
        return self._adminPublicSshKeys

    @adminPublicSshKeys.setter
    def adminPublicSshKeys(self, values: List[str]):
        validate_public_ssh_keys(values)
        self._adminPublicSshKeys = values

    @property
    def dnsForwarders(self) -> List[str]:
        return [ipv4address.exploded for ipv4address in self._dnsForwarders]

    @dnsForwarders.setter
    def dnsForwarders(self, values: List[str]):
        self._dnsForwarders = list(map(IPv4Address, values))

    @property
    def virtualMachineNames(self):
        return [
            maasRegionControllerInstaller.virtualMachine.name
            for maasRegionControllerInstaller
            in self._maasRegionControllerInstallers
        ]

    @property
    def maasRegionControllerInstaller(self) -> MaasRegionControllerInstaller:
        assert len(self) == 1
        return next(iter(self))

    def __iter__(self):
        yield from self._maasRegionControllerInstallers


class MaasRackControllerInstallers(MaasInstallersBase):

    def __init__(self, hypervisors: Hypervisors):
        self._hypervisors = hypervisors
        self._maasRackControllerInstallers: List[MaasRackControllerInstaller] = []

    def from_config(self, fromConfig):
        for virtualMachineFromConfig in fromConfig['virtual_machines']:
            _ensure_no_virtual_machine_duplicates(
                virtualMachineFromConfig['name'],
                self.virtualMachineNames
            )
            maasRackControllerInstaller = MaasRackControllerInstaller(
                maasRackControllerInstallers=self,
                virtualMachine=self._hypervisors.get_virtual_machine_by_name(virtualMachineFromConfig['name']),
            )
            self._maasRackControllerInstallers.append(maasRackControllerInstaller)

    @property
    def virtualMachineNames(self):
        return [
            maasRackControllerInstaller.virtualMachine.name
            for maasRackControllerInstaller
            in self._maasRackControllerInstallers
        ]

    @property
    def maasRackControllerInstaller(self) -> MaasRackControllerInstaller:
        assert len(self) == 1
        return next(iter(self))

    def __iter__(self):
        yield from self._maasRackControllerInstallers


class MaasDatabaseInstallers(MaasInstallersBase):

    def __init__(self, hypervisors: Hypervisors):
        self._hypervisors = hypervisors
        self._maasDatabaseInstallers: List[MaasDatabaseInstaller] = []

    def from_config(self, fromConfig):
        for virtualMachineFromConfig in fromConfig['virtual_machines']:
            _ensure_no_virtual_machine_duplicates(
                virtualMachineFromConfig['name'],
                self.virtualMachineNames
            )
            maasDatabaseInstaller = MaasDatabaseInstaller(
                maasDatabaseInstallers=self,
                virtualMachine=self._hypervisors.get_virtual_machine_by_name(virtualMachineFromConfig['name']),
            )
            self._maasDatabaseInstallers.append(maasDatabaseInstaller)

    @property
    def virtualMachineNames(self):
        return [
            maasDatabaseInstaller.virtualMachine.name
            for maasDatabaseInstaller
            in self._maasDatabaseInstallers
        ]

    @property
    def maasDatabaseInstaller(self) -> MaasDatabaseInstaller:
        assert len(self) == 1
        return next(iter(self))

    def __iter__(self):
        yield from self._maasDatabaseInstallers


# ------------------------------------------------


class MaasInstallerBase(ABC):

    @property
    @abstractmethod
    def installed(self) -> bool:
        pass

    @abstractmethod
    def install(self) -> None:
        pass

    @property
    @abstractmethod
    def isLatestStableRelease(self) -> bool:
        pass

    @abstractmethod
    def uninstall(self) -> None:
        pass

    @property
    @abstractmethod
    def configured(self) -> bool:
        pass

    @abstractmethod
    def configure(self, *args, **kwargs) -> None:
        pass

    @abstractmethod
    def __add__(
            self,
            other
    ) -> Union[
        MaasRegionRackDatabaseInstaller,
        MaasRegionDatabaseInstaller,
        MaasRegionRackInstaller
    ]:
        pass

    @abstractmethod
    def __str__(self):
        pass


class MaasRegionControllerInstaller(MaasInstallerBase):

    def __init__(
            self,
            maasRegionControllerInstallers: MaasRegionControllerInstallers,
            virtualMachine: VirtualMachine,
            networkDeviceName: str,
            interfaceName: str,
    ):
        self.maasRegionControllerInstallers = maasRegionControllerInstallers
        self.virtualMachine = virtualMachine
        self._networkDeviceName = None
        self._interfaceName = None
        self.networkDeviceName = networkDeviceName
        self.interfaceName = interfaceName

    @property
    def networkDeviceName(self) -> str:
        assert self._networkDeviceName is not None
        return self._networkDeviceName

    @networkDeviceName.setter
    def networkDeviceName(self, value: str):
        try:
            self.virtualMachine.networkDevices.by_name(value)
        except KeyError as exception:
            raise KeyError(
                f'Network device "{value}" '
                f'does not exist in {self.virtualMachine}.'
            ) from exception
        else:
            self._networkDeviceName = value

    @property
    def interfaceName(self) -> str:
        assert self._interfaceName is not None
        return self._interfaceName

    @interfaceName.setter
    def interfaceName(self, value: str):
        networkDevice = self.virtualMachine.networkDevices.by_name(
            self.networkDeviceName
        )
        try:
            networkDevice.networkInterfaces.by_name(value)
        except KeyError as exception:
            raise KeyError(
                f'Interface "{value}" '
                f'does not exist in {networkDevice}.'
            ) from exception
        else:
            self._interfaceName = value

    @property
    def installed(self) -> bool:
        raise NotImplementedError()

    def install(self) -> None:
        raise NotImplementedError()

    def uninstall(self) -> None:
        raise NotImplementedError()

    @property
    def isLatestStableRelease(self) -> bool:
        raise NotImplementedError()

    @property
    def configured(self) -> bool:
        raise NotImplementedError()

    def configure(self) -> None:
        raise NotImplementedError()

    def __add__(self, other):

        if isinstance(other, MaasDatabaseInstaller):
            maasDatabaseInstaller = other
            maasDatabaseInstallers = maasDatabaseInstaller.maasDatabaseInstallers
            if len(maasDatabaseInstallers.virtualMachineNames) > 1:
                raise ValueError()
            if maasDatabaseInstallers.virtualMachineNames[0] != self.virtualMachine.name:
                raise ValueError()
            maasRegionDatabaseInstaller = MaasRegionDatabaseInstaller(
                maasRegionControllerInstaller=self,
                maasDatabaseInstaller=maasDatabaseInstaller,
                virtualMachine=self.virtualMachine,
            )
            return maasRegionDatabaseInstaller

        elif isinstance(other, MaasRackControllerInstaller):
            maasRackControllerInstaller = other
            maasRackControllerInstallers = maasRackControllerInstaller.maasRackControllerInstallers
            if len(maasRackControllerInstallers.virtualMachineNames) > 1:
                raise ValueError()
            if maasRackControllerInstallers.virtualMachineNames[0] != self.virtualMachine.name:
                raise ValueError()
            maasRegionRackInstaller = MaasRegionRackInstaller(
                maasRegionControllerInstaller=self,
                maasRackControllerInstaller=maasRackControllerInstaller,
                virtualMachine=self.virtualMachine,
            )
            return maasRegionRackInstaller

        else:
            raise TypeError(
                f'Cannot combine {type(self)} with {type(other)}.'
            )

    def __str__(self):
        return f'region on {self.virtualMachine}'


class MaasRackControllerInstaller(MaasInstallerBase):

    def __init__(
            self,
            maasRackControllerInstallers: MaasRackControllerInstallers,
            virtualMachine: VirtualMachine,
    ):
        self.maasRackControllerInstallers = maasRackControllerInstallers
        self.virtualMachine = virtualMachine
        self._packageInfo = PackageInfo(
            virtualMachine=self.virtualMachine,
            packageName='maas-rack-controller',
            packageRepository='ppa:maas/stable',
        )

    @property
    async def installed(self) -> bool:
        assert self.virtualMachine.on
        return await self._packageInfo.installed \
               and await self._packageInfo.successfulInstallation

    @async_retry(exceptions=[RuntimeError])
    async def install(self) -> None:
        INSTALL_SCRIPT_FILE_NAME = 'install-rack.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            INSTALL_SCRIPT_FILE_NAME
        )
        output, errorOutput = await self.virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{INSTALL_SCRIPT_FILE_NAME}',
            environment={'ROOT_PASSWORD': self.virtualMachine.secret.value},
        )

    def uninstall(self) -> None:
        raise NotImplementedError()

    @property
    async def isLatestStableRelease(self) -> bool:
        assert await self._packageInfo.installed
        return await self._packageInfo.installedVersion == await self._packageInfo.candidateVersion

    @property
    def configured(self) -> bool:
        raise NotImplementedError()

    @retry(exceptions=[RuntimeError])
    def configure(
            self,
            maasRegionDatabaseInstaller: MaasRegionDatabaseInstaller
    ) -> None:
        SCRIPT_FILE_NAME = 'configure-maas-rack.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            SCRIPT_FILE_NAME,
        )
        environment = {
            'ROOT_PASSWORD': self.virtualMachine.secret.value,
            'MAAS_URL': maasRegionDatabaseInstaller.maasUrl,
            'SHARED_SECRET': maasRegionDatabaseInstaller.sharedSecret,
        }
        output, errorOutput = self.virtualMachine.execute_local_script_on_remote(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{SCRIPT_FILE_NAME}',
            environment=environment,
        )

    def __add__(self, other):

        if isinstance(other, MaasRegionControllerInstaller):
            maasRegionControllerInstaller = other
            maasRegionControllerInstallers = maasRegionControllerInstaller.maasRegionControllerInstallers
            if len(maasRegionControllerInstallers) > 1:
                raise ValueError()
            if len(self.maasRackControllerInstallers) > 1:
                raise ValueError()
            if maasRegionControllerInstallers.virtualMachineNames[0] != self.virtualMachine.name:
                raise ValueError()
            maasRegionRackInstaller = MaasRegionRackInstaller(
                maasRegionControllerInstaller=maasRegionControllerInstaller,
                maasRackControllerInstaller=self,
                virtualMachine=self.virtualMachine,
            )
            return maasRegionRackInstaller

        elif isinstance(other, MaasRegionDatabaseInstaller):
            maasRegionDatabaseInstaller = other
            if len(self.maasRackControllerInstallers) > 1:
                raise ValueError()
            if maasRegionDatabaseInstaller.virtualMachine.name != self.virtualMachine.name:
                raise ValueError()
            maasRegionRackDatabaseInstaller = MaasRegionRackDatabaseInstaller(
                maasRegionControllerInstaller=maasRegionDatabaseInstaller.maasRegionControllerInstaller,
                maasDatabaseInstaller=maasRegionDatabaseInstaller.maasDatabaseInstaller,
                maasRackControllerInstaller=self,
                virtualMachine=self.virtualMachine,
            )
            return maasRegionRackDatabaseInstaller

        else:
            raise TypeError(
                f'Cannot combine {type(self)} with {type(other)}.'
            )

    def __str__(self):
        return f'rack on {self.virtualMachine}'


class MaasDatabaseInstaller(MaasInstallerBase):

    def __init__(
            self,
            maasDatabaseInstallers: MaasDatabaseInstallers,
            virtualMachine: VirtualMachine,
    ):
        self.maasDatabaseInstallers = maasDatabaseInstallers
        self.virtualMachine = virtualMachine

    @property
    def installed(self) -> bool:
        raise NotImplementedError()

    def install(self) -> None:
        raise NotImplementedError()

    def uninstall(self) -> None:
        raise NotImplementedError()

    @property
    def isLatestStableRelease(self) -> bool:
        raise NotImplementedError()

    @property
    def configured(self) -> bool:
        raise NotImplementedError()

    def configure(self) -> None:
        raise NotImplementedError()

    def __add__(self, other):
        raise NotImplementedError()

    def __str__(self):
        return f'database on {self.virtualMachine}'


class MaasRegionRackDatabaseInstaller(MaasInstallerBase):

    def __init__(
            self,
            maasRegionControllerInstaller: MaasRegionControllerInstaller,
            maasDatabaseInstaller: MaasDatabaseInstaller,
            maasRackControllerInstaller: MaasRackControllerInstaller,
            virtualMachine: VirtualMachine,
    ):
        self._maasRegionControllerInstaller = maasRegionControllerInstaller
        self._maasDatabaseInstaller = maasDatabaseInstaller
        self._maasRackControllerInstaller = maasRackControllerInstaller
        self._virtualMachine = virtualMachine

    @property
    def installed(self) -> bool:
        raise NotImplementedError()

    def install(self) -> None:
        raise NotImplementedError()

    def uninstall(self) -> None:
        raise NotImplementedError()

    @property
    def isLatestStableRelease(self) -> bool:
        raise NotImplementedError()

    def __add__(self, other):
        raise NotImplementedError()


class MaasRegionDatabaseInstaller(MaasInstallerBase):

    def __init__(
            self,
            maasRegionControllerInstaller: MaasRegionControllerInstaller,
            maasDatabaseInstaller: MaasDatabaseInstaller,
            virtualMachine: VirtualMachine,
    ):
        self.maasRegionControllerInstaller = maasRegionControllerInstaller
        self.maasDatabaseInstaller = maasDatabaseInstaller
        self.virtualMachine = virtualMachine
        self._packageInfo = PackageInfo(
            virtualMachine=self.virtualMachine,
            packageName='maas-region-controller',
            packageRepository='ppa:maas/stable',
        )

    @property
    async def installed(self) -> bool:
        assert self.virtualMachine.on
        return await self._packageInfo.installed \
               and await self._packageInfo.successfulInstallation

    @property
    async def isLatestStableRelease(self) -> bool:
        assert await self._packageInfo.installed
        return await self._packageInfo.installedVersion == await self._packageInfo.candidateVersion

    @async_retry(exceptions=[RuntimeError])
    async def install(self) -> None:
        INSTALL_SCRIPT_FILE_NAME = 'install-region-database.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            INSTALL_SCRIPT_FILE_NAME
        )
        output, errorOutput = await self.virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{INSTALL_SCRIPT_FILE_NAME}',
            environment={'ROOT_PASSWORD': self.virtualMachine.secret.value},
        )

    def uninstall(self) -> None:
        raise NotImplementedError()

    def __add__(self, other):
        raise NotImplementedError()

    @property
    def configured(self) -> bool:
        raise NotImplementedError()

    @retry(exceptions=[RuntimeError])
    def configure(self) -> None:
        SCRIPT_FILE_NAME = 'configure-maas-region.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            SCRIPT_FILE_NAME,
        )
        maasRegionControllerInstallers = self.maasRegionControllerInstaller.maasRegionControllerInstallers
        environment = {
            'ROOT_PASSWORD': self.virtualMachine.secret.value,
            'ADMIN_USER_NAME': maasRegionControllerInstallers.adminUserName,
            'ADMIN_PASSWORD': maasRegionControllerInstallers.adminPassword,
            'ADMIN_E_MAIL': maasRegionControllerInstallers.adminEmail,
            'MAAS_URL': self.maasUrl,
            'DNS_FORWARDERS': ' '.join(maasRegionControllerInstallers.dnsForwarders),
        }
        output, errorOutput = self.virtualMachine.execute_local_script_on_remote(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{SCRIPT_FILE_NAME}',
            environment=environment,
        )

        for adminPublicSshKey in maasRegionControllerInstallers.adminPublicSshKeys:
            self._add_admin_public_ssh_key(adminPublicSshKey)

    @retry(exceptions=[RuntimeError])
    def _add_admin_public_ssh_key(self, adminPublicSshKey: str) -> None:
        SCRIPT_FILE_NAME = 'add-admin-public-ssh-key-to-region.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            SCRIPT_FILE_NAME,
        )
        maasRegionControllerInstallers = self.maasRegionControllerInstaller.maasRegionControllerInstallers
        environment = {
            'ROOT_PASSWORD': self.virtualMachine.secret.value,
            'ADMIN_USER_NAME': maasRegionControllerInstallers.adminUserName,
            'ADMIN_PASSWORD': maasRegionControllerInstallers.adminPassword,
            'ADMIN_E_MAIL': maasRegionControllerInstallers.adminEmail,
            'MAAS_URL': self.maasUrl,
            'PUBLIC_SSH_KEY': adminPublicSshKey,
        }
        output, errorOutput = self.virtualMachine.execute_local_script_on_remote(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{SCRIPT_FILE_NAME}',
            environment=environment,
        )

    @property
    def sharedSecret(self) -> str:
        SCRIPT_FILE_NAME = 'get-region-shared-secret.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            SCRIPT_FILE_NAME,
        )
        output, errorOutput = self.virtualMachine.execute_local_script_on_remote(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{SCRIPT_FILE_NAME}',
        )
        assert re.search(r'^[a-z0-9]{32}$', output) is not None
        return output

    @property
    def maasUrl(self) -> str:
        networkDevice = self.virtualMachine.networkDevices.by_name(
            self.maasRegionControllerInstaller.networkDeviceName
        )
        networkInterface = networkDevice.networkInterfaces.by_name(
            self.maasRegionControllerInstaller.interfaceName
        )
        return f'http://{networkInterface.ipAddress}:5240/MAAS'

    def __str__(self):
        return f'region+database on {self.virtualMachine}'


class MaasRegionRackInstaller(MaasInstallerBase):

    def __init__(
            self,
            maasRegionControllerInstaller: MaasRegionControllerInstaller,
            maasRackControllerInstaller: MaasRackControllerInstaller,
            virtualMachine: VirtualMachine,
    ):
        self._maasRegionControllerInstaller = maasRegionControllerInstaller
        self._maasRackControllerInstaller = maasRackControllerInstaller
        self._virtualMachine = virtualMachine

    @property
    def installed(self) -> bool:
        raise NotImplementedError()

    def install(self) -> None:
        raise NotImplementedError()

    def uninstall(self) -> None:
        raise NotImplementedError()

    @property
    def isLatestStableRelease(self) -> bool:
        raise NotImplementedError()

    def __add__(self, other):
        raise NotImplementedError()


# ------------------------------------------------


def _ensure_no_virtual_machine_duplicates(virtualMachineName, virtualMachineNames):
    if virtualMachineName in virtualMachineNames:
        raise ValueError(
            f'Duplicate virtual machine '
            f'{virtualMachineName} '
            f'in region controllers.'
        )
