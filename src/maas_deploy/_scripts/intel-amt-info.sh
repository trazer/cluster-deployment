#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

amttool "${POWER_ADDRESS}" info
