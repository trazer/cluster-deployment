#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "y" | amttool "${POWER_ADDRESS}" powerup pxe
