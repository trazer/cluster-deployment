#!/usr/bin/env bash

echo "${ROOT_PASSWORD}" | \
    maas apikey --username "${ADMIN_USER_NAME}"
