#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "${ROOT_PASSWORD}" | sudo -S add-apt-repository -y ppa:maas/stable
echo "${ROOT_PASSWORD}" | sudo -S apt update
echo "${ROOT_PASSWORD}" | sudo -S apt install -y maas-rack-controller
