#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "${ROOT_PASSWORD}" | \
    sudo -S add-apt-repository -y "deb http://mirrors.kernel.org/ubuntu bionic main universe"

echo "${ROOT_PASSWORD}" | \
    sudo -S apt-get update

echo "${ROOT_PASSWORD}" | \
    sudo -S apt-get install -y amtterm

echo "${ROOT_PASSWORD}" | \
    sudo -S apt-get install -y wsmancli
