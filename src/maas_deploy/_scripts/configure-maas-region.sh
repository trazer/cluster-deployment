#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "${ROOT_PASSWORD}" | \
    sudo -S maas init \
    --admin-username "${ADMIN_USER_NAME}" \
    --admin-password "${ADMIN_PASSWORD}" \
    --admin-email "${ADMIN_E_MAIL}"

API_KEY=$(echo "${ROOT_PASSWORD}" | maas apikey --username "${ADMIN_USER_NAME}")

echo "${ROOT_PASSWORD}" | \
    sudo -S maas login \
    "${ADMIN_USER_NAME}" \
    "${MAAS_URL}" \
    "${API_KEY}"

maas "${ADMIN_USER_NAME}" maas set-config name=upstream_dns value="${DNS_FORWARDERS}"

maas "${ADMIN_USER_NAME}" maas set-config name=completed_intro value=True
