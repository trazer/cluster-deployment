#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

API_KEY=$(echo "${ROOT_PASSWORD}" | maas apikey --username "${ADMIN_USER_NAME}")

echo "${ROOT_PASSWORD}" | \
    sudo -S maas login \
    "${ADMIN_USER_NAME}" \
    "${MAAS_URL}" \
    "${API_KEY}"

maas "${ADMIN_USER_NAME}" sshkeys create "key=${PUBLIC_SSH_KEY}"
