#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "${ROOT_PASSWORD}" | sudo -S add-apt-repository -y "${PACKAGE_REPOSITORY}"
# Use apt-get instead of apt in bash script
# see https://askubuntu.com/questions/990823/apt-gives-unstable-cli-interface-warning
echo "${ROOT_PASSWORD}" | sudo -S apt-get update
echo "${ROOT_PASSWORD}" | sudo -S apt-cache policy "${PACKAGE_NAME}"
