#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

maas "${ADMIN_USER_NAME}" rack-controller list-boot-images "${RACK_SYSTEM_ID}"
