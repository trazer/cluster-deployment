#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "${ROOT_PASSWORD}" | \
    sudo -S maas login \
    "${ADMIN_USER_NAME}" \
    "${MAAS_URL}" \
    "${API_KEY}"

# See https://stackoverflow.com/a/13864829
if [ -z ${SECONDARY_RACK_SYSTEM_ID+x} ]
then
    maas root vlan update "${FABRIC_ID}" "${VLAN_VID}" \
        primary_rack="${PRIMARY_RACK_SYSTEM_ID}" \
        secondary_rack="" \
        dhcp_on=True
else
    maas root vlan update "${FABRIC_ID}" "${VLAN_VID}" \
        primary_rack="${PRIMARY_RACK_SYSTEM_ID}" \
        secondary_rack="${SECONDARY_RACK_SYSTEM_ID}" \
        dhcp_on=True
fi
