#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

ipmipower --stat \
    --hostname="${POWER_ADDRESS}" \
    --username="${POWER_USER_NAME}" \
    --password="${POWER_PASSWORD}" \
    --privilege-level=ADMIN \
    --driver-type=LAN_2_0 \
    --session-timeout=60000
