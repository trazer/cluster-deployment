#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "${ROOT_PASSWORD}" | \
    sudo -S maas login \
    "${ADMIN_USER_NAME}" \
    "${MAAS_URL}" \
    "${API_KEY}"

# TODO: do not hardcode "root"
maas root vlan update "${FABRIC_ID}" "${VLAN_VID}" dhcp_on=False
