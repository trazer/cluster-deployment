#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "${ROOT_PASSWORD}" | \
    sudo -S maas-rack register \
    --url "${MAAS_URL}" \
    --secret "${SHARED_SECRET}"
