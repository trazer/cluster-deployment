#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

dpkg-query --show --showformat="${FORMAT}" "${PACKAGE_NAME}"
