import operator
import os
import re
from ipaddress import IPv4Address
from typing import (
    List,
    Tuple,
)

from maas.client.bones import CallError as MaasCallError
from maas.client.enum import NodeStatus
from maas.client.viscera.block_devices import BlockDevice as ActualBlockDevice
from maas.client.viscera.machines import Machine as ActualMachine
from maas.client.viscera.partitions import Partition as ActualPartition

from clusterutils import async_retry
from clusterutils._regexp_patterns import MAC_ADDRESS_PATTERN
from clusterutils.remote_script_exec import (
    SftpHandler,
    SshHandler,
    execute_local_script_on_remote,
)
from clusterutils.resources import (
    Resource,
    ResourceKeyValidator,
    Resources,
)
from credentials import Secret
from ._maas_server import MaasServer

# TODO: Add more checks and validation

_CURRENT_PATH = os.path.split(__file__)[0]
_SCRIPTS_FOLDER_NAME = '_scripts'

_INTEL_AMT_POWER_TYPE = 'AMT'
_IPMI_POWER_TYPE = 'IPMI'
_POWER_TYPES = [
    _INTEL_AMT_POWER_TYPE,
    _IPMI_POWER_TYPE,
]

_POWER_TYPE_MAPPING = {
    _INTEL_AMT_POWER_TYPE: 'amt',
    _IPMI_POWER_TYPE: 'ipmi',
}

_INTEL_AMT_ON_STATUS = 'S0'
_INTEL_AMT_OFF_STATUS = 'S5'

_IPMI_OFF_STATUS = 'off'
_IPMI_ON_STATUS = 'on'


class Machines(Resources):

    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='values()',
            attributeNames=('hostname', ),
        ),
        ResourceKeyValidator(
            resourceIterableTree='values()',
            attributeNames=('powerAddress', ),
        ),
        ResourceKeyValidator(
            resourceIterableTree='values()',
            attributeNames=('powerMacAddress', ),
        ),
    ]

    KEY_NAME = ('hostname', )

    def __init__(self, maasServer: MaasServer):
        super().__init__(parentResource=None)
        self.maasServer = maasServer

    def from_config(
            self,
            machinesConfig: dict,
    ) -> None:
        for machineConfig in machinesConfig['machines']:
            machine = Machine(
                machines=self,
            )
            machine.from_config(machineConfig)
            self[machine.hostname] = machine

    def __str__(self):
        return f'machines'


class Machine(Resource):

    def __init__(self, machines: Machines):
        super().__init__(
            parentResource=None,
            resources=machines,
        )
        self.hostname: str = None
        self.powerType: str = None
        self._powerAddress: IPv4Address = None
        self.powerMacAddress: str = None
        self.bmcCredentials: Secret = None
        self.blockDevices = BlockDevices(machine=self)

    @property
    def powerAddress(self) -> str:
        return self._powerAddress.compressed

    def from_config(
            self,
            machineConfig: dict,
    ) -> None:

        self.hostname = machineConfig['hostname']

        if machineConfig['power_type'] not in _POWER_TYPES:
            raise ValueError()
        self.powerType = machineConfig['power_type']

        self._powerAddress = IPv4Address(machineConfig['power_address'])

        if re.search(MAC_ADDRESS_PATTERN, machineConfig['power_mac_address']) is None:
            raise ValueError()
        self.powerMacAddress = machineConfig['power_mac_address']

        self.bmcCredentials = Secret(
            serviceName=machineConfig['bmc_credentials']['service_name'],
            userName=machineConfig['bmc_credentials']['user_name']
        )

        for blockDeviceFromConfig in machineConfig['block_devices']:
            blockDevice = BlockDevice(
                machine=self,
                blockDevices=self.blockDevices,
            )
            blockDevice.from_config(blockDeviceFromConfig)
            self.blockDevices[blockDevice.name] = blockDevice

        if self.blockDevices.numberOfBootableDisks != 1:
            raise ValueError(f'One boot disk is required for {self}.')

    async def deploy(self) -> None:
        actualMachine = await self.actualMachine
        await actualMachine.deploy()

    async def release(self) -> None:
        actualMachine = await self.actualMachine
        await actualMachine.release()

    async def configure(self) -> None:

        actualMachine = await self.actualMachine

        if actualMachine.hostname != self.hostname:
            print(f'Setting hostname of {self}.')
            actualMachine.hostname = self.hostname

        await actualMachine.save()

        await self._configure_storage()

    async def _configure_storage(self) -> None:

        await self._check_for_unexpected_block_devices()

        await self._check_for_missing_block_devices()

        # We must delete existing partitions before
        # creating new ones because there could be
        # mount point clash for two or more partitions
        # in different block devices.
        for blockDevice in self.blockDevices.values():
            await blockDevice.delete_existing_partitions()

        for blockDevice in self.blockDevices.values():
            await blockDevice.configure()

    async def _check_for_unexpected_block_devices(self) -> None:
        actualMachine = await self.actualMachine
        actualBlockDevices = actualMachine.block_devices
        for actualBlockDevice in actualBlockDevices:
            if actualBlockDevice.name not in self._blockDeviceNames:
                raise ValueError()

    @property
    def _blockDeviceNames(self) -> List[str]:
        return list(map(
            operator.attrgetter('name'),
            self.blockDevices.values()
        ))

    async def _check_for_missing_block_devices(self) -> None:
        for blockDevice in self.blockDevices.values():
            if not await blockDevice.exists:
                raise ValueError()

    @async_retry(exceptions=[MaasCallError])
    async def commission(self) -> None:
        actualMachine = await self.actualMachine
        await actualMachine.commission(
            testing_scripts=[
                'smartctl-short',
                'internet-connectivity',
            ]
        )

    async def bmc_power_on(self) -> None:
        if self.powerType == _INTEL_AMT_POWER_TYPE:
            await self._intel_amt_bmc_power_on()
        elif self.powerType == _IPMI_POWER_TYPE:
            await self._ipmi_bmc_power_on()
        else:
            raise ValueError(self.powerType)

    async def _intel_amt_bmc_power_on(self) -> None:
        await self._intel_amt_wake_up()
        SCRIPT_FILE_NAME = 'intel-amt-power-on.sh'
        output, errorOutput = await self._execute_intel_amt_script(SCRIPT_FILE_NAME)

    async def _ipmi_bmc_power_on(self) -> None:
        SCRIPT_FILE_NAME = 'ipmi-power-on.sh'
        output, errorOutput = await self._execute_ipmi_script(SCRIPT_FILE_NAME)

    async def bmc_power_off(self) -> None:
        if self.powerType == _INTEL_AMT_POWER_TYPE:
            await self._intel_amt_bmc_power_off()
        elif self.powerType == _IPMI_POWER_TYPE:
            await self._ipmi_bmc_power_off()
        else:
            raise ValueError(self.powerType)

    async def _intel_amt_bmc_power_off(self) -> None:
        await self._intel_amt_wake_up()
        SCRIPT_FILE_NAME = 'intel-amt-power-off.sh'
        output, errorOutput = await self._execute_intel_amt_script(SCRIPT_FILE_NAME)

    async def _ipmi_bmc_power_off(self) -> None:
        SCRIPT_FILE_NAME = 'ipmi-power-off.sh'
        output, errorOutput = await self._execute_ipmi_script(SCRIPT_FILE_NAME)

    async def bmc_power_reset(self) -> None:
        if self.powerType == _INTEL_AMT_POWER_TYPE:
            await self._intel_amt_bmc_reset()
        elif self.powerType == _IPMI_POWER_TYPE:
            await self._ipmi_bmc_power_reset()
        else:
            raise ValueError(self.powerType)

    async def _intel_amt_bmc_reset(self) -> None:
        await self._intel_amt_wake_up()
        SCRIPT_FILE_NAME = 'intel-amt-reset.sh'
        output, errorOutput = await self._execute_intel_amt_script(SCRIPT_FILE_NAME)

    async def _ipmi_bmc_power_reset(self) -> None:
        SCRIPT_FILE_NAME = 'ipmi-reset.sh'
        output, errorOutput = await self._execute_ipmi_script(SCRIPT_FILE_NAME)

    @property
    async def bmcOn(self) -> bool:
        if self.powerType == _INTEL_AMT_POWER_TYPE:
            return await self.intelAmtBmcStatus == _INTEL_AMT_ON_STATUS
        elif self.powerType == _IPMI_POWER_TYPE:
            return await self.ipmiBmcStatus == _IPMI_ON_STATUS
        else:
            raise ValueError(self.powerType)

    @property
    async def bmcOff(self) -> bool:
        if self.powerType == _INTEL_AMT_POWER_TYPE:
            return await self.intelAmtBmcStatus == _INTEL_AMT_OFF_STATUS
        elif self.powerType == _IPMI_POWER_TYPE:
            return await self.ipmiBmcStatus == _IPMI_OFF_STATUS
        else:
            raise ValueError(self.powerType)

    @property
    async def intelAmtBmcStatus(self) -> str:
        await self._intel_amt_wake_up()
        SCRIPT_FILE_NAME = 'intel-amt-info.sh'
        output, errorOutput = await self._execute_intel_amt_script(SCRIPT_FILE_NAME)
        PATTERN = re.compile(
            r'''
            ^Powerstate:[\ ]+(?P<status>S[0-9]).*$
            ''', re.VERBOSE
        )
        for line in output.splitlines():
            if 'Powerstate' not in line:
                continue
            match = re.search(PATTERN, line)
            if match is not None:
                return match.group('status')
        raise RuntimeError()

    @property
    async def ipmiBmcStatus(self) -> str:
        SCRIPT_FILE_NAME = 'ipmi-status.sh'
        output, errorOutput = await self._execute_ipmi_script(SCRIPT_FILE_NAME)
        PATTERN = re.compile(
            r'''
            ^
            # We allow out of range IPv4 addresses
            # (e.g. 999.999.999.999) but it's ok
            # since we know ipmipower will give a
            # valid IPv4.
            (?:[0-9]{1,3}\.){3}
            (?:[0-9]{1,3})
            (?::[\ ]*)
            (?P<status>(?:on)|(?:off))
            $
            ''', re.VERBOSE
        )
        for line in output.splitlines():
            match = re.search(PATTERN, line)
            if match is not None:
                return match.group('status')
        raise RuntimeError()

    @async_retry(exceptions=[RuntimeError])
    async def _intel_amt_wake_up(self) -> None:
        """
        Sometimes, the Intel AMT BMC seems to be sleeping.
        This method query twice the BMC in order to wake it up.
        """
        SCRIPT_FILE_NAME = 'intel-amt-wake-up.sh'
        output, errorOutput = await self._execute_intel_amt_script(SCRIPT_FILE_NAME)

    async def _execute_ipmi_script(self, scriptFileName) -> Tuple[str, str]:
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            scriptFileName,
        )
        environment = {
            'POWER_ADDRESS': self.powerAddress,
            'POWER_USER_NAME': self.bmcCredentials.userName,
            'POWER_PASSWORD': self.bmcCredentials.value,
        }
        assert len(self.maasServer.rackControllers) == 1, 'Not implemented'
        rackController = next(self.maasServer.rackControllers.values())
        output, errorOutput = await rackController.virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{scriptFileName}',
            environment=environment,
        )
        return output, errorOutput

    @async_retry(exceptions=[RuntimeError])
    async def _execute_intel_amt_script(self, scriptFileName) -> Tuple[str, str]:
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            scriptFileName,
        )
        environment = {
            'POWER_ADDRESS': self.powerAddress,
            'AMT_PASSWORD': self.bmcCredentials.value,
        }
        assert len(self.maasServer.rackControllers) == 1, 'Not implemented'
        rackController = next(self.maasServer.rackControllers.values())
        output, errorOutput = await rackController.virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{scriptFileName}',
            environment=environment,
        )
        return output, errorOutput

    @property
    async def exists(self) -> bool:
        try:
            _ = await self.actualMachine
        except _ActualMachineNotFoundError:
            return False
        else:
            return True

    @property
    async def new(self) -> bool:
        return await self.machineStatus == NodeStatus.NEW

    @property
    async def ready(self) -> bool:
        return await self.machineStatus == NodeStatus.READY

    @property
    async def deploying(self) -> bool:
        return await self.machineStatus == NodeStatus.DEPLOYING

    @property
    async def deployed(self) -> bool:
        return await self.machineStatus == NodeStatus.DEPLOYED

    @property
    async def failedDeployment(self) -> bool:
        return await self.machineStatus == NodeStatus.FAILED_DEPLOYMENT

    @property
    async def releasing(self) -> bool:
        return await self.machineStatus == NodeStatus.RELEASING

    @property
    async def failedReleasing(self) -> bool:
        return await self.machineStatus == NodeStatus.FAILED_RELEASING

    @property
    async def failedCommissioning(self) -> bool:
        return await self.machineStatus == NodeStatus.FAILED_COMMISSIONING

    @property
    async def commissioning(self) -> bool:
        return await self.machineStatus == NodeStatus.COMMISSIONING

    @property
    async def testing(self) -> bool:
        return await self.machineStatus == NodeStatus.TESTING

    @property
    async def machineStatus(self) -> str:
        actualMachine = await self.actualMachine
        return actualMachine.status

    async def configure_bmc(self) -> None:

        if self.powerType == _INTEL_AMT_POWER_TYPE:
            powerParameters = {
                'power_address': self.powerAddress,
                'power_pass': self.bmcCredentials.value,
            }
        elif self.powerType == _IPMI_POWER_TYPE:
            powerParameters = {
                'power_address': self.powerAddress,
                'power_user': self.bmcCredentials.userName,
                'power_pass': self.bmcCredentials.value,
                'mac_address': self.powerMacAddress,
            }
        else:
            raise NotImplementedError(self.powerType)

        actualMachine = await self.actualMachine
        await actualMachine.set_power(
            power_type=_POWER_TYPE_MAPPING[self.powerType],
            power_parameters=powerParameters
        )

    async def set_hostname(self) -> None:
        actualMachine = await self.actualMachine
        actualMachine.hostname = self.hostname
        await actualMachine.save()

    async def execute_local_script_on_remote(
            self,
            localFilePath: str,
            remoteFilePath: str,
            environment: dict = None,
            environmentQuoteTypes: dict = None,
    ) -> Tuple[str, str]:

        sshHandler = SshHandler(
            username='ubuntu',
            ipAddress=await self.ipAddress,
        )

        sftpHandler = SftpHandler(sshHandler)

        output, errorOutput = await execute_local_script_on_remote(
            localFilePath,
            remoteFilePath,
            sshHandler,
            sftpHandler,
            environment,
            environmentQuoteTypes,
        )

        return output, errorOutput

    @property
    async def ipAddress(self) -> str:
        if not await self.bmcOn:
            raise RuntimeError()
        if not await self.deployed:
            raise RuntimeError()
        actualMachine = await self.actualMachine
        return actualMachine.ip_addresses[0]

    @property
    async def actualMachine(self) -> ActualMachine:
        client = await self.maasServer.client
        for actualMachine in await client.machines.list():
            for actualInterface in actualMachine.interfaces:
                if actualInterface.mac_address.upper() == self.powerMacAddress.upper():
                    return actualMachine
        raise _ActualMachineNotFoundError()

    @property
    def maasServer(self) -> MaasServer:
        assert isinstance(self.resources.maasServer, MaasServer)
        return self.resources.maasServer

    def __str__(self):
        return f'{self.hostname}'


class BlockDevices(Resources):

    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='values()',
            attributeNames=('name', ),
        ),
    ]

    KEY_NAME = ('name', )

    def __init__(self, machine: Machine):
        super().__init__(parentResource=machine)

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        if self.numberOfBootableDisks > 1:
            raise ValueError(f'Cannot have more than '
                             f'one boot disk in {self}')

    @property
    def numberOfBootableDisks(self) -> int:
        return len([
            blockDevice
            for blockDevice in self.values()
            if blockDevice.bootDisk
        ])

    @property
    def machine(self) -> Machine:
        assert isinstance(self.parentResource, Machine)
        return self.parentResource

    def __str__(self):
        return f'block devices of {self.machine}'


class BlockDevice(Resource):

    def __init__(self, machine: Machine, blockDevices: BlockDevices):
        super().__init__(
            parentResource=machine,
            resources=blockDevices,
        )
        self.name: str = None
        self.bootDisk: bool = None
        self.partitions = Partitions(blockDevice=self)

    def from_config(self, blockDeviceFromConfig: dict) -> None:

        self.name = blockDeviceFromConfig['name']

        assert isinstance(blockDeviceFromConfig['boot_disk'], bool)
        self.bootDisk = blockDeviceFromConfig['boot_disk']

        for partitionFromConfig in blockDeviceFromConfig['partitions']:
            partition = Partition(
                blockDevice=self,
                partitions=self.partitions,
            )
            partition.from_config(partitionFromConfig)
            self.partitions[partition.mountPoint] = partition

        if self.bootDisk and self.partitions.numberOfBootablePartitions != 1:
            raise ValueError(f'One boot partition is required for {self}.')

    async def configure(self) -> None:

        if self.bootDisk:
            print(f'Setting {self} as boot disk.')
            actualBlockDevice = await self.actualBlockDevice
            await actualBlockDevice.set_as_boot_disk()

        for partition in self.partitions.values():
            assert not await partition.exists
            print(f'Creating {partition}.')
            await partition.create()
            assert await partition.exists

    async def delete_existing_partitions(self) -> None:
        actualBlockDevice = await self.actualBlockDevice
        actualPartitions = actualBlockDevice.partitions
        for actualPartition in actualPartitions:
            print(f'Deleting '
                  f'{actualPartition.filesystem.mount_point} '
                  f'from {self}.')
            await actualPartition.delete()

    @property
    async def actualBlockDeviceSize(self) -> int:
        actualBlockDevice = await self.actualBlockDevice
        return actualBlockDevice.size

    @property
    async def exists(self) -> bool:
        try:
            _ = await self.actualBlockDevice
        except _ActualBlockDeviceNotFoundError:
            return False
        else:
            return True

    @property
    async def actualBlockDevice(self) -> ActualBlockDevice:
        actualMachine = await self.machine.actualMachine
        actualBlockDevices = actualMachine.block_devices
        for actualBlockDevice in actualBlockDevices:
            if actualBlockDevice.name == self.name:
                return actualBlockDevice
        raise _ActualBlockDeviceNotFoundError()

    @property
    def partitionNames(self) -> List[str]:
        return [
            partition.name
            for partition in self.partitions.values()
        ]

    @property
    def machine(self) -> Machine:
        assert isinstance(self.parentResource, Machine)
        return self.parentResource

    @property
    def maasServer(self) -> MaasServer:
        assert isinstance(self.machine.maasServer, MaasServer)
        return self.machine.maasServer

    def __str__(self):
        return f'block device {self.name} of {self.machine}'


class Partitions(Resources):

    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='values()',
            attributeNames=('mountPoint', ),
        ),
    ]

    KEY_NAME = ('mountPoint', )

    def __init__(self, blockDevice: BlockDevice):
        super().__init__(parentResource=blockDevice)

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        if self.numberOfBootablePartitions > 1:
            raise ValueError(f'Cannot have more than one '
                             f'bootable partition in {self}.')

    @property
    def numberOfBootablePartitions(self) -> int:
        return len([
            partition
            for partition in self.values()
            if partition.bootable
        ])

    @property
    def blockDevice(self) -> BlockDevice:
        assert isinstance(self.parentResource, BlockDevice)
        return self.parentResource

    def __str__(self):
        return f'partitions of {self.blockDevice}'


class Partition(Resource):

    def __init__(self, blockDevice: BlockDevices, partitions: Partitions):
        super().__init__(
            parentResource=blockDevice,
            resources=partitions,
        )
        self.mountPoint: str = None
        self.fileSystemType: str = None
        self._size: str = None
        self.bootable: bool = None

    def from_config(self, partitionFromConfig: dict) -> None:

        # TODO: Check if a valid path
        self.mountPoint = partitionFromConfig['mount_point']

        # TODO: Check valid values
        self.fileSystemType = partitionFromConfig['fstype']

        # TODO: Use StorageSize
        self._size = partitionFromConfig['size']

        assert isinstance(partitionFromConfig['bootable'], bool)
        self.bootable = partitionFromConfig['bootable']

        if self.bootable and self.mountPoint != '/':
            raise ValueError()
        if not self.bootable and self.mountPoint == '/':
            raise ValueError()

    @property
    async def size(self) -> int:
        try:
            return int(self._size)
        except ValueError:
            size = await self.blockDevice.actualBlockDeviceSize
            for partition in self.partitions.values():
                try:
                    size -= int(partition._size)
                except ValueError:
                    pass
            # We multiply by 0.999 to workaround
            # what seems to be a rounding error.
            # If we don't do that, MAAS complains
            # that the requested partition's size
            # is greater than the available space
            # on the block device.
            size = int(size * 0.999)
            assert size > 0
            return size

    @property
    async def exists(self) -> bool:
        try:
            _ = await self.actualPartition
        except _ActualPartitionNotFoundError:
            return False
        else:
            return True

    async def create(self) -> None:
        actualBlockDevice = await self.blockDevice.actualBlockDevice
        size = await self.size
        actualPartition = await actualBlockDevice.partitions.create(
            size,
        )
        await actualPartition.format(fstype=self.fileSystemType)
        await actualPartition.mount(mount_point=self.mountPoint)

    @property
    async def actualPartition(self) -> ActualPartition:
        actualBlockDevice = await self.blockDevice.actualBlockDevice
        actualPartitions = actualBlockDevice.partitions
        for actualPartition in actualPartitions:
            if actualPartition.filesystem.mount_point == self.mountPoint:
                return actualPartition
        raise _ActualPartitionNotFoundError()

    @property
    def blockDevice(self) -> BlockDevice:
        assert isinstance(self.parentResource, BlockDevice)
        return self.parentResource

    @property
    def partitions(self) -> Partitions:
        assert isinstance(self.resources, Partitions)
        return self.resources

    @property
    def maasServer(self) -> MaasServer:
        assert isinstance(self.blockDevice.maasServer, MaasServer)
        return self.blockDevice.maasServer

    def __str__(self):
        return f'partition {self.mountPoint} of {self.blockDevice}'


class _ActualMachineNotFoundError(Exception):
    pass


class _ActualBlockDeviceNotFoundError(Exception):
    pass


class _ActualPartitionNotFoundError(Exception):
    pass
