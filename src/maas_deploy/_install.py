from __future__ import annotations

import asyncio
import os
from typing import List

from ruamel.yaml import YAML

from clusterutils import YamlSchemaValidator
from proxmox import (
    Hypervisors,
    VirtualMachine,
    load_virtual_machines_from_config,
)
from ._installers import (
    MaasDatabaseInstallers,
    MaasRackControllerInstaller,
    MaasRackControllerInstallers,
    MaasRegionControllerInstallers,
    MaasRegionDatabaseInstaller,
    MaasRegionRackDatabaseInstaller,
)

_CURRENT_PATH = os.path.split(__file__)[0]
_SCHEMA_FOLDER_NAME = '_schemas'


def install_maas_controllers(
        virtualMachinesFilePaths: List[str],
        maasControllersFilePaths: List[str],
        forceReinstall: bool,
) -> None:

    hypervisors = Hypervisors()
    load_virtual_machines_from_config(
        virtualMachinesFilePaths,
        hypervisors,
    )

    maasRegionControllerInstallers = MaasRegionControllerInstallers(hypervisors)
    maasRackControllerInstallers = MaasRackControllerInstallers(hypervisors)
    maasDatabaseInstallers = MaasDatabaseInstallers(hypervisors)
    _load_maas_controllers_from_config(
        maasControllersFilePaths,
        maasRegionControllerInstallers,
        maasRackControllerInstallers,
        maasDatabaseInstallers,
    )

    maasInstallationType = _MaasInstallationTopology(
        maasRegionControllerInstallers,
        maasRackControllerInstallers,
        maasDatabaseInstallers,
    )

    # Type 1
    if maasInstallationType.allOnSameMachine \
            and not maasInstallationType.highlyAvailableDatabases \
            and not maasInstallationType.highlyAvailableRegionControllers \
            and not maasInstallationType.highlyAvailableRackControllers:
        maasRegionRackDatabaseInstaller = (
                maasRegionControllerInstallers.maasRegionControllerInstaller +
                maasDatabaseInstallers.maasDatabaseInstaller +
                maasRackControllerInstallers.maasRackControllerInstaller
        )
        _install_region_database_rack(
            maasRegionRackDatabaseInstaller,
            forceReinstall,
        )
        _configure_region_database_rack()
        print(f'Restarting {maasRegionRackDatabaseInstaller.virtualMachine}.')
        maasRegionRackDatabaseInstaller.virtualMachine.restart()

    # Type 2
    elif maasInstallationType.colocatedRegionAndDatabase \
            and not maasInstallationType.colocatedRegionAndRack \
            and not maasInstallationType.highlyAvailableDatabases \
            and not maasInstallationType.highlyAvailableRegionControllers \
            and not maasInstallationType.highlyAvailableRackControllers:
        maasRegionDatabaseInstaller = (
                maasRegionControllerInstallers.maasRegionControllerInstaller +
                maasDatabaseInstallers.maasDatabaseInstaller
        )
        maasRackControllerInstaller = (
            maasRackControllerInstallers.maasRackControllerInstaller
        )
        asyncio.run(_gather_type_2_installation(
            maasRegionDatabaseInstaller,
            maasRackControllerInstaller,
            forceReinstall,
        ))
        _configure_region_database(
            maasRegionDatabaseInstaller,
        )
        _configure_rack(
            maasRegionDatabaseInstaller,
            maasRackControllerInstaller,
        )
        asyncio.run(_restart_virtual_machines([
            maasRackControllerInstaller.virtualMachine,
            maasRegionDatabaseInstaller.virtualMachine,
        ]))

    else:
        raise NotImplementedError(
            f'Specified MAAS topology '
            f'is not implemented or invalid.'
        )


def _load_maas_controllers_from_config(
        maasControllersFilePaths: List[str],
        maasRegionControllerInstallers: MaasRegionControllerInstallers,
        maasRackControllerInstallers: MaasRackControllerInstallers,
        maasDatabaseInstallers: MaasDatabaseInstallers,
) -> None:

    for maasControllersFilePath in maasControllersFilePaths:
        _validate_maas_controllers_schema(maasControllersFilePath)

    for maasControllersFilePath in maasControllersFilePaths:
        yaml = YAML(typ='safe')
        with open(maasControllersFilePath, 'rt') as file:
            maasControllersFromConfig = yaml.load(file)
            maasRegionControllerInstallers.from_config(
                maasControllersFromConfig['region_controllers'],
                maasControllersFromConfig['maas'],
            )
            maasRackControllerInstallers.from_config(
                maasControllersFromConfig['rack_controllers'],
            )
            maasDatabaseInstallers.from_config(
                maasControllersFromConfig['databases'],
            )

    _validate_at_least_one_virtual_machine_per_service(
        maasRegionControllerInstallers,
        maasRackControllerInstallers,
        maasDatabaseInstallers,
    )


def _validate_maas_controllers_schema(maasControllersFilePath: str) -> None:
    yamlSchemaValidator = YamlSchemaValidator(
        source_file=maasControllersFilePath,
        schema_files=[os.path.join(
            _CURRENT_PATH, _SCHEMA_FOLDER_NAME, 'maas_controllers_schema.yaml')]
    )
    yamlSchemaValidator.validate(raise_exception=True)


def _validate_at_least_one_virtual_machine_per_service(
        maasRegionControllerInstallers: MaasRegionControllerInstallers,
        maasRackControllerInstallers: MaasRackControllerInstallers,
        maasDatabaseInstallers: MaasDatabaseInstallers,
) -> None:
    SERVICE_NAME_2_INSTALLERS = {
        'region controllers': maasRegionControllerInstallers,
        'rack controllers': maasRackControllerInstallers,
        'databases': maasDatabaseInstallers,
    }
    for serviceName, installers in SERVICE_NAME_2_INSTALLERS.items():
        if len(installers) == 0:
            raise ValueError(
                f'At least one virtual machine '
                f'is required for {serviceName}.'
            )


class _MaasInstallationTopology:

    def __init__(
            self,
            maasRegionControllerInstallers: MaasRegionControllerInstallers,
            maasRackControllerInstallers: MaasRackControllerInstallers,
            maasDatabaseInstallers: MaasDatabaseInstallers,
    ):
        self._maasRegionControllerInstallers = maasRegionControllerInstallers
        self._maasRackControllerInstallers = maasRackControllerInstallers
        self._maasDatabaseInstallers = maasDatabaseInstallers

    @property
    def allOnSameMachine(self) -> bool:
        return self.colocatedRegionAndDatabase \
               and self.colocatedRegionAndRack

    @property
    def colocatedRegionAndDatabase(self) -> bool:
        regionAndDatabaseVirtualMachineNames = set(
            self._maasRegionControllerInstallers.virtualMachineNames
        )
        regionAndDatabaseVirtualMachineNames.update(set(
            self._maasDatabaseInstallers.virtualMachineNames
        ))
        return len(regionAndDatabaseVirtualMachineNames) == 1

    @property
    def colocatedRegionAndRack(self) -> bool:
        regionAndRackVirtualMachineNames = set(
            self._maasRegionControllerInstallers.virtualMachineNames
        )
        regionAndRackVirtualMachineNames.update(set(
            self._maasRackControllerInstallers.virtualMachineNames
        ))
        return len(regionAndRackVirtualMachineNames) == 1

    @property
    def highlyAvailableDatabases(self) -> bool:
        return len(self._maasDatabaseInstallers) > 1

    @property
    def highlyAvailableRegionControllers(self) -> bool:
        return len(self._maasRegionControllerInstallers) > 1

    @property
    def highlyAvailableRackControllers(self) -> bool:
        return len(self._maasRackControllerInstallers) > 1


# ------------------------------------------------


async def _gather_type_2_installation(
        maasRegionDatabaseInstaller: MaasRegionDatabaseInstaller,
        maasRackControllerInstaller: MaasRackControllerInstaller,
        forceReinstall: bool,
) -> None:
    await asyncio.gather(
        _install_region_database(
            maasRegionDatabaseInstaller,
            forceReinstall,
        ),
        _install_rack(
            maasRackControllerInstaller,
            forceReinstall,
        ),
    )


async def _install_region_database(
        maasRegionDatabaseInstaller: MaasRegionDatabaseInstaller,
        forceReinstall: bool,
) -> None:
    await _install_non_available_installer(
        maasRegionDatabaseInstaller,
        forceReinstall,
    )


async def _install_rack(
        maasRackControllerInstaller: MaasRackControllerInstaller,
        forceReinstall: bool,
) -> None:
    await _install_non_available_installer(
        maasRackControllerInstaller,
        forceReinstall,
    )


def _install_region_database_rack(
        maasRegionRackDatabaseInstaller: MaasRegionRackDatabaseInstaller,
        forceReinstall: bool,
) -> None:
    _install_non_available_installer(
        maasRegionRackDatabaseInstaller,
        forceReinstall,
    )


async def _install_non_available_installer(
        serviceInstaller,
        forceReinstall: bool,
) -> None:

    if serviceInstaller.virtualMachine.off:
        print(f'Starting {serviceInstaller.virtualMachine}.')
        await serviceInstaller.virtualMachine.start_async()

    # It is slow to query those properties
    # so we query them once.
    installed = await serviceInstaller.installed
    isLatestStableRelease = await serviceInstaller.isLatestStableRelease if installed else False

    if not installed:
        print(f'Installing {serviceInstaller} for the first time.')
        await serviceInstaller.install()

    elif installed \
            and not isLatestStableRelease:
        await serviceInstaller.uninstall()
        await serviceInstaller.install()

    elif installed \
            and isLatestStableRelease \
            and forceReinstall:
        await serviceInstaller.uninstall()
        await serviceInstaller.install()

    elif installed \
            and isLatestStableRelease \
            and not forceReinstall:
        print(f'Latest version of {serviceInstaller} already installed.')

    else:
        raise NotImplementedError()


# ------------------------------------------------


def _configure_region_database(
        maasRegionDatabaseInstaller: MaasRegionDatabaseInstaller,
) -> None:

    # TODO
    # if maasRegionDatabaseInstaller.configured:
    #     return

    print(f'Configuring {maasRegionDatabaseInstaller}.')

    maasRegionDatabaseInstaller.configure()

    print(f'Done configuring {maasRegionDatabaseInstaller}.')


def _configure_rack(
        maasRegionDatabaseInstaller: MaasRegionDatabaseInstaller,
        maasRackControllerInstaller: MaasRackControllerInstaller,
) -> None:

    # TODO
    # if maasRackControllerInstaller.configured:
    #     return

    print(f'Configuring {maasRackControllerInstaller}.')

    maasRackControllerInstaller.configure(
        maasRegionDatabaseInstaller
    )

    print(f'Done configuring {maasRackControllerInstaller}.')


def _configure_region_database_rack() -> None:
    raise NotImplementedError()


# ------------------------------------------------


async def _restart_virtual_machines(
        virtualMachines: List[VirtualMachine]
) -> None:
    await asyncio.gather(*list(map(
        _restart_virtual_machine,
        virtualMachines,
    )))


async def _restart_virtual_machine(
        virtualMachine: VirtualMachine,
) -> None:
    print(f'Restarting {virtualMachine}.')
    await virtualMachine.restart_async()
    print(f'Done restarting {virtualMachine}.')
