import itertools
import os
import re
from typing import (
    Sequence,
    Union,
)

from clusterutils import (
    EMAIL_PATTERN,
    OPEN_SSH_PUBLIC_KEY_PATTERN,
    async_retry,
)
from proxmox import VirtualMachine

_CURRENT_PATH = os.path.split(__file__)[0]
_SCRIPTS_FOLDER_NAME = '_scripts'


class PackageInfo:

    def __init__(
            self,
            virtualMachine: VirtualMachine,
            packageName: str,
            packageRepository: str = None,
    ):
        self.virtualMachine = virtualMachine
        self.packageName = packageName
        self.packageRepository = packageRepository

    @property
    async def installed(self) -> bool:
        print(f'Looking if {self.packageName} on repository '
              f'{self.packageRepository} is installed '
              f'on {self.virtualMachine}')
        return await self.installedVersion is not None

    @property
    async def installedVersion(self) -> Union[str, None]:
        print(f'Looking for installed version '
              f'of {self.packageName} on repository '
              f'{self.packageRepository} '
              f'on {self.virtualMachine}')
        PATTERN = re.compile(r'^[ ]*?Installed: (?P<installedVersion>.*?)$')
        aptCachePolicy = await self._aptCachePolicy
        for line in aptCachePolicy.splitlines():
            match = re.search(PATTERN, line)
            if match is not None:
                installedVersion = match.group('installedVersion')
                if installedVersion == '(none)':
                    return None
                else:
                    return installedVersion
        raise RuntimeError()

    @property
    async def candidateVersion(self) -> str:
        print(f'Looking for candidate version '
              f'of {self.packageName} on repository '
              f'{self.packageRepository} '
              f'on {self.virtualMachine}')
        PATTERN = re.compile(r'^[ ]*?Candidate: (?P<candidateVersion>.*?)$')
        aptCachePolicy = await self._aptCachePolicy
        for line in aptCachePolicy.splitlines():
            match = re.search(PATTERN, line)
            if match is not None:
                candidateVersion = match.group('candidateVersion')
                return candidateVersion
        raise RuntimeError()

    @property
    @async_retry(exceptions=[RuntimeError])
    async def _aptCachePolicy(self) -> str:
        SCRIPT_FILE_NAME = 'get-apt-cache-policy.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            SCRIPT_FILE_NAME,
        )
        environment = {
            'ROOT_PASSWORD': self.virtualMachine.secret.value,
            'PACKAGE_REPOSITORY': self.packageRepository,
            'PACKAGE_NAME': self.packageName,
        }
        output, errorOutput = await self.virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{SCRIPT_FILE_NAME}',
            environment=environment,
        )
        return output

    @property
    async def successfulInstallation(self) -> bool:
        print(f'Looking if {self.packageName} on repository '
              f'{self.packageRepository} was successfully installed '
              f'on {self.virtualMachine}')
        # https://linuxprograms.wordpress.com/2010/05/11/status-dpkg-list/
        packageInstallationStatus = await self._packageInstallationStatus
        assert packageInstallationStatus in self._possiblePackageStatuses, \
            packageInstallationStatus
        return packageInstallationStatus == 'ii'

    @property
    def _possiblePackageStatuses(self):
        firstCharacters = [
            'u',
            'i',
            'r',
            'p',
            'h',
        ]
        secondCharacters = [
            'n',
            'i',
            'c',
            'u',
            'f',
            'h',
            'W',
            't',
        ]
        thirdCharacters = [
            'R',
            '',
        ]
        possiblePackageStatuses = map(
            ''.join,
            itertools.product(firstCharacters, secondCharacters, thirdCharacters, repeat=1)
        )
        possiblePackageStatuses = list(possiblePackageStatuses)
        return possiblePackageStatuses

    @property
    @async_retry(exceptions=[RuntimeError])
    async def _packageInstallationStatus(self) -> str:
        SCRIPT_FILE_NAME = 'get-package-installation-status.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            SCRIPT_FILE_NAME,
        )
        environment = {
            'FORMAT': '${db:Status-Abbrev}',
            'PACKAGE_NAME': self.packageName,
        }
        output, errorOutput = await self.virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{SCRIPT_FILE_NAME}',
            environment=environment,
            environmentQuoteTypes={'FORMAT': 'single'}
        )
        output = output.strip().strip('\n').strip()
        return output


def validate_email(email) -> None:
    if re.search(EMAIL_PATTERN, email) is None:
        raise ValueError(f'{email} is not a valid e-mail address.')


def validate_public_ssh_keys(publicSshKeys: Sequence[str]) -> None:
    assert all(isinstance(publicSshKey, str) for publicSshKey in publicSshKeys)
    if not all(re.search(OPEN_SSH_PUBLIC_KEY_PATTERN, value) is not None
               for value in publicSshKeys):
        raise ValueError()


def get_interface_for_api_server(virtualMachineFromConfig: dict) -> dict:
    for interfaceFromConfig in virtualMachineFromConfig['interfaces']:
        assert isinstance(interfaceFromConfig['used_for_api_server'], bool)
        if interfaceFromConfig['used_for_api_server']:
            return interfaceFromConfig
    raise RuntimeError()


def ensure_only_one_interface_for_api_server(virtualMachineFromConfig: dict) -> None:
    n = 0
    for interfaceFromConfig in virtualMachineFromConfig['interfaces']:
        assert isinstance(interfaceFromConfig['used_for_api_server'], bool)
        if interfaceFromConfig['used_for_api_server']:
            n += 1
    if n != 1:
        raise ValueError()
