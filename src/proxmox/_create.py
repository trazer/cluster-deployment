import asyncio
import functools
import os
from typing import List

import paramiko
from ruamel.yaml import YAML

from ._hypervisor import (
    Hypervisor,
    Hypervisors,
)
from ._schema import validate_virtual_machines_schema
from ._virtual_machine import VirtualMachine

_CURRENT_PATH = os.path.split(__file__)[0]
_SCRIPTS_FOLDER_NAME = '_scripts'


def create_virtual_machines(
        virtualMachinesFilePaths: List[str],
        skipExisting: bool,
        stopIfOn: bool,
) -> None:
    """
    Create virtual machines according to the configuration files (YAML format).

    The default behavior is as follows:

       #. If the ``vmid`` does not exist, the virtual machine is created as specified.

       #. If the ``vmid`` exists and :param:`skipExisting` is ``True``, the virtual
          machine is not created.

       #. If the ``vmid`` exists but :param:`skipExisting` is ``False``, the existing
          virtual machine is deleted, then created as specified.

    In any case where the virtual machine exists and it is powered on, it will be
    shutdown before deletion only if the flag :param:`stopIfOn` is ``True``.
    """

    hypervisors = Hypervisors()
    load_virtual_machines_from_config(
        virtualMachinesFilePaths,
        hypervisors,
    )

    asyncio.run(_create_virtual_machines(
        hypervisors,
        skipExisting,
        stopIfOn,
    ))

    assert all(
        virtualMachine.off
        for hypervisor in hypervisors.values()
        for virtualMachine in hypervisor.virtualMachines.values()
    )


async def _create_virtual_machines(
        hypervisors: Hypervisors,
        skipExisting: bool,
        stopIfOn: bool,
) -> None:
    virtualMachineIterator = (
        virtualMachine
        for hypervisor in hypervisors.values()
        for virtualMachine in hypervisor.virtualMachines.values()
    )
    await asyncio.gather(*list(map(
        functools.partial(_create_virtual_machine, skipExisting, stopIfOn),
        virtualMachineIterator
    )))


async def _create_virtual_machine(
        skipExisting: bool,
        stopIfOn: bool,
        virtualMachine: VirtualMachine,
) -> None:

    if not virtualMachine.exists:
        await virtualMachine.create()

    elif virtualMachine.exists \
            and not skipExisting \
            and virtualMachine.off:
        virtualMachine.delete()
        await virtualMachine.create()

    elif virtualMachine.exists \
            and not skipExisting \
            and virtualMachine.on \
            and stopIfOn:
        await virtualMachine.stop()
        virtualMachine.delete()
        await virtualMachine.create()

    elif virtualMachine.exists \
            and not skipExisting \
            and virtualMachine.on \
            and not stopIfOn:
        msg = (
            f'Can\'t delete virtual machine '
            f'{virtualMachine.vmid} ({virtualMachine.name}) '
            f'because it is powered on.'
        )
        raise RuntimeError(msg)

    else:
        raise NotImplementedError(f'Undefined state for {virtualMachine}.')

    # Start the virtual machine for
    # the first time so that the
    # cloud-init drive installs
    # the operating system
    await virtualMachine.start_async(wait=True)

    await _wait_for_operating_system_to_be_ready(virtualMachine)

    if virtualMachine.enableIpForwarding:
        print(f'Enabling ip forwarding on {virtualMachine}.')
        await _enable_ip_forwarding(virtualMachine)

    print(f'Shutting down {virtualMachine}.')
    await virtualMachine.shutdown_async()
    print(f'{virtualMachine} is ready.')


async def _wait_for_operating_system_to_be_ready(virtualMachine: VirtualMachine) -> None:

    while True:
        if not virtualMachine.reachable:
            print(f'{virtualMachine} is not reachable.')
            continue
        if not await _is_operating_system_on_virtual_machine_ready(virtualMachine):
            print(f'Operating system on {virtualMachine} is not ready.')
            continue
        break

    print(f'Performing full upgrade of {virtualMachine}.')
    await virtualMachine.full_upgrade()


# TODO: Put somewhere else...
async def _enable_ip_forwarding(virtualMachine: VirtualMachine) -> None:
    SCRIPT_FILE_NAME = 'enable-ip-forwarding.sh'
    localFilePath = os.path.join(
        _CURRENT_PATH,
        _SCRIPTS_FOLDER_NAME,
        SCRIPT_FILE_NAME,
    )
    environment = {
        'ROOT_PASSWORD': virtualMachine.secret.value,
    }
    output, errorOutput = await virtualMachine.execute_local_script_on_remote_async(
        localFilePath=localFilePath,
        remoteFilePath=f'/tmp/{SCRIPT_FILE_NAME}',
        environment=environment,
    )


def load_virtual_machines_from_config(
        virtualMachinesFilePaths: List[str],
        hypervisors: Hypervisors,
) -> None:

    for virtualMachinesFilePath in virtualMachinesFilePaths:
        validate_virtual_machines_schema(virtualMachinesFilePath)

    for virtualMachinesFilePath in virtualMachinesFilePaths:
        yaml = YAML(typ='safe')
        with open(virtualMachinesFilePath, 'rt') as file:
            for fromConfig in yaml.load(file)['hypervisors']:
                hypervisor = Hypervisor(hypervisors=hypervisors)
                hypervisor.from_config(fromConfig)
                hypervisors[hypervisor.name] = hypervisor


async def _is_operating_system_on_virtual_machine_ready(virtualMachine: VirtualMachine) -> bool:
    command = 'echo "Hello world!"'
    try:
        with virtualMachine.ssh() as ssh:
            stdin, stdout, stderr = ssh.exec_command(command)
            while not stdout.channel.exit_status_ready():
                await asyncio.sleep(0.5)
            if stdout.channel.exit_status != 0:
                raise RuntimeError()
    except paramiko.ssh_exception.NoValidConnectionsError:
        return False
    else:
        return True
