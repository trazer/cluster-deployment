from __future__ import annotations

import asyncio
import contextlib
import shlex
from typing import Generator

import aiohttp
import paramiko
from proxmoxer import (
    ProxmoxAPI,
    ResourceException,
)

from credentials import Secret
from ._base import (
    BaseResource,
    BaseResources,
    UniqueKeyName,
)
from ._utils import is_reachable
from ._virtual_machine import (
    VirtualMachine,
    VirtualMachines,
)

_IMAGE_NAME_2_URL = {
    'bionic-server-cloudimg-amd64.img':
        r'https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img'
}

_IMAGE_NAME_2_HASH_URL = {
    'bionic-server-cloudimg-amd64.img':
        r'https://cloud-images.ubuntu.com/bionic/current/SHA256SUMS'
}


class Hypervisor(BaseResource):

    def __init__(self, hypervisors: Hypervisors):
        super().__init__(resources=hypervisors)
        self.name = None
        self.ipAddress = None
        self.port = None
        self.secret = None
        self.verifySsl = None
        self.virtualMachines = VirtualMachines(parentResource=self)
        self._imagesDownloading = {}

    @property
    def exists(self) -> bool:

        if not self.reachable:
            return False

        # Request the hypervisor's API version details.
        # If the hypervisor does not exist, it will
        # throw an exception.
        try:
            _ = self.proxmoxApi.nodes(self.name).version.get()
        except ResourceException:
            return False

        return True

    @property
    def reachable(self) -> bool:
        return is_reachable(self.ipAddress)

    def from_config(self, fromConfig) -> None:

        self.name = fromConfig['name']
        self.ipAddress = fromConfig['ip_address']
        self.port = fromConfig['port']
        self.secret = Secret(
            fromConfig['credentials']['service_name'],
            fromConfig['credentials']['user_name'],
        )
        self.verifySsl = fromConfig['verify_ssl']

        for fromConfigVirtualMachine in fromConfig['virtual_machines']:
            virtualMachine = VirtualMachine(
                hypervisor=self,
                virtualMachines=self.virtualMachines,
            )
            virtualMachine.from_config(fromConfigVirtualMachine)
            self.virtualMachines[virtualMachine.vmid] = virtualMachine

    # -----------------------------------------------

    async def download_image(self, imageName: str) -> None:

        with self._image_download_notify(imageName), \
                self.ssh() as ssh:
            command = f'wget {shlex.quote(_IMAGE_NAME_2_URL[imageName])}'
            stdin, stdout, stderr = ssh.exec_command(command)
            while not stdout.channel.exit_status_ready():
                await asyncio.sleep(0.5)
            if stdout.channel.exit_status != 0:
                raise RuntimeError(command)

        print(f'Done downloading {imageName}.')

    def image_is_already_downloading(self, imageName) -> bool:
        return imageName in self._imagesDownloading

    @contextlib.contextmanager
    def _image_download_notify(self, imageName: str):
        assert imageName not in self._imagesDownloading
        self._imagesDownloading[imageName] = True
        try:
            yield
        finally:
            del self._imagesDownloading[imageName]

    def image_exists(self, imageName: str) -> bool:
        with self.sftp() as sftp:
            fileNames = sftp.listdir()
            return imageName in fileNames

    async def identical_image_hash(self, imageName: str) -> bool:
        localImageHash = await self._get_local_image_hash(imageName)
        remoteImageHash = await self._get_remote_image_hash(imageName)
        return localImageHash == remoteImageHash

    async def _get_local_image_hash(self, imageName: str) -> str:
        with self.ssh() as ssh:
            command = f'sha256sum {shlex.quote(imageName)}'
            stdin, stdout, stderr = ssh.exec_command(command)
            while not stdout.channel.exit_status_ready():
                await asyncio.sleep(0.5)
            if stdout.channel.exit_status != 0:
                raise RuntimeError(command)
            return str(stdout.read(), encoding='utf-8').split(' ')[0]

    @staticmethod
    async def _get_remote_image_hash(imageName: str) -> str:
        async with aiohttp.ClientSession() as session:
            async with session.get(_IMAGE_NAME_2_HASH_URL[imageName]) as response:
                response = await response.text()
        for line in response.splitlines():
            if line.split(' *')[1] != imageName:
                continue
            return line.split(' *')[0]
        raise RuntimeError('Could not find remote image hash.')

    def delete_local_image(self, imageName: str) -> None:
        with self.sftp() as sftp:
            sftp.remove(imageName)

    # -----------------------------------------------

    @property
    def ipAddressPort(self) -> str:
        return f'{self.ipAddress}:{self.port}'

    @property
    def proxmoxApi(self) -> ProxmoxAPI:
        return ProxmoxAPI(
            self.ipAddressPort,
            user=self.secret.userName,
            password=self.secret.value,
            verify_ssl=self.verifySsl
        )

    @contextlib.contextmanager
    def ssh(self) -> paramiko.SSHClient:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        sshSecret = Secret('proxmox-ssh', 'root')
        ssh.connect(
            self.ipAddress,
            username=sshSecret.userName,
            password=sshSecret.value,
        )
        try:
            yield ssh
        finally:
            ssh.close()

    @contextlib.contextmanager
    def sftp(self) -> paramiko.SFTPClient:
        with self.ssh() as ssh:
            sftp = ssh.open_sftp()
            try:
                yield sftp
            finally:
                sftp.close()

    def iter_hypervisors(self) -> Generator[Hypervisor]:
        yield from self._resources.values()

    def iter_virtual_machines(self) -> Generator[VirtualMachine]:
        yield from self.virtualMachines.values()

    def __str__(self):
        return '{}'.format(self.name)


class Hypervisors(BaseResources):

    UNIQUE_KEY_NAMES = [
        UniqueKeyName(
            iterableNames='iter_hypervisors()',
            attributeNames=('name', )
        ),
    ]

    KEY_NAME = ('name', )

    def iter_hypervisors(self) -> Generator[Hypervisor]:
        yield from self.values()

    def get_virtual_machine_by_name(self, name: str) -> VirtualMachine:
        for hypervisor in self.values():
            for virtualMachine in hypervisor.virtualMachines.values():
                if virtualMachine.name != name:
                    continue
                return virtualMachine
        raise ValueError(f'Virtual machine {name} can not be found.')
