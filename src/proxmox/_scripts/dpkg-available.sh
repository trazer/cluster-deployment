#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

# lsof will return non-zero if file not found.
# Hence, we allow the command to fail with || true
# to prevent bash from exiting.
dpkg_lock=$(echo "${ROOT_PASSWORD}" | sudo -S lsof /var/lib/dpkg/lock || true)
dpkg_frontend_lock=$(echo "${ROOT_PASSWORD}" | sudo -S lsof /var/lib/dpkg/lock-frontend || true)
if echo "${dpkg_lock}" | grep -Fq "/var/lib/dpkg/lock"; then
    echo "false"
elif echo "${dpkg_frontend_lock}" | grep -Fq "/var/lib/dpkg/lock-frontend"; then
    echo "false"
else
    echo "true"
fi
