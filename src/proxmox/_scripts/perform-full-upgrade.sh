#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "${ROOT_PASSWORD}" | sudo -S apt-get update
echo "${ROOT_PASSWORD}" | sudo -S apt-get -y full-upgrade
