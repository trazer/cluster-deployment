import re
from typing import Union

import pingparsing

from clusterutils import retry


class StorageSize:

    _KILO_BYTE_2_BYTE = 1024
    _MEGA_BYTE_2_BYTE = 1024 * _KILO_BYTE_2_BYTE
    _GIGA_BYTE_2_BYTE = 1024 * _MEGA_BYTE_2_BYTE
    _TERA_BYTE_2_BYTE = 1024 * _GIGA_BYTE_2_BYTE

    _DIGITAL_SIZE_PATTERN_2_CONVERSION_FACTOR = {
        re.compile(r'(?P<value>[+-]?\d+(?:\.\d+)?)(?P<suffix>k)'): _KILO_BYTE_2_BYTE,
        re.compile(r'(?P<value>[+-]?\d+(?:\.\d+)?)(?P<suffix>M)'): _MEGA_BYTE_2_BYTE,
        re.compile(r'(?P<value>[+-]?\d+(?:\.\d+)?)(?P<suffix>G)'): _GIGA_BYTE_2_BYTE,
        re.compile(r'(?P<value>[+-]?\d+(?:\.\d+)?)(?P<suffix>T)'): _TERA_BYTE_2_BYTE,
    }

    def __init__(self, value: Union[str, float, int]):
        if isinstance(value, (float, int)):
            self._value = float(value)
        else:
            try:
                self._value = float(value)
            except ValueError:
                for pattern, conversionFactor in self._DIGITAL_SIZE_PATTERN_2_CONVERSION_FACTOR.items():
                    match = re.search(pattern, value)
                    if match is not None:
                        self._value = float(match.group('value')) * conversionFactor
                        break
                else:
                    raise ValueError(f'Invalid size "{value}".')

    @property
    def byte(self) -> float:
        return self._value

    @property
    def kilobyte(self) -> float:
        return self._value / self._KILO_BYTE_2_BYTE

    @property
    def megabyte(self) -> float:
        return self._value / self._MEGA_BYTE_2_BYTE

    @property
    def gigabyte(self) -> float:
        return self._value / self._GIGA_BYTE_2_BYTE

    @property
    def terabyte(self) -> float:
        return self._value / self._TERA_BYTE_2_BYTE


@retry(exceptions=[AttributeError])
def is_reachable(ipAddress: str) -> bool:

    NUMBER_OF_PACKETS = 3

    pingTransmitter = pingparsing.PingTransmitter()
    pingTransmitter.destination_host = ipAddress
    pingTransmitter.count = NUMBER_OF_PACKETS

    pingParser = pingparsing.PingParsing()
    pingResult = pingParser.parse(pingTransmitter.ping()).as_dict()

    return pingResult['packet_loss_count'] < NUMBER_OF_PACKETS
