from __future__ import annotations

import asyncio
import contextlib
import os
import re
import shlex
import stat
import textwrap
import urllib.parse
from ipaddress import IPv4Interface
from time import sleep
from typing import (
    Generator,
    List,
    TYPE_CHECKING,
    Tuple,
    Union,
)

import paramiko
from proxmoxer import (
    ProxmoxAPI,
    ResourceException as ProxmoxResourceException,
)

from clusterutils import (
    OPEN_SSH_PUBLIC_KEY_PATTERN,
    prepend_environment_to_command,
    retry,
)
from credentials import Secret
from ._base import (
    BaseResource,
    BaseResources,
    UniqueKeyName,
)
from ._utils import (
    StorageSize,
    is_reachable,
)

if TYPE_CHECKING:
    from ._hypervisor import Hypervisor


_CURRENT_PATH = os.path.split(__file__)[0]
_SCRIPTS_FOLDER_NAME = '_scripts'


class VirtualMachine(BaseResource):

    CLOUD_INIT_IDE_NAME = 'ide2'

    def __init__(
            self,
            hypervisor: Hypervisor,
            virtualMachines: VirtualMachines,
    ):
        super().__init__(parentResource=hypervisor, resources=virtualMachines)
        self.vmid: int = None
        self.name: str = None
        self.image: str = None
        self._memory: int = None
        self.cores: int = None
        self.disks: Disks = Disks(parentResource=self)
        self.networkDevices: NetworkDevices = NetworkDevices(parentResource=self)
        self._publicSshKeys: List[str] = []
        self.secret: Secret = None
        self.enableIpForwarding: bool = None

    @property
    def exists(self) -> bool:

        if not self.hypervisor.exists:
            return False

        return any(
            self.vmid == int(qemu['vmid'])
            for qemu in
            self.proxmoxApi.nodes(self.hypervisor.name).qemu.get()
        )

    def from_config(self, fromConfig) -> None:

        self.name = fromConfig['name']
        self.vmid = fromConfig['vmid']
        self.image = fromConfig['image']
        self.memory = fromConfig['memory']
        self.cores = fromConfig['cores']
        self.publicSshKeys = fromConfig['public_ssh_keys']

        for fromConfigDisk in fromConfig['disks']:
            disk = Disk(
                virtualMachine=self,
                disks=self.disks,
            )
            disk.from_config(fromConfigDisk)
            self.disks[disk.name] = disk

        for fromConfigNetworkDevice in fromConfig['network_devices']:
            networkDevice = NetworkDevice(
                virtualMachine=self,
                networkDevices=self.networkDevices,
            )
            networkDevice.from_config(fromConfigNetworkDevice)
            self.networkDevices[networkDevice.name] = networkDevice

        self.secret = Secret(
            fromConfig['credentials']['service_name'],
            fromConfig['credentials']['user_name'],
        )

        assert isinstance(fromConfig['enable_ip_forwarding'], bool)
        self.enableIpForwarding = fromConfig['enable_ip_forwarding']

    @property
    def publicSshKeys(self) -> List[str]:
        return self._publicSshKeys

    @publicSshKeys.setter
    def publicSshKeys(self, values: List[str]):
        if not all(isinstance(value, str) for value in values):
            raise TypeError()
        if not all(re.search(OPEN_SSH_PUBLIC_KEY_PATTERN, value) is not None
                   for value in values):
            raise ValueError()
        self._publicSshKeys = values

    async def create(self) -> None:

        parameters = {
            'vmid': self.vmid,
            'name': self.name,
            'cores': self.cores,
            'memory': int(StorageSize(self.memory).megabyte),
            'scsihw': 'virtio-scsi-pci',
            'ciuser': self.secret.userName,
            'cipassword': self.secret.value,
            'sshkeys': urllib.parse.quote('\n'.join(self.publicSshKeys), safe=''),
            self.CLOUD_INIT_IDE_NAME: 'local-lvm:cloudinit',
        }

        self.proxmoxApi. \
            nodes(self.hypervisor.name). \
            qemu.create(**parameters)

        for disk in self.disks.values():
            if disk.bootDisk:
                await self._download_image()
                await self._import_image_to_disk()
                disk.mount()
                disk.resize()
                disk.set_boot()
            else:
                disk.create()

        for networkDevice in self.networkDevices.values():
            networkDevice.create()

            for networkInterface in networkDevice.networkInterfaces.values():
                networkInterface.create()

        # Configure vga and serial (terminal) devices
        config = {
            'vga': 'std',
            'serial0': 'socket',
        }
        self.proxmoxApi. \
            nodes(self.hypervisor.name). \
            qemu(self.vmid). \
            config.post(**config)

    def delete(self) -> None:
        self.proxmoxApi. \
            nodes(self.hypervisor.name). \
            qemu(self.vmid).delete()

    async def _download_image(self) -> None:

        imageExists = self.hypervisor.image_exists(self.image)

        if imageExists and \
                not await self.hypervisor.identical_image_hash(self.image) \
                and not self.hypervisor.image_is_already_downloading(self.image):
            print(f'Remote and local hashes differ for {self.image}. Re-downloading.')
            self.hypervisor.delete_local_image(self.image)
            await self.hypervisor.download_image(self.image)

        elif imageExists and \
                not await self.hypervisor.identical_image_hash(self.image) \
                and self.hypervisor.image_is_already_downloading(self.image):
            print(f'{self.image} is already downloading on {self.hypervisor}...waiting')
            while self.hypervisor.image_is_already_downloading(self.image):
                await asyncio.sleep(1)
            await asyncio.sleep(0.5)
            assert await self.hypervisor.identical_image_hash(self.image)

        elif not imageExists:
            print(f'Downloading {self.image}.')
            await self.hypervisor.download_image(self.image)

        else:
            print(f'Local {self.image} already up-to-date.')

    async def _import_image_to_disk(self) -> None:
        with self.hypervisor.ssh() as ssh:
            command = 'qm importdisk {vmid} {source} {storage}'
            command = command.format(
                vmid=shlex.quote(str(self.vmid)),
                source=shlex.quote(self.image),
                storage=shlex.quote('local-lvm'),
            )
            stdin, stdout, stderr = ssh.exec_command(command)
            while not stdout.channel.exit_status_ready():
                await asyncio.sleep(0.5)
            if stdout.channel.exit_status != 0:
                raise RuntimeError(f'{command}\n{stdout.read()}\n{stderr.read()}')

    async def full_upgrade(self) -> None:

        while not await self.dpkgReady:
            print(f'Waiting for dpkg on {self}.')
            await asyncio.sleep(1)
        print(f'dpkg ready on {self}.')

        SCRIPT_FILE_NAME = 'perform-full-upgrade.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            SCRIPT_FILE_NAME,
        )
        output, errorOutput = await self.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{SCRIPT_FILE_NAME}',
            environment={'ROOT_PASSWORD': self.secret.value},
        )

    @property
    async def dpkgReady(self) -> bool:
        SCRIPT_FILE_NAME = 'dpkg-available.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            SCRIPT_FILE_NAME,
        )
        output, errorOutput = await self.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{SCRIPT_FILE_NAME}',
            environment={'ROOT_PASSWORD': self.secret.value},
        )
        output = output.strip('\n')
        if output == 'true':
            return True
        elif output == 'false':
            return False
        else:
            raise ValueError(output)

    @property
    def on(self) -> bool:
        return self._status == 'running'

    @property
    def off(self) -> bool:
        return self._status == 'stopped'

    @property
    def _status(self) -> str:
        current = self.proxmoxApi. \
            nodes(self.hypervisor.name). \
            qemu(self.vmid). \
            status. \
            current.get()
        assert current['status'] in ['stopped', 'running']
        return current['status']

    def restart(self) -> None:
        self.shutdown()
        self.start(wait=True)

    # TODO: Make on async method
    async def restart_async(self) -> None:
        await self.shutdown_async()
        await self.start_async(wait=True)

    def start(self, wait: bool = True) -> None:
        self.proxmoxApi. \
            nodes(self.hypervisor.name). \
            qemu(self.vmid). \
            status. \
            start.post()

        if wait:
            while not self.reachable:
                sleep(1)

    # TODO: Make on async method
    async def start_async(self, wait: bool = True) -> None:
        self.proxmoxApi. \
            nodes(self.hypervisor.name). \
            qemu(self.vmid). \
            status. \
            start.post()

        if wait:
            while not self.reachable:
                await asyncio.sleep(1)

    def reset(self) -> None:
        self.proxmoxApi. \
            nodes(self.hypervisor.name). \
            qemu(self.vmid). \
            status. \
            reset.post()

        # Make sure the reset process
        # has actually started
        sleep(5)

        while not self.reachable:
            sleep(1)

    async def stop(self) -> None:
        self.proxmoxApi. \
            nodes(self.hypervisor.name). \
            qemu(self.vmid). \
            status. \
            stop.post()
        # Wait for virtual machine to be off
        while not self.off:
            await asyncio.sleep(1)

    def shutdown(self, now: bool = False) -> None:

        with self.ssh() as ssh:
            if now:
                command = f'{self.secret.value} | sudo -S shutdown -h now'
            else:
                command = f'{self.secret.value} | sudo -S shutdown'
            stdin, stdout, stderr = ssh.exec_command(command)
            while not stdout.channel.exit_status_ready():
                sleep(1)
            if stdout.channel.exit_status != 0:
                raise RuntimeError(command)

        # Wait for virtual machine to be off
        while not self.off:
            sleep(1)

    # TODO: Make a single async method
    async def shutdown_async(self, now: bool = False) -> None:
        with self.ssh() as ssh:
            if now:
                command = f'{self.secret.value} | sudo -S shutdown -h now'
            else:
                command = f'{self.secret.value} | sudo -S shutdown'
            stdin, stdout, stderr = ssh.exec_command(command)
            while not stdout.channel.exit_status_ready():
                await asyncio.sleep(1)
            if stdout.channel.exit_status != 0:
                raise RuntimeError(command)

        # Wait for virtual machine to be off
        while not self.off:
            await asyncio.sleep(1)

    @property
    def memory(self) -> int:
        return self._memory

    @memory.setter
    def memory(self, value: Union[str, float, int]) -> None:
        self._memory = int(StorageSize(value).byte)

    # TODO: Make async
    def execute_local_script_on_remote(
            self,
            localFilePath: str,
            remoteFilePath: str,
            environment: dict = None,
            environmentQuoteTypes: dict = None,
    ) -> Tuple[str, str]:

        if environment is None:
            environment = {}

        # Transfer script to remote server
        with self.sftp() as sftp:
            sftp.put(
                localpath=localFilePath,
                remotepath=remoteFilePath,
            )
            # stat.S_IRWXU − Read, write, and execute by owner.
            # stat.S_IWGRP − Write by group.
            # stat.S_IRWXG − Read, write, and execute by group.
            # stat.S_IWOTH − Write by others.
            # stat.S_IRWXO − Read, write, and execute by others.
            sftp.chmod(
                remoteFilePath,
                stat.S_IRWXU | stat.S_IWGRP | stat.S_IWOTH,
            )

        # We manually declare environment variables here
        # because passing the environment to exec_command()
        # below won't always work because the SSH server does
        # not necessarily accept environment variables. See
        # AcceptEnv setting in the server's SSH config file.
        command = f'{remoteFilePath}'
        command = prepend_environment_to_command(command, environment, environmentQuoteTypes)
        with self.ssh() as ssh:
            stdin, stdout, stderr = ssh.exec_command(command)
            while not stdout.channel.exit_status_ready():
                sleep(0.5)
            output = str(stdout.read(), encoding='UTF-8')
            errorOutput = str(stderr.read(), encoding='UTF-8')
            if stdout.channel.exit_status != 0:
                MSG = (
                    f'The following command has failed with '
                    f'status {stdout.channel.exit_status} on {self}:\n'
                    f'{textwrap.indent(command, prefix=" " * 4)}\n'
                    f'stdout:\n'
                    f'{textwrap.indent(output, prefix=" " * 4)}\n'
                    f'stderr:\n'
                    f'{textwrap.indent(errorOutput, prefix=" " * 4)}'
                )
                raise RuntimeError(MSG)
            return output, errorOutput

    # TODO: Remove once execute_local_script_on_remote is async
    async def execute_local_script_on_remote_async(
            self,
            localFilePath: str,
            remoteFilePath: str,
            environment: dict = None,
            environmentQuoteTypes: dict = None,
    ) -> Tuple[str, str]:

        if environment is None:
            environment = {}

        # Transfer script to remote server
        with self.sftp() as sftp:
            sftp.put(
                localpath=localFilePath,
                remotepath=remoteFilePath,
            )
            # stat.S_IRWXU − Read, write, and execute by owner.
            # stat.S_IWGRP − Write by group.
            # stat.S_IRWXG − Read, write, and execute by group.
            # stat.S_IWOTH − Write by others.
            # stat.S_IRWXO − Read, write, and execute by others.
            sftp.chmod(
                remoteFilePath,
                stat.S_IRWXU | stat.S_IWGRP | stat.S_IWOTH,
            )

        # We manually declare environment variables here
        # because passing the environment to exec_command()
        # below won't always work because the SSH server does
        # not necessarily accept environment variables. See
        # AcceptEnv setting in the server's SSH config file.
        command = f'{remoteFilePath}'
        command = prepend_environment_to_command(command, environment, environmentQuoteTypes)
        with self.ssh() as ssh:
            stdin, stdout, stderr = ssh.exec_command(command)
            while not stdout.channel.exit_status_ready():
                await asyncio.sleep(0.5)
            output = str(stdout.read(), encoding='UTF-8')
            errorOutput = str(stderr.read(), encoding='UTF-8')
            if stdout.channel.exit_status != 0:
                MSG = (
                    f'The following command has failed with '
                    f'status {stdout.channel.exit_status} on {self}:\n'
                    f'{textwrap.indent(command, prefix=" " * 4)}\n'
                    f'stdout:\n'
                    f'{textwrap.indent(output, prefix=" " * 4)}\n'
                    f'stderr:\n'
                    f'{textwrap.indent(errorOutput, prefix=" " * 4)}'
                )
                raise RuntimeError(MSG)
            return output, errorOutput

    @contextlib.contextmanager
    @retry(
        maximumNumberOfRetries=3,
        minimumBackoffTime=5000,
        maximumBackoffTime=15000,
        exceptions=[paramiko.AuthenticationException],
    )
    def ssh(self) -> paramiko.SSHClient:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        try:
            self._ssh_connect_with_password_based_auth(ssh)
        except paramiko.BadAuthenticationType:
            try:
                self._ssh_connect_with_unencrypted_ssh_key(ssh)
            except paramiko.PasswordRequiredException:
                self._ssh_connect_with_encrypted_ssh_key(ssh)
        except paramiko.PasswordRequiredException:
            self._ssh_connect_with_encrypted_ssh_key(ssh)

        try:
            yield ssh
        finally:
            ssh.close()

    def _ssh_connect_with_password_based_auth(self, ssh: paramiko.SSHClient) -> None:
        ssh.connect(
            self._ipAddressForSsh,
            username=self.secret.userName,
            password=self.secret.value,
        )

    def _ssh_connect_with_unencrypted_ssh_key(self, ssh: paramiko.SSHClient) -> None:
        ssh.connect(
            self._ipAddressForSsh,
        )

    def _ssh_connect_with_encrypted_ssh_key(self, ssh: paramiko.SSHClient) -> None:
        if 'PKEY_SERVICE_NAME' not in os.environ \
                or 'PKEY_USER_NAME' not in os.environ:
            raise EnvironmentError(
                'Missing PKEY_SERVICE_NAME and/or PKEY_USER_NAME.')
        privateKeySecret = Secret(
            os.environ['PKEY_SERVICE_NAME'],
            os.environ['PKEY_USER_NAME'],
        )
        ssh.connect(
            self._ipAddressForSsh,
            passphrase=privateKeySecret.value,
        )

    @contextlib.contextmanager
    def sftp(self) -> paramiko.SFTPClient:
        with self.ssh() as ssh:
            sftp = ssh.open_sftp()
            try:
                yield sftp
            finally:
                sftp.close()

    @property
    def _ipAddressForSsh(self) -> str:
        for networkDevice in self.networkDevices.values():
            for networkInterface in networkDevice.networkInterfaces.values():
                if not is_reachable(networkInterface.ipAddress):
                    continue
                return networkInterface.ipAddress
        raise RuntimeError(f'Cannot find reachable interface for {self}.')

    @property
    def reachable(self) -> bool:
        """
        Checks if virtual machine is reachable
         through least one interface.
        """
        for networkDevice in self.networkDevices.values():
            for networkInterface in networkDevice.networkInterfaces.values():
                if is_reachable(networkInterface.ipAddress):
                    return True
        return False

    @property
    def proxmoxApi(self) -> ProxmoxAPI:
        return self.hypervisor.proxmoxApi

    @property
    def hypervisor(self) -> Hypervisor:
        from ._hypervisor import Hypervisor
        assert isinstance(self._parentResource, Hypervisor)
        return self._parentResource

    def iter_disks(self) -> Generator[Disk]:
        yield from self.disks.values()

    def iter_network_devices(self) -> Generator[NetworkDevice]:
        yield from self.networkDevices.values()

    def iter_network_interfaces(self) -> Generator[NetworkInterface]:
        for networkDevice in self.networkDevices.values():
            yield from networkDevice.networkInterfaces.values()

    def __str__(self):
        return '{}/{}'.format(self.hypervisor, self.name)


class VirtualMachines(BaseResources):

    UNIQUE_KEY_NAMES = [
        UniqueKeyName(  # vmid must be unique within the hypervisor
            iterableNames='hypervisor.iter_virtual_machines()',
            attributeNames=('vmid', )
        ),
        UniqueKeyName(  # name must be unique within all the hypervisors
            iterableNames='hypervisor.iter_hypervisors().iter_virtual_machines()',
            attributeNames=('name', )
        ),
    ]

    KEY_NAME = ('vmid', )

    @property
    def hypervisor(self) -> Hypervisor:
        from ._hypervisor import Hypervisor
        assert isinstance(self._parentResource, Hypervisor)
        return self._parentResource


# ------------------------------------------------


class Disk(BaseResource):

    def __init__(
            self,
            virtualMachine: VirtualMachine,
            disks: Disks,
    ):
        super().__init__(parentResource=virtualMachine, resources=disks)
        self.name = None
        self._size = None
        self.bootDisk = None

    def from_config(self, fromConfigDisk) -> None:
        self.name = fromConfigDisk['name']
        self.size = fromConfigDisk['size']
        self.bootDisk = fromConfigDisk['boot_disk']

    @property
    def size(self) -> int:
        return self._size

    @size.setter
    def size(self, value: Union[str, int, float]):
        self._size = int(StorageSize(value).byte)

    @property
    def fileName(self) -> str:
        return f'vm-{self.virtualMachine.vmid}-disk-{self.scsiIndex}'

    @property
    def scsiIndex(self) -> int:
        return int(re.search(r'scsi(?P<index>\d+)', self.name).group('index'))

    def create(self) -> None:
        assert not self.exists
        assert not self.fileExists
        self.allocate()
        self.mount()

    def allocate(self) -> None:
        assert not self.exists
        assert not self.fileExists
        self.proxmoxApi. \
            nodes(self.virtualMachine.hypervisor.name). \
            storage('local-lvm'). \
            content.post(
                filename=self.fileName,
                size=int(StorageSize(self.size).kilobyte),
                vmid=self.virtualMachine.vmid,
            )

    def set_boot(self) -> None:
        assert self.bootDisk
        self.proxmoxApi. \
            nodes(self.virtualMachine.hypervisor.name). \
            qemu(self.virtualMachine.vmid). \
            config.post(bootdisk=self.name)

    @property
    def exists(self) -> bool:
        config = self.proxmoxApi. \
            nodes(self.virtualMachine.hypervisor.name). \
            qemu(self.virtualMachine.vmid). \
            config.get()
        return self.name in config

    def mount(self) -> None:
        assert not self.mounted
        assert self.fileExists
        config = {self.name: f'local-lvm:{self.fileName}'}
        self.proxmoxApi. \
            nodes(self.virtualMachine.hypervisor.name). \
            qemu(self.virtualMachine.vmid). \
            config.post(**config)

    @property
    def mounted(self) -> bool:
        config = self.proxmoxApi. \
            nodes(self.virtualMachine.hypervisor.name). \
            qemu(self.virtualMachine.vmid). \
            config.get()
        try:
            return f'local-lvm:{self.fileName}' in config[self.name].split(',')
        except KeyError:
            return False

    @property
    def fileExists(self) -> bool:
        content = self.proxmoxApi. \
            nodes(self.virtualMachine.hypervisor.name). \
            storage('local-lvm'). \
            content.get()
        return any(file['volid'] == 'local-lvm:{}'.format(self.fileName) for file in content)

    @retry(exceptions=[ProxmoxResourceException])
    def resize(self) -> None:
        self.proxmoxApi.nodes(self.virtualMachine.hypervisor.name). \
            qemu(self.virtualMachine.vmid). \
            resize.put(
            disk=self.name,
            size=self.size,
        )

    @property
    def proxmoxApi(self) -> ProxmoxAPI:
        return self.hypervisor.proxmoxApi

    @property
    def virtualMachine(self) -> VirtualMachine:
        assert isinstance(self._parentResource, VirtualMachine)
        return self._parentResource

    @property
    def hypervisor(self) -> Hypervisor:
        from ._hypervisor import Hypervisor
        assert isinstance(self.virtualMachine._parentResource, Hypervisor)
        return self.virtualMachine._parentResource

    def __str__(self):
        return '{}/{}'.format(self.virtualMachine, self.name)


class Disks(BaseResources):

    UNIQUE_KEY_NAMES = [
        UniqueKeyName(
            iterableNames='virtualMachine.iter_disks()',
            attributeNames=('name', )
        ),
    ]

    KEY_NAME = ('name', )

    def __setitem__(self, name: str, disk: Disk):

        if disk.bootDisk \
                and sum(1 for disk in self.values() if disk.bootDisk) >= 1:
            bootDisk = next(disk for disk in self.values() if disk.bootDisk)
            raise ValueError(f'Cannot have boot disk "{disk}" '
                             f'because "{bootDisk}" already '
                             f'is the boot disk.')

        super().__setitem__(name, disk)

    @property
    def hypervisor(self) -> Hypervisor:
        from ._hypervisor import Hypervisor
        assert isinstance(self.virtualMachine.hypervisor, Hypervisor)
        return self.virtualMachine.hypervisor

    @property
    def virtualMachine(self) -> VirtualMachine:
        assert isinstance(self._parentResource, VirtualMachine)
        return self._parentResource


# ------------------------------------------------


class NetworkDevice(BaseResource):

    def __init__(
            self,
            virtualMachine: VirtualMachine,
            networkDevices: NetworkDevices,
    ):
        super().__init__(parentResource=virtualMachine, resources=networkDevices)
        self.name = None
        self.bridge = None
        self._vid = None
        self.networkInterfaces = NetworkInterfaces(parentResource=self)

    def from_config(self, fromConfigNetworkDevice) -> None:

        self.name = fromConfigNetworkDevice['name']
        self.bridge = fromConfigNetworkDevice['bridge']
        self.vid = fromConfigNetworkDevice['vid']

        for fromConfigNetworkInterface in fromConfigNetworkDevice['network_interfaces']:
            networkInterface = NetworkInterface(
                networkDevice=self,
                networkInterfaces=self.networkInterfaces,
            )
            networkInterface.from_config(fromConfigNetworkInterface)
            self.networkInterfaces[networkInterface.name] = networkInterface

    def create(self) -> None:
        assert not self.exists
        config = [
            'virtio',
            f'bridge={self.bridge}',
        ]
        if self.vid > 0:
            if not self._bridged_network_device_is_vlan_aware():
                msg = (
                    f'Can\'t add tag "{self.vid}" '
                    f'to "{self}" since the bridge '
                    f'device "{self.bridge}" on '
                    f'"{self.hypervisor}" is not '
                    f'properly configured for VLAN.'
                )
                raise ValueError(msg)
            config.append(f'tag={self.vid}')
        self.proxmoxApi. \
            nodes(self.virtualMachine.hypervisor.name). \
            qemu(self.virtualMachine.vmid). \
            config.post(**{self.name: ','.join(config)})

    def _bridged_network_device_is_vlan_aware(self) -> bool:
        """
        Check that the network device on the hypervisor
        has the adequate VLAN configuration.
        """

        bridgedNetworkInterface = self.proxmoxApi. \
            nodes(self.hypervisor.name). \
            network(self.bridge).get()

        try:
            if bridgedNetworkInterface['bridge_vlan_aware'] != 1:
                return False
        except KeyError:
            return False

        if not bridgedNetworkInterface['bridge_ports'].endswith(f'.{self.vid}'):
            return False

        return True

    @property
    def vid(self) -> int:
        return self._vid

    @vid.setter
    def vid(self, value: Union[int, str]) -> None:
        self._vid = int(value)

    @property
    def exists(self) -> bool:
        config = self.proxmoxApi. \
            nodes(self.virtualMachine.hypervisor.name). \
            qemu(self.virtualMachine.vmid). \
            config.get()
        return self.name in config

    @property
    def actualMacAddress(self) -> str:
        config = self.proxmoxApi. \
            nodes(self.virtualMachine.hypervisor.name). \
            qemu(self.virtualMachine.vmid). \
            config.get()
        actualNetworkDevice = config[self.name]
        PATTERN = re.compile(
            r'''
            (?:
            (?:macaddr=)
            |
            (?:virtio=)
            )
            (?P<macAddress>
            (?:[A-Fa-f0-9]{2}:){5}
            (?:[A-Fa-f0-9]{2})
            )
            ''', re.VERBOSE
        )
        match = re.search(PATTERN, actualNetworkDevice)
        return match.group('macAddress').upper()

    @property
    def proxmoxApi(self) -> ProxmoxAPI:
        return self.hypervisor.proxmoxApi

    @property
    def virtualMachine(self) -> VirtualMachine:
        assert isinstance(self._parentResource, VirtualMachine)
        return self._parentResource

    @property
    def hypervisor(self) -> Hypervisor:
        from ._hypervisor import Hypervisor
        assert isinstance(self.virtualMachine._parentResource, Hypervisor)
        return self.virtualMachine._parentResource

    def __str__(self):
        return '{}/{}'.format(self.virtualMachine, self.name)


class NetworkDevices(BaseResources):

    UNIQUE_KEY_NAMES = [
        UniqueKeyName(
            iterableNames='virtualMachine.iter_network_devices()',
            attributeNames=('name', )
        ),
    ]

    KEY_NAME = ('name', )

    @property
    def virtualMachine(self) -> VirtualMachine:
        assert isinstance(self._parentResource, VirtualMachine)
        return self._parentResource

    def by_name(self, name: str) -> NetworkDevice:
        return self[name]


# ------------------------------------------------


class NetworkInterface(BaseResource):

    def __init__(
            self,
            networkDevice: NetworkDevice,
            networkInterfaces: NetworkInterfaces,
    ):
        super().__init__(
            parentResource=networkDevice,
            resources=networkInterfaces,
        )
        self.name: str = None
        self._ipCidr: IPv4Interface = None
        self.gateway: str = None

    def from_config(self, fromConfigNetworkInterface) -> None:
        self.name = fromConfigNetworkInterface['name']
        self.ipCidr = fromConfigNetworkInterface['ip_cidr']
        self.gateway = fromConfigNetworkInterface['gateway']

    @property
    def ipAddress(self) -> str:
        return str(self._ipCidr.ip)

    @property
    def ipCidr(self) -> str:
        return self._ipCidr.with_prefixlen

    @ipCidr.setter
    def ipCidr(self, value: str):
        self._ipCidr = IPv4Interface(value)

    def create(self) -> None:
        assert not self.exists
        if self.gateway == '':
            config = [
                f'ip={self.ipCidr}',
            ]
        else:
            config = [
                f'gw={self.gateway}',
                f'ip={self.ipCidr}',
            ]
        self.proxmoxApi. \
            nodes(self.networkDevice.virtualMachine.hypervisor.name). \
            qemu(self.networkDevice.virtualMachine.vmid). \
            config.post(**{self.name: ','.join(config)})

    @property
    def exists(self) -> bool:
        config = self.proxmoxApi. \
            nodes(self.hypervisor.name). \
            qemu(self.virtualMachine.vmid). \
            config.get()
        return self.name in config

    @property
    def proxmoxApi(self) -> ProxmoxAPI:
        return self.hypervisor.proxmoxApi

    @property
    def hypervisor(self) -> Hypervisor:
        from ._hypervisor import Hypervisor
        assert isinstance(self.virtualMachine._parentResource, Hypervisor)
        return self.virtualMachine._parentResource

    @property
    def virtualMachine(self) -> VirtualMachine:
        assert isinstance(self.networkDevice._parentResource, VirtualMachine)
        return self.networkDevice._parentResource

    @property
    def networkDevice(self) -> NetworkDevice:
        assert isinstance(self._parentResource, NetworkDevice)
        return self._parentResource

    def __str__(self):
        return '{}/{}'.format(self.networkDevice, self.name)


class NetworkInterfaces(BaseResources):

    UNIQUE_KEY_NAMES = [
        UniqueKeyName(
            iterableNames='virtualMachine.iter_network_interfaces()',
            attributeNames=('name', )
        ),
    ]

    KEY_NAME = ('name', )

    @property
    def virtualMachine(self) -> VirtualMachine:
        assert isinstance(self._parentResource._parentResource, VirtualMachine)
        return self._parentResource._parentResource

    def by_name(self, name: str) -> NetworkInterface:
        return self[name]
