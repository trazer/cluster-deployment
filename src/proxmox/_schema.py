import os

from clusterutils import YamlSchemaValidator

_CURRENT_PATH = os.path.split(__file__)[0]
_SCHEMA_FOLDER_NAME = '_schemas'


def validate_virtual_machines_schema(virtualMachinesFilePath):
    _validate_schema(virtualMachinesFilePath, 'virtual_machines_schema.yaml')


def _validate_schema(filePath, schemaFileName):
    yamlSchemaValidator = YamlSchemaValidator(
        source_file=filePath,
        schema_files=[os.path.join(_CURRENT_PATH, _SCHEMA_FOLDER_NAME, schemaFileName)]
    )
    yamlSchemaValidator.validate(raise_exception=True)
