import abc
from operator import attrgetter
from typing import (
    List,
    Tuple,
    Union,
)


# TODO: Use clusterutils.resources instead


class UniqueKeyName:

    __slots__ = [
        'iterableNames',
        'attributeNames',
    ]

    def __init__(
            self,
            iterableNames: str,
            attributeNames: Tuple[str, ...],
    ):
        self.iterableNames = iterableNames.split('.')
        self.attributeNames = attributeNames


class BaseResource(abc.ABC):

    def __init__(self, parentResource=None, resources=None):
        self._parentResource = parentResource
        self._resources = resources

    @abc.abstractmethod
    def from_config(self, fromConfigResource):
        pass


class BaseResources(dict):

    UNIQUE_KEY_NAMES: List[UniqueKeyName] = []

    KEY_NAME: Tuple[str] = ()

    def __init__(self, parentResource: BaseResource=None):
        self._parentResource = parentResource
        super().__init__()

    def __setitem__(self, key: Union[str, int], resource: BaseResource):

        assert key is not None
        assert all(
            isinstance(resource_, type(resource))
            for resource_ in resource._resources.values()
        )

        if self.__does_resource_exist(resource):
            raise DuplicatedResourceError(key)

        if key != attrgetter(*self.KEY_NAME)(resource):
            raise KeyError(key)

        super().__setitem__(key, resource)

    def __does_resource_exist(self, resource):

        for uniqueKeyName in self.UNIQUE_KEY_NAMES:

            # Make sure attributes exist
            try:
                _ = [attrgetter(attrName)(resource) for attrName in uniqueKeyName.attributeNames]
            except AttributeError as e:
                raise AttributeError(f'"{uniqueKeyName.attributeNames}" not in "{resource}".') from e

            # Check if current key does not already exist
            if attrgetter(*uniqueKeyName.attributeNames)(resource) in self.__get_unique_keys(uniqueKeyName):
                msg = (
                    f'{attrgetter(*uniqueKeyName.attributeNames)(resource)} '
                    f'already exists for {resource}.'
                )
                raise DuplicatedResourceError(msg)

    def __get_unique_keys(self, uniqueKeyName):
        uniqueKeys = []
        for resource in self.__recursive_resources_iterator(self, uniqueKeyName.iterableNames):
            uniqueKeys.append(attrgetter(*uniqueKeyName.attributeNames)(resource))
        return uniqueKeys

    @classmethod
    def __recursive_resources_iterator(cls, obj, iterableNames):

        if len(iterableNames) == 0:
            yield obj
            return
        else:
            iterableName = iterableNames[0]

        if iterableName.endswith('()'):
            for childObj in getattr(obj, iterableName.rstrip('()'))():
                yield from cls.__recursive_resources_iterator(childObj, iterableNames[1:])
        else:
            childObj = getattr(obj, iterableName)
            yield from cls.__recursive_resources_iterator(childObj, iterableNames[1:])


class DuplicatedResourceError(Exception):
    pass
