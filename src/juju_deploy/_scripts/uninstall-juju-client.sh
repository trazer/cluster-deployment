#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "${ROOT_PASSWORD}" | sudo -S apt-get remove -y "${PACKAGE_NAME}"
echo "${ROOT_PASSWORD}" | sudo -S apt-get autoremove -y
