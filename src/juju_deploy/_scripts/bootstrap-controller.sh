#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

juju bootstrap "${CLOUD_NAME}" --to "${MACHINE_FQDN}"
