#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

juju destroy-controller -y --destroy-all-models --destroy-storage "${CLOUD_NAME}"

juju remove-cloud "${CLOUD_NAME}"
