#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

echo "${ROOT_PASSWORD}" | sudo -S add-apt-repository -y "${PACKAGE_REPOSITORY}"
echo "${ROOT_PASSWORD}" | sudo -S apt-get update
echo "${ROOT_PASSWORD}" | sudo -S apt-get install -y "${PACKAGE_NAME}"
