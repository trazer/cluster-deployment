#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

juju add-cloud --replace "${CLOUD_NAME}" "${CLOUDS_DEFINITION_FILE_PATH}"

juju add-credential --replace "${CLOUD_NAME}" -f "${CLOUDS_CREDENTIALS_FILE_PATH}"
