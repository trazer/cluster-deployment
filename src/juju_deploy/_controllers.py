from __future__ import annotations

import asyncio
import json
import os
import random
from string import (
    ascii_letters,
    digits,
)
from tempfile import TemporaryDirectory
from typing import List

from ruamel.yaml import YAML

from clusterutils import (
    async_retry,
)
from clusterutils.resources import (
    Resource,
    ResourceKeyValidator,
    Resources,
)
from maas_deploy import (
    MaasServer,
    load_maas_server,
)
from proxmox import (
    Hypervisors,
    load_virtual_machines_from_config,
)
from ._client import (
    JujuClient,
    JujuClients,
    _load_juju_clients,
    _validate_schema,
)

_CURRENT_PATH = os.path.split(__file__)[0]
_SCHEMA_FOLDER_NAME = '_schemas'
_SCRIPTS_FOLDER_NAME = '_scripts'

_CLOUD_NAME = 'maas'
_TLD = 'maas'


def bootstrap_juju_controllers(
        virtualMachinesFilePaths: List[str],
        maasControllersFilePaths: List[str],
        maasNetworksFilePaths: List[str],
        jujuControllersFilePaths: List[str],
) -> None:

    hypervisors = Hypervisors()
    load_virtual_machines_from_config(
        virtualMachinesFilePaths,
        hypervisors,
    )

    maasServer = MaasServer(hypervisors)
    load_maas_server(
        maasServer,
        maasControllersFilePaths,
        maasNetworksFilePaths,
    )

    jujuClients = JujuClients(hypervisors, maasServer)
    _load_juju_clients(
        jujuClients,
        jujuControllersFilePaths,
    )

    jujuControllers = JujuControllers(jujuClients, maasServer)
    _load_juju_controllers(
        jujuControllers,
        jujuControllersFilePaths,
    )

    asyncio.run(
        _bootstrap_juju_controllers(jujuControllers)
    )


async def _bootstrap_juju_controllers(jujuControllers: JujuControllers) -> None:

    assert len(jujuControllers) == 1

    jujuController = next(iter(jujuControllers.values()))

    if await jujuController.bootstrapped:
        raise NotImplementedError()

    print(f'Bootstrapping controller on {jujuController}.')
    await jujuController.bootstrap()


def destroy_juju_controllers(
        virtualMachinesFilePaths: List[str],
        maasControllersFilePaths: List[str],
        maasNetworksFilePaths: List[str],
        jujuControllersFilePaths: List[str],
) -> None:

    hypervisors = Hypervisors()
    load_virtual_machines_from_config(
        virtualMachinesFilePaths,
        hypervisors,
    )

    maasServer = MaasServer(hypervisors)
    load_maas_server(
        maasServer,
        maasControllersFilePaths,
        maasNetworksFilePaths,
    )

    jujuClients = JujuClients(hypervisors, maasServer)
    _load_juju_clients(
        jujuClients,
        jujuControllersFilePaths,
    )

    jujuControllers = JujuControllers(jujuClients, maasServer)
    _load_juju_controllers(
        jujuControllers,
        jujuControllersFilePaths,
    )

    asyncio.run(
        _destroy_juju_controllers(jujuControllers)
    )


async def _destroy_juju_controllers(jujuControllers: JujuControllers) -> None:

    assert len(jujuControllers) == 1

    jujuController = next(iter(jujuControllers.values()))

    print(f'Destroying {jujuController}.')
    await jujuController.destroy()


def _load_juju_controllers(
    jujuControllers: JujuControllers,
    jujuControllersFilePaths: List[str],
) -> None:

    assert len(jujuControllersFilePaths) == 1

    _validate_schema(
        jujuControllersFilePaths[0],
        schemaFileName='juju_controllers_schema.yaml',
    )

    config = {}
    yaml = YAML(typ='safe')
    with open(jujuControllersFilePaths[0], 'rt') as file:
        config.update(yaml.load(file))

    if len(config['juju_controllers']) > 1:
        raise NotImplementedError()

    for jujuControllerConfig in config['juju_controllers']['machines']:
        jujuController = JujuController(jujuControllers)
        jujuController.from_config(jujuControllerConfig)
        jujuControllers[jujuController.name] = jujuController


class JujuControllers(Resources):

    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='values()',
            attributeNames=('name', ),
        ),
    ]

    KEY_NAME = ('name', )

    def __init__(self, jujuClients: JujuClients, maasServer: MaasServer):
        super().__init__(parentResource=None)
        self.jujuClients = jujuClients
        self.maasServer = maasServer

    def __str__(self):
        return f'Juju controllers'


class JujuController(Resource):

    def __init__(self, jujuControllers: JujuControllers):
        super().__init__(
            parentResource=None,
            resources=jujuControllers,
        )
        self.name: str = None

    def from_config(self, config: dict) -> None:
        self.name = config['name']
        # TODO: Check machine existence

    @property
    async def bootstrapped(self) -> bool:
        actualControllers = await self._actualControllers
        if actualControllers['controllers'] is None:
            return False
        return _CLOUD_NAME in actualControllers['controllers']

    @property
    async def _actualControllers(self) -> dict:
        INSTALL_SCRIPT_FILE_NAME = 'get-juju-controllers.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            INSTALL_SCRIPT_FILE_NAME
        )
        virtualMachine = self.jujuClient.virtualMachine
        environment = {}
        output, errorOutput = await virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{INSTALL_SCRIPT_FILE_NAME}',
            environment=environment,
        )
        actualControllers = json.loads(output)
        return actualControllers

    @async_retry(exceptions=[RuntimeError])
    async def bootstrap(self) -> None:

        print(f'Adding cloud {_CLOUD_NAME} to {self}.')
        await self.add_cloud()

        INSTALL_SCRIPT_FILE_NAME = 'bootstrap-controller.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            INSTALL_SCRIPT_FILE_NAME
        )
        virtualMachine = self.jujuClient.virtualMachine
        environment = {
            'ROOT_PASSWORD': virtualMachine.secret.value,
            'CLOUD_NAME': _CLOUD_NAME,
            'MACHINE_FQDN': f'{self.name}.{_TLD}',
        }
        output, errorOutput = await virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{INSTALL_SCRIPT_FILE_NAME}',
            environment=environment,
        )

    @async_retry(exceptions=[RuntimeError])
    async def add_cloud(self) -> None:
        INSTALL_SCRIPT_FILE_NAME = 'add-cloud.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            INSTALL_SCRIPT_FILE_NAME
        )
        virtualMachine = self.jujuClient.virtualMachine
        environment = {
            'ROOT_PASSWORD': virtualMachine.secret.value,
            'CLOUD_NAME': _CLOUD_NAME,
            'CLOUDS_DEFINITION_FILE_PATH': self._generate_cloud_definition_file(),
            'CLOUDS_CREDENTIALS_FILE_PATH': await self._generate_cloud_credentials_file(),
        }
        output, errorOutput = await virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{INSTALL_SCRIPT_FILE_NAME}',
            environment=environment,
        )

    def _generate_cloud_definition_file(self) -> str:
        # TODO: Validate schema
        cloudDefinition = {
            'clouds': {
                _CLOUD_NAME: {  # TODO: Add parameter in MaasServer for cloud name
                    'type': 'maas',
                    'auth-types': ['oauth1'],
                    'endpoint': self.maasServer.apiUrl,
                }
            }
        }
        fileName = 'cloud_' + ''.join(random.choices(ascii_letters + digits, k=10)) + '.yaml'
        remoteFilePath = os.path.join('/tmp', fileName)
        with TemporaryDirectory() as temporaryFolderPath:
            localFilePath = os.path.join(temporaryFolderPath, fileName)
            with open(localFilePath, 'wt') as file:
                yaml = YAML()
                yaml.dump(cloudDefinition, file)
            with self.jujuClient.virtualMachine.sftp() as sftp:
                sftp.put(
                    localpath=localFilePath,
                    remotepath=remoteFilePath,
                )
        return remoteFilePath

    async def _generate_cloud_credentials_file(self) -> str:
        # TODO: Validate schema
        cloudCredentials = {
            'credentials': {
                _CLOUD_NAME: {  # TODO: Add parameter in MaasServer for cloud name
                    self.maasServer.adminUserName: {
                        'auth-type': 'oauth1',
                        'maas-oauth': await self.maasServer.apiKey,
                    }
                }
            }
        }
        fileName = 'cloud_cred_' + ''.join(random.choices(ascii_letters + digits, k=10)) + '.yaml'
        remoteFilePath = os.path.join('/tmp', fileName)
        with TemporaryDirectory() as temporaryFolderPath:
            localFilePath = os.path.join(temporaryFolderPath, fileName)
            with open(localFilePath, 'wt') as file:
                yaml = YAML()
                yaml.dump(cloudCredentials, file)
            with self.jujuClient.virtualMachine.sftp() as sftp:
                sftp.put(
                    localpath=localFilePath,
                    remotepath=remoteFilePath,
                )
        return remoteFilePath

    async def destroy(self) -> None:
        INSTALL_SCRIPT_FILE_NAME = 'destroy-controllers-and-models.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            INSTALL_SCRIPT_FILE_NAME
        )
        virtualMachine = self.jujuClient.virtualMachine
        environment = {
            'CLOUD_NAME': _CLOUD_NAME,
        }
        output, errorOutput = await virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{INSTALL_SCRIPT_FILE_NAME}',
            environment=environment,
        )

        while await self.bootstrapped:
            print(f'Waiting for {self} to be destroyed.')
            await asyncio.sleep(1)

    @property
    def maasServer(self) -> MaasServer:
        assert isinstance(self.jujuControllers.maasServer, MaasServer)
        return self.jujuControllers.maasServer

    @property
    def jujuClient(self) -> JujuClient:
        assert len(self.jujuControllers.jujuClients) == 1
        jujuClient = next(iter(self.jujuControllers.jujuClients.values()))
        assert isinstance(jujuClient, JujuClient)
        return jujuClient

    @property
    def jujuControllers(self) -> jujuControllers:
        assert isinstance(self.resources, JujuControllers)
        return self.resources

    def __str__(self):
        return f'Juju controller {self.name}'

