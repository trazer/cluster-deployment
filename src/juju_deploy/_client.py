from __future__ import annotations

import asyncio
import functools
import os
from typing import List

from ruamel.yaml import YAML

from clusterutils import (
    YamlSchemaValidator,
    async_retry,
)
from clusterutils.resources import (
    Resource,
    ResourceKeyValidator,
    Resources,
)
from maas_deploy import (
    MaasServer,
    load_maas_server,
)
from maas_deploy._utils import PackageInfo
from proxmox import (
    Hypervisors,
    VirtualMachine,
    load_virtual_machines_from_config,
)

_CURRENT_PATH = os.path.split(__file__)[0]
_SCHEMA_FOLDER_NAME = '_schemas'
_SCRIPTS_FOLDER_NAME = '_scripts'


def install_juju_clients(
    virtualMachinesFilePaths: List[str],
    maasControllersFilePaths: List[str],
    maasNetworksFilePaths: List[str],
    jujuControllersFilePaths: List[str],
    forceReinstall: bool,
) -> None:

    hypervisors = Hypervisors()
    load_virtual_machines_from_config(
        virtualMachinesFilePaths,
        hypervisors,
    )

    maasServer = MaasServer(hypervisors)
    load_maas_server(
        maasServer,
        maasControllersFilePaths,
        maasNetworksFilePaths,
    )

    jujuClients = JujuClients(hypervisors, maasServer)
    _load_juju_clients(
        jujuClients,
        jujuControllersFilePaths,
    )

    asyncio.run(
        _install_juju_clients(jujuClients, forceReinstall)
    )


def _load_juju_clients(
    jujuClients: JujuClients,
    jujuControllersFilePaths: List[str],
) -> None:

    assert len(jujuControllersFilePaths) == 1

    _validate_schema(
        jujuControllersFilePaths[0],
        schemaFileName='juju_controllers_schema.yaml',
    )

    config = {}
    yaml = YAML(typ='safe')
    with open(jujuControllersFilePaths[0], 'rt') as file:
        config.update(yaml.load(file))

    if len(config['juju_clients']) > 1:
        raise NotImplementedError()

    for jujuClientConfig in config['juju_clients']['virtual_machines']:
        jujuClient = JujuClient(jujuClients)
        jujuClient.from_config(jujuClientConfig)
        jujuClients[jujuClient.name] = jujuClient


def _validate_schema(filePath: str, schemaFileName: str) -> None:
    schemaFilePath = os.path.join(
        _CURRENT_PATH,
        _SCHEMA_FOLDER_NAME,
        schemaFileName,
    )
    yamlSchemaValidator = YamlSchemaValidator(
        source_file=filePath,
        schema_files=[schemaFilePath]
    )
    yamlSchemaValidator.validate(raise_exception=True)


class JujuClients(Resources):

    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='values()',
            attributeNames=('name', ),
        ),
    ]

    KEY_NAME = ('name', )

    def __init__(self, hypervisors: Hypervisors, maasServer: MaasServer):
        super().__init__(parentResource=None)
        self.hypervisors = hypervisors
        self.maasServer = maasServer

    def __str__(self):
        return f'Juju clients'


class JujuClient(Resource):

    def __init__(self, jujuClients: JujuClients):
        super().__init__(
            parentResource=None,
            resources=jujuClients,
        )
        self.name: str = None
        self._packageInfo: PackageInfo = None

    def from_config(self, config: dict) -> None:
        self.name = config['name']
        try:
            _ = self.hypervisors.get_virtual_machine_by_name(self.name)
        except ValueError as exception:
            raise ValueError(f'Invalid virtual machine {self.name}'
                             f'for {self}.') from exception

    @property
    async def installed(self) -> bool:
        return await self.packageInfo.installed

    @async_retry(exceptions=[RuntimeError])
    async def install(self) -> None:
        INSTALL_SCRIPT_FILE_NAME = 'install-juju-client.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            INSTALL_SCRIPT_FILE_NAME
        )
        environment = {
            'ROOT_PASSWORD': self.virtualMachine.secret.value,
            'PACKAGE_REPOSITORY': self.packageInfo.packageRepository,
            'PACKAGE_NAME': self.packageInfo.packageName,
        }
        output, errorOutput = await self.virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{INSTALL_SCRIPT_FILE_NAME}',
            environment=environment,
        )

        print(f'Installing charm snap on {self.virtualMachine}.')
        await self._install_charm_snap()

    @async_retry(exceptions=[RuntimeError])
    async def _install_charm_snap(self) -> None:
        INSTALL_SCRIPT_FILE_NAME = 'install-charm-snap.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            INSTALL_SCRIPT_FILE_NAME
        )
        environment = {
            'ROOT_PASSWORD': self.virtualMachine.secret.value
        }
        output, errorOutput = await self.virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{INSTALL_SCRIPT_FILE_NAME}',
            environment=environment,
        )

    async def uninstall(self) -> None:
        INSTALL_SCRIPT_FILE_NAME = 'uninstall-juju-client.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            INSTALL_SCRIPT_FILE_NAME
        )
        environment = {
            'ROOT_PASSWORD': self.virtualMachine.secret.value,
            'PACKAGE_NAME': self.packageInfo.packageName,
        }
        output, errorOutput = await self.virtualMachine.execute_local_script_on_remote_async(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{INSTALL_SCRIPT_FILE_NAME}',
            environment=environment,
        )

    @property
    def packageInfo(self) -> PackageInfo:
        if self._packageInfo is None:
            self._packageInfo = PackageInfo(
                self.virtualMachine,
                packageName='juju',
                packageRepository='ppa:juju/stable',
            )
        return self._packageInfo

    @property
    def virtualMachine(self) -> VirtualMachine:
        return self.hypervisors.get_virtual_machine_by_name(self.name)

    @property
    def hypervisors(self) -> Hypervisors:
        assert isinstance(self.jujuClients.hypervisors, Hypervisors)
        return self.jujuClients.hypervisors

    @property
    def jujuClients(self) -> JujuClients:
        assert isinstance(self.resources, JujuClients)
        return self.resources

    def __str__(self):
        return f'Juju client {self.name}'


async def _install_juju_clients(
        jujuClients: JujuClients,
        forceReinstall: bool,
) -> None:
    await asyncio.gather(*list(map(
        functools.partial(_install_juju_client, forceReinstall),
        jujuClients.values(),
    )))


async def _install_juju_client(
        forceReinstall: bool,
        jujuClient: JujuClient,
) -> None:

    if jujuClient.virtualMachine.off:
        print(f'Starting {jujuClient.virtualMachine}.')
        await jujuClient.virtualMachine.start_async(wait=True)

    print(f'Checking if {jujuClient} is already installed.')
    if await jujuClient.installed \
            and forceReinstall:
        print(f'Uninstalling {jujuClient} first.')
        await jujuClient.uninstall()
    elif await jujuClient.installed \
            and not forceReinstall:
        raise NotImplementedError()
    else:
        print(f'{jujuClient} is not already installed.')

    print(f'Installing {jujuClient}.')
    await jujuClient.install()
