import os


def delete_secrets():
    from . import _keyring
    filePath = _keyring.file_path
    os.remove(filePath)
    del _keyring
    assert not os.path.isfile(filePath)
