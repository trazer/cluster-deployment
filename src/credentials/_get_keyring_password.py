import sys


def get_keyring_password() -> None:
    from . import _get_keyring_password
    password = _get_keyring_password()
    sys.stdout.write(password)
