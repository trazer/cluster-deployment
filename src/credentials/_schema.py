import os

from pykwalify.core import Core as YamlSchemaValidator

_CURRENT_PATH = os.path.split(__file__)[0]


def validate_schema(secretsFilePath):
    yamlSchemaValidator = YamlSchemaValidator(
        source_file=secretsFilePath,
        schema_files=[os.path.join(_CURRENT_PATH, 'secrets_schema.yaml')]
    )
    yamlSchemaValidator.validate(raise_exception=True)
