import getpass
import os

from keyrings.cryptfile.cryptfile import CryptFileKeyring

from ._delete import delete_secrets
from ._get_keyring_password import get_keyring_password
from ._register import register_secrets
from ._secret import Secret

_keyringInitialized = False

_keyring = None

_KEYRING_PASSWORD_ENVIRONMENT_VARIABLE_NAME = 'KEYRING_PASSWORD'


def _init_keyring():

    DUMMY_SERVICE_NAME = '__dummy-service-name__'
    DUMMY_USER_NAME = '__dummy-user-name__'
    DUMMY_PASSWORD = '123456'

    keyring_ = CryptFileKeyring()
    if _KEYRING_PASSWORD_ENVIRONMENT_VARIABLE_NAME in os.environ:
        keyring_.keyring_key = os.environ[_KEYRING_PASSWORD_ENVIRONMENT_VARIABLE_NAME]
    else:
        keyring_.keyring_key = _get_keyring_password()

    try:
        dummyPassword = keyring_.get_password(DUMMY_SERVICE_NAME, DUMMY_USER_NAME)
    except ValueError:  # MAC check failed
        print('Wrong keyring password!')
        os.remove(keyring_.file_path)
        del keyring_
        return _init_keyring()

    if dummyPassword is not None:
        assert dummyPassword == DUMMY_PASSWORD
        return keyring_
    else:
        try:
            os.remove(keyring_.file_path)
        except FileNotFoundError:
            pass
        keyring_.set_password(DUMMY_SERVICE_NAME, DUMMY_USER_NAME, DUMMY_PASSWORD)
        return keyring_


def _get_keyring_password():
    while True:
        PROMPT_TEMPLATE = '{} secret for {}: '
        password1 = getpass.getpass(prompt=PROMPT_TEMPLATE.format('Enter', 'encrypted keyring file'))
        password2 = getpass.getpass(prompt=PROMPT_TEMPLATE.format('Re-enter', 'encrypted keyring file'))
        if password1 != password2:
            print('Password mismatch!')
        elif password1.strip() == password2.strip() == '':
            print('Password is empty!')
        else:
            return password1
