from ruamel.yaml import YAML

from ._schema import validate_schema
from ._secret import Secret


def register_secrets(secretsFilePaths, validate, force):

    for secretsFilePath in secretsFilePaths:
        validate_schema(secretsFilePath)

    importedSecrets = []
    for secretsFilePath in secretsFilePaths:
        yaml = YAML(typ='safe')
        with open(secretsFilePath, 'rt') as file:
            importedSecrets += yaml.load(file)

    _check_for_duplicated_secrets(importedSecrets)

    for importedSecret in importedSecrets:

        secret = Secret(importedSecret['service_name'], importedSecret['user_name'])

        if not secret.exists:
            secret.set()
        elif secret.exists and force:
            secret.delete()
            secret.set()

        if validate:
            while True:
                if not _is_valid(secret):
                    print('Validation failed for secret of {}.'.format(secret))
                    secret.delete()
                    secret.set()
                else:
                    break
            raise NotImplementedError()


def _check_for_duplicated_secrets(importedSecrets):
    secretKeys = []
    for importedSecret in importedSecrets:
        secretKey = (importedSecret['service_name'], importedSecret['user_name'])
        if secretKey in secretKeys:
            raise ValueError('Duplicated secret ({}, {})!'.format(*secretKey))
        else:
            secretKeys.append(secretKey)


def _is_valid(secret):
    return True
