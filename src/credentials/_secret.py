import getpass

from clusterutils import retry


def _initialize_keyring(func):
    def _wrapper(self):
        import credentials
        if not credentials._keyringInitialized:
            assert credentials._keyring is None
            credentials._keyring = credentials._init_keyring()
            credentials._keyringInitialized = True
        else:
            assert credentials._keyring is not None
        return func(self)
    return _wrapper


class Secret:

    def __init__(self, serviceName, userName):
        self.serviceName = serviceName
        self.userName = userName

    @_initialize_keyring
    def set(self):
        from . import _keyring

        if self.exists:
            raise SecretAlreadyExists()

        while True:
            PROMPT_TEMPLATE = '{} secret for {}: '
            password1 = getpass.getpass(prompt=PROMPT_TEMPLATE.format('Enter', self))
            password2 = getpass.getpass(prompt=PROMPT_TEMPLATE.format('Re-enter', self))
            if password1 != password2:
                print('Password mismatch for {}!'.format(self))
            elif password1.strip() == password2.strip() == '':
                print('Password is empty for {}!'.format(self))
            else:
                break

        _keyring.set_password(self.serviceName, self.userName, password1)

    @property
    @_initialize_keyring
    @retry(exceptions=[ValueError])
    def value(self):
        from . import _keyring
        if not self.exists:
            raise SecretNotFound(self)
        return _keyring.get_password(self.serviceName, self.userName)

    @_initialize_keyring
    def delete(self):
        from . import _keyring
        if not self.exists:
            raise SecretNotFound()
        _keyring.delete_password(self.serviceName, self.userName)

    @property
    @_initialize_keyring
    def exists(self):
        from . import _keyring
        return _keyring.get_password(self.serviceName, self.userName) is not None

    @property
    @_initialize_keyring
    def key(self):
        return self.serviceName, self.userName

    def __str__(self):
        return 'user "{}" in service "{}"'.format(self.userName, self.serviceName)


class SecretNotFound(Exception):
    pass


class SecretAlreadyExists(Exception):
    pass
