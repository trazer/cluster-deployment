import asyncio
import os
from typing import List

from ruamel.yaml import YAML

from clusterutils import YamlSchemaValidator
from maas_deploy._commission import _load_machines
from maas_deploy._maas_server import (
    MaasServer,
    load_maas_server,
)
from maas_deploy._machines import (
    Machines,
)
from proxmox import (
    Hypervisors,
    load_virtual_machines_from_config,
)
from ._runners import (
    Runner,
    Runners,
)

_CURRENT_PATH = os.path.split(__file__)[0]
_SCHEMA_FOLDER_NAME = '_schemas'


def register_gitlab_runners(
        virtualMachinesFilePaths: List[str],
        maasControllersFilePaths: List[str],
        maasNetworksFilePaths: List[str],
        maasMachinesFilePaths: List[str],
        runnersFilePaths: List[str],
) -> None:

    hypervisors = Hypervisors()
    load_virtual_machines_from_config(
        virtualMachinesFilePaths,
        hypervisors,
    )

    maasServer = MaasServer(hypervisors)
    load_maas_server(
        maasServer,
        maasControllersFilePaths,
        maasNetworksFilePaths,
    )

    machines = Machines(maasServer)
    _load_machines(
        machines,
        maasMachinesFilePaths,
    )

    runners = Runners(
        maasServer,
        machines,
    )
    _load_runners(
        runners,
        runnersFilePaths,
    )

    asyncio.run(_register_gitlab_runners(
        runners,
    ))


# ------------------------------------------------


def _load_runners(
        runners: Runners,
        runnersFilePaths: List[str],
) -> None:

    for filePath in runnersFilePaths:
        _validate_runner_schema(
            filePath,
            schemaFileName='gitlab_runners_schema.yaml',
        )

    config = {}
    assert len(runnersFilePaths) == 1
    yaml = YAML(typ='safe')
    with open(runnersFilePaths[0], 'rt') as file:
        config.update(yaml.load(file))

    runners.from_config(config)


# TODO: Make one function for all cluster-deployment project
def _validate_runner_schema(filePath: str, schemaFileName: str) -> None:
    schemaFilePath = os.path.join(
        _CURRENT_PATH,
        _SCHEMA_FOLDER_NAME,
        schemaFileName,
    )
    yamlSchemaValidator = YamlSchemaValidator(
        source_file=filePath,
        schema_files=[schemaFilePath]
    )
    yamlSchemaValidator.validate(raise_exception=True)


# ------------------------------------------------


async def _register_gitlab_runners(
        runners: Runners,
) -> None:
    await asyncio.gather(*list(map(
        _register_gitlab_runner,
        runners.values(),
    )))


async def _register_gitlab_runner(
    runner: Runner,
) -> None:

    print(f'Checking if {runner.machine} is already deployed.')
    if await runner.machine.deployed:
        raise MachineAlreadyDeployed(f'{runner.machine}')

    print(f'Deploying {runner.machine}.')
    await runner.machine.deploy()

    while not await runner.machine.deployed:
        print(f'Waiting for {runner.machine} to be deployed.')
        await asyncio.sleep(2)

    print(f'Registering {runner}.')
    await runner.register()


class MachineAlreadyDeployed(Exception):
    pass
