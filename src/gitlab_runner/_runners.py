import os

from clusterutils.resources import (
    Resource,
    ResourceKeyValidator,
    Resources,
)
from credentials import Secret
from maas_deploy import MaasServer
from maas_deploy._machines import (
    Machine,
    Machines,
)

_CURRENT_PATH = os.path.split(__file__)[0]
_SCRIPTS_FOLDER_NAME = '_scripts'


class Runners(Resources):

    RESOURCE_KEY_VALIDATORS = [
        ResourceKeyValidator(
            resourceIterableTree='values()',
            attributeNames=('name', ),
        ),
    ]

    KEY_NAME = ('name', )

    def __init__(
            self,
            maasServer: MaasServer,
            machines: Machines,
    ):
        super().__init__(parentResource=None)
        self.maasServer = maasServer
        self.machines = machines

    def from_config(
            self,
            runnersConfig: dict,
    ) -> None:
        for runnerConfig in runnersConfig['machines']:
            runner = Runner(
                runners=self,
            )
            runner.from_config(runnerConfig)
            self[runner.name] = runner

    def __str__(self):
        return f'runners'


class Runner(Resource):

    def __init__(self, runners: Runners):
        super().__init__(
            parentResource=None,
            resources=runners,
        )
        self.name: str = None
        self.gitlabUrl: str = None
        self.gitlabRegistrationTokenSecret: Secret = None

    def from_config(
            self,
            runnerConfig: dict,
    ) -> None:
        self.name = runnerConfig['name']
        self.gitlabUrl = runnerConfig['gitlab_url']
        self.gitlabRegistrationTokenSecret = Secret(
            runnerConfig['gitlab_registration_token_secret']['service_name'],
            runnerConfig['gitlab_registration_token_secret']['user_name']
        )

    async def register(self) -> None:
        INSTALL_SCRIPT_FILE_NAME = 'setup-gitlab-runner.sh'
        localFilePath = os.path.join(
            _CURRENT_PATH,
            _SCRIPTS_FOLDER_NAME,
            INSTALL_SCRIPT_FILE_NAME
        )
        environment = {
            'URL': self.gitlabUrl,
            'REGISTRATION_TOKEN': self.gitlabRegistrationTokenSecret.value,
        }
        output, errorOutput = await self.machine.execute_local_script_on_remote(
            localFilePath=localFilePath,
            remoteFilePath=f'/tmp/{INSTALL_SCRIPT_FILE_NAME}',
            environment=environment,
        )

    @property
    def machine(self) -> Machine:
        assert isinstance(self.runners.machines, Machines)
        machines = self.runners.machines
        return machines[self.name]

    @property
    def runners(self) -> Runners:
        assert isinstance(self.resources, Runners)
        return self.resources

    def __str__(self):
        return f'{self.name}'
