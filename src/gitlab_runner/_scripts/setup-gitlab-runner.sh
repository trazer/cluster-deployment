#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

sudo chmod +x /usr/local/bin/gitlab-runner

curl -sSL https://get.docker.com/ | sh

sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

sudo usermod -aG docker gitlab-runner

sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

sudo gitlab-runner start

sudo gitlab-runner register -n \
   --url ${URL} \
   --registration-token ${REGISTRATION_TOKEN} \
   --executor shell

