#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"

VIRTUAL_MACHINES_FILE_PATH=../clusterconfig/virtual_machines.yaml
MAAS_CONTROLLERS_FILE_PATH=../clusterconfig/maas_controllers.yaml
MAAS_NETWORKS_FILE_PATH=../clusterconfig/maas_network.yaml
JUJU_CONTROLLERS_FILE_PATH=../clusterconfig/juju_controllers.yaml

source ~/.virtualenvs/cluster-deployment/bin/activate

export KEYRING_PASSWORD=$(cluster get-keyring-password)

#cluster juju-clients install --force-reinstall \
#    "${VIRTUAL_MACHINES_FILE_PATH}" \
#    "${MAAS_CONTROLLERS_FILE_PATH}" \
#    "${MAAS_NETWORKS_FILE_PATH}" \
#    "${JUJU_CONTROLLERS_FILE_PATH}"

cluster juju-controllers bootstrap \
    "${VIRTUAL_MACHINES_FILE_PATH}" \
    "${MAAS_CONTROLLERS_FILE_PATH}" \
    "${MAAS_NETWORKS_FILE_PATH}" \
    "${JUJU_CONTROLLERS_FILE_PATH}"

#cluster juju-controllers destroy \
#    "${VIRTUAL_MACHINES_FILE_PATH}" \
#    "${MAAS_CONTROLLERS_FILE_PATH}" \
#    "${MAAS_NETWORKS_FILE_PATH}" \
#    "${JUJU_CONTROLLERS_FILE_PATH}"
