#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"

VIRTUAL_MACHINES_FILE_PATH=../clusterconfig/virtual_machines.yaml
MAAS_CONTROLLERS_FILE_PATH=../clusterconfig/maas_controllers.yaml
MAAS_NETWORKS_FILE_PATH=../clusterconfig/maas_network.yaml
MAAS_MACHINES_FILE_PATH=../clusterconfig/machines.yaml
JUJU_CONTROLLERS_FILE_PATH=../clusterconfig/juju_controllers.yaml

source ~/.virtualenvs/cluster-deployment/bin/activate

export KEYRING_PASSWORD=$(cluster get-keyring-password)

cluster virtual-machines create --stop-if-on "${VIRTUAL_MACHINES_FILE_PATH}"

cluster maas-controllers install \
    "${VIRTUAL_MACHINES_FILE_PATH}" \
    "${MAAS_CONTROLLERS_FILE_PATH}"

cluster maas-configure \
    "${VIRTUAL_MACHINES_FILE_PATH}" \
    "${MAAS_CONTROLLERS_FILE_PATH}" \
    "${MAAS_NETWORKS_FILE_PATH}"

cluster maas-commission \
    --no-deployment-testing \
    "${VIRTUAL_MACHINES_FILE_PATH}" \
    "${MAAS_CONTROLLERS_FILE_PATH}" \
    "${MAAS_NETWORKS_FILE_PATH}" \
    "${MAAS_MACHINES_FILE_PATH}"

cluster juju-clients install --force-reinstall \
    "${VIRTUAL_MACHINES_FILE_PATH}" \
    "${MAAS_CONTROLLERS_FILE_PATH}" \
    "${MAAS_NETWORKS_FILE_PATH}" \
    "${JUJU_CONTROLLERS_FILE_PATH}"

cluster juju-controllers bootstrap \
    "${VIRTUAL_MACHINES_FILE_PATH}" \
    "${MAAS_CONTROLLERS_FILE_PATH}" \
    "${MAAS_NETWORKS_FILE_PATH}" \
    "${JUJU_CONTROLLERS_FILE_PATH}"
