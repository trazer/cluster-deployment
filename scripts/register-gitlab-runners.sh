#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"

VIRTUAL_MACHINES_FILE_PATH=../clusterconfig/virtual_machines.yaml
MAAS_CONTROLLERS_FILE_PATH=../clusterconfig/maas_controllers.yaml
MAAS_NETWORKS_FILE_PATH=../clusterconfig/maas_network.yaml
MAAS_MACHINES_FILE_PATH=../clusterconfig/machines.yaml
GITLAB_RUNNERS_FILE_PATH=../clusterconfig/gitlab_runners.yaml

cluster gitlab-runners register \
    "${VIRTUAL_MACHINES_FILE_PATH}" \
    "${MAAS_CONTROLLERS_FILE_PATH}" \
    "${MAAS_NETWORKS_FILE_PATH}" \
    "${MAAS_MACHINES_FILE_PATH}" \
    "${GITLAB_RUNNERS_FILE_PATH}"
