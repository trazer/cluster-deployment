#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"

source ~/.virtualenvs/cluster-deployment/bin/activate

pip install -e ..
