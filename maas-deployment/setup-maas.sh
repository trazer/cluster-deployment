#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
#set -o xtrace

export PROFILE=root
export EMAIL_ADDRESS=maxenceboutet@outlook.com
export API_KEY_FILE=~/.maas_api_key
export API_SERVER=10.20.0.25:5240
export MAAS_URL=http://${API_SERVER}/MAAS/api/2.0
export MAAS_CONTROLLER_HOSTNAME="maas-controller-testing"
export UPSTREAM_DNS=192.168.1.1
export SLEEP_AFTER_COMMAND=0
export MACHINES_FILE_PATH=./machines.json
export SUBNETS_FILE_PATH=./subnets.json
#export HARDWARE_TESTING_SCRIPTS="internet-connectivity,stress-ng-cpu-short,stress-ng-memory-short"
export HARDWARE_TESTING_SCRIPTS="internet-connectivity"

source ../ask-sudo-password.sh
export ROOT_PASSWORD=$(ask_for_password_until_correct root | tail -1)
export MAAS_PASSWORD=$(get_password MAAS | tail -1)
export IDRAC_PASSWORD=$(get_password iDRAC | tail -1)  # TODO: Test password validity
export AMT_PASSWORD=$(get_password AMT | tail -1)  # TODO: Test password validity

# -----------------------------------------------

function main() {

    wait_for_dpkg_to_become_available

    update_system

    add_package_repositories "http://mirrors.kernel.org/ubuntu"  # required for additional packages
    install_package wsmancli
    install_package amtterm
    install_package jq

    enable_ip_forwarding

    install_maas

    init_maas

    login_to_maas

    echo "MAAS controller system_id is $(get_maas_controller_system_id)"

    set_up_upstream_dns

    import_public_ssh_key

    select_boot_resources

    read -t 120 -p "Press any key to continue (will continue in 120s)... " -n 1 -s || true && echo

    configure_subnets

    configure_controller_interfaces

    delete_unused_fabrics

    wait_for_boot_resources_to_be_imported_and_synced

    start_machines

    wait_for_machines_to_be_discovered

    set_machines_hostname

    configure_machines_power_parameters

    commission_machines

    wait_for_machines_to_be_commissioned

    configure_machines_storage

}

# -----------------------------------------------

function wait_for_dpkg_to_become_available() {
    sleep 5
    while true
    do
        if dpkg_available; then
            echo "dpkg is available!"
            sleep 1
            break
        else
            echo "Waiting for dpkg to become available..."
            sleep 5
        fi
    done
}

function dpkg_available() {
    local __dpkg_lock=$(echo "${ROOT_PASSWORD}" | sudo -S lsof /var/lib/dpkg/lock)
    local __dpkg_frontend_lock=$(echo "${ROOT_PASSWORD}" | sudo -S lsof /var/lib/dpkg/lock-frontend)
    if echo "${__dpkg_lock}" | grep -Fq "/var/lib/dpkg/lock"; then
        return 1
    elif echo "${__dpkg_frontend_lock}" | grep -Fq "/var/lib/dpkg/lock-frontend"; then
        return 1
    else
        return 0
    fi
}

function update_system() {
    echo "${ROOT_PASSWORD}" | sudo -S apt update
    echo "${ROOT_PASSWORD}" | sudo -S apt full-upgrade -y
}

function add_package_repositories() {
    echo "deb $1 bionic main universe"
    if grep -Fxq "deb $1 bionic main universe" /etc/apt/sources.list; then
        echo
    else
        echo "${ROOT_PASSWORD}" | sudo -S sed -i "$ a deb $1 bionic main universe" /etc/apt/sources.list
        echo "${ROOT_PASSWORD}" | sudo -S apt update
    fi
}

function install_package() {
    echo "${ROOT_PASSWORD}" | sudo -S apt install -y $1
}

function enable_ip_forwarding() {
    echo "Enabling IPv4 forwarding..."

    echo "${ROOT_PASSWORD}" | sudo -S sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf

    # Reload sysctl settings
    echo "${ROOT_PASSWORD}" | sudo -S sysctl -p

    if [[ "$(sysctl net.ipv4.ip_forward)" != "net.ipv4.ip_forward = 1" ]]; then
        echo "Failed to enable IPv4 forwarding."
        exit 1
    fi
}

function install_maas() {
    echo "Installing MAAS..."
    echo "${ROOT_PASSWORD}" | sudo -S apt install -y maas
}

function init_maas() {
    echo "Creating admin user \"${PROFILE}\" (${EMAIL_ADDRESS})..."
    local __command=$(echo "${ROOT_PASSWORD}" \
        | sudo -S maas createadmin \
        --username="${PROFILE}" \
        --email=${EMAIL_ADDRESS} \
        --password=${MAAS_PASSWORD})
    sleep "${SLEEP_AFTER_COMMAND}"

    echo "Storing API key for ${PROFILE} in ${API_KEY_FILE}..."
    local __command=$(echo "${ROOT_PASSWORD}" | \
        sudo -S maas-region apikey --username="${PROFILE}" \
        > "${API_KEY_FILE}")
    sleep "${SLEEP_AFTER_COMMAND}"
}

function login_to_maas() {
    echo "Logging in as ${PROFILE} to ${MAAS_URL}..."
    local __command=$(maas login "${PROFILE}" "${MAAS_URL}" - < "${API_KEY_FILE}")
    sleep "${SLEEP_AFTER_COMMAND}"
}

function set_up_upstream_dns() {
    echo "Setting up ${UPSTREAM_DNS} as DNS forwarder..."
    local __command=$(maas "${PROFILE}" maas set-config name=upstream_dns value="${UPSTREAM_DNS}")
    sleep "${SLEEP_AFTER_COMMAND}"
}

function import_public_ssh_key() {
    echo "Adding public SSH key..."
    local __ssh_key=$(cat ~/.ssh/id_rsa.pub)
    local __command=$(maas "${PROFILE}" sshkeys create "key=${__ssh_key}")
    sleep "${SLEEP_AFTER_COMMAND}"
}

function select_boot_resources() {
    echo "Selecting boot resources..."
    local __command=$(maas "${PROFILE}" boot-source-selections create 1 \
        os="ubuntu" \
        release="bionic" \
        arches="amd64" \
        subarches="*" \
        labels="*")
    sleep "${SLEEP_AFTER_COMMAND}"

    echo "Starting import of boot resources..."
    local __command=$(maas "${PROFILE}" boot-resources import)
    sleep "${SLEEP_AFTER_COMMAND}"
}

function configure_subnets() {

    # TODO: Verify that subnets from config exist

    local __subnet_cidr=""

    for __subnet_cidr in $(get_subnets_cidr_from_config); do

        local __fabric_name_from_config=$(subnet_cidr_2_fabric_name_from_config "${__subnet_cidr}")
        local __vid_from_config=$(subnet_cidr_2_vid_from_config "${__subnet_cidr}")
        local __dns_severs_from_config=$(subnet_cidr_2_dns_servers_from_config "${__subnet_cidr}")
        local __gateway_from_config=$(subnet_cidr_2_gateway_from_config "${__subnet_cidr}")
        local __managed_from_config=$(subnet_cidr_2_managed_from_config "${__subnet_cidr}")
        local __dhcp_from_config=$(subnet_cidr_2_dhcp_from_config "${__subnet_cidr}")
        local __start_ip_address_from_config=$(subnet_cidr_2_start_ip_address_from_config "${__subnet_cidr}")
        local __end_ip_address_from_config=$(subnet_cidr_2_end_ip_address_from_config "${__subnet_cidr}")

        if [[ "$(vid_exists_in_fabric "${__fabric_name_from_config}" "${__vid_from_config}")" = "false" ]]; then
            create_vid_in_fabric "${__fabric_name_from_config}" "${__vid_from_config}"
        fi

        if [[ "$(subnet_cidr_2_subnet_fabric_name "${__subnet_cidr}")" != "${__fabric_name_from_config}" ]]; then
            move_subnet_to_fabric "${__subnet_cidr}" "${__fabric_name_from_config}" "${__vid_from_config}"
        fi

        if [[ "$(subnet_cidr_2_subnet_vlan_vid "${__subnet_cidr}")" != "${__vid_from_config}" ]]; then
            change_vlan_vid_of_subnet "${__subnet_cidr}" "${__vid_from_config}"
        fi

        local __subnet_managed=$(subnet_cidr_2_subnet_managed "${__subnet_cidr}")
        if [[ "${__subnet_managed}" = "true" && "${__managed_from_config}" = "false" ]]; then
            set_subnet_to_unmanaged "${__subnet_cidr}"
        elif [[ "${__subnet_managed}" = "false" && "${__managed_from_config}" = "true" ]]; then
            set_subnet_to_managed "${__subnet_cidr}"
        fi

        delete_existing_ip_ranges_on_subnet "${__subnet_cidr}"

        local __subnet_dhcp_on=$(subnet_cidr_2_subnet_dhcp_on "${__subnet_cidr}")
        if [[ "${__subnet_dhcp_on}" = "true" && "${__dhcp_from_config}" = "false" ]]; then
            disable_dhcp_on_subnet "${__subnet_cidr}"
        elif [[ "${__subnet_dhcp_on}" = "false" && "${__dhcp_from_config}" = "true" ]]; then
            set_dynamic_ip_range_on_subnet "${__subnet_cidr}" \
                "${__start_ip_address_from_config}" \
                "${__end_ip_address_from_config}"
            enable_dhcp_on_subnet "${__subnet_cidr}"
        fi

        if [[ "$(subnet_cidr_2_subnet_dns_servers "${__subnet_cidr}")" != "${__dns_severs_from_config}" \
                && "${__dns_severs_from_config}" != "" ]]; then
            set_dns_servers "${__subnet_cidr}" "${__dns_severs_from_config}"
        fi

        if [[ "$(subnet_cidr_2_subnet_gateway_ip "${__subnet_cidr}")" != "${__gateway_from_config}" \
                && "${__gateway_from_config}" != "" ]]; then
            set_gateway_ip "${__subnet_cidr}" "${__gateway_from_config}"
        fi

    done

}

function vid_exists_in_fabric() {
    declare __fabric_name=$1
    declare __vid=$2

    local __fabric_id=$(fabric_name_2_fabric_id "${__fabric_name}")

    local __vlans_in_fabric_info=$(maas "${PROFILE}" vlans read "${__fabric_id}")

    local __vlans_in_fabric_vid=$(echo "${__vlans_in_fabric_info}" \
        | jq --raw-output \
        '.[].vid')

    local __vlan_in_fabric_vid=""
    for __vlan_in_fabric_vid in ${__vlans_in_fabric_vid}; do
        if [[ "${__vlan_in_fabric_vid}" = "${__vid}" ]]; then
            echo "true"
            return 0
        fi
    done

    echo "false"
}

function create_vid_in_fabric() {
    declare __fabric_name=$1
    declare __vid=$2

    echo "Creating VLAN with VID=${__vid} on ${__fabric_name}..."

    local __command=$(maas "${PROFILE}" vlans create \
        $(fabric_name_2_fabric_id "${__fabric_name}") \
        name="" \
        description="" \
        vid="${__vid}")

    echo "${__command}" >> ./debug.log

    sleep "${SLEEP_AFTER_COMMAND}"
}

function move_subnet_to_fabric() {
    declare __subnet_cidr=$1
    declare __fabric_name=$2
    declare __vid=$3

    echo "Moving subnet ${__subnet_cidr} to ${__fabric_name} with vid=${__vid}..."

    local __subnet_id=$(subnet_cidr_2_subnet_id "${__subnet_cidr}")
    local __vlan_id=$(fabric_name_and_vid_2_fabric_vlan_id "${__fabric_name}" "${__vid}")

    local __command=$(maas "${PROFILE}" subnet update "${__subnet_id}" vlan="${__vlan_id}")

    echo "${__command}" >> ./debug.log

    sleep "${SLEEP_AFTER_COMMAND}"
}

function change_vlan_vid_of_subnet() {
    declare __subnet_cidr=$1
    declare __vid=$2

    echo "Changing VID of subnet ${__subnet_cidr} to ${__vid}..."

    local __fabric_name=$(subnet_cidr_2_subnet_fabric_name "${__subnet_cidr}")
    local __vlan_id=$(fabric_name_and_vid_2_fabric_vlan_id "${__fabric_name}" "${__vid}")

    local __subnet_id=$(subnet_cidr_2_subnet_id "${__subnet_cidr}")

    local __command=$(maas "${PROFILE}" subnet update "${__subnet_id}" vlan="${__vlan_id}")

    sleep "${SLEEP_AFTER_COMMAND}"
}

function set_subnet_to_managed() {
    declare __subnet_cidr=$1
    echo "Setting subnet ${__subnet_cidr} to managed..."
    local __subnet_id=$(subnet_cidr_2_subnet_id "${__subnet_cidr}")
    local __command=$(maas "${PROFILE}" subnet update "${__subnet_id}" managed=True)
}

function set_subnet_to_unmanaged() {
    declare __subnet_cidr=$1
    echo "Setting subnet ${__subnet_cidr} to unmanaged..."
    local __subnet_id=$(subnet_cidr_2_subnet_id "${__subnet_cidr}")
    local __command=$(maas "${PROFILE}" subnet update "${__subnet_id}" managed=False)
}

function delete_existing_ip_ranges_on_subnet() {

    declare __subnet_cidr=$1

    local __ip_range_id=""

    for __ip_range_id in $(get_ip_ranges_id); do

        if [[ "$(ip_range_id_2_subnet_cidr "${__ip_range_id}")" != "${__subnet_cidr}" ]]; then
            continue
        fi

        echo "Deleting existing IP range with ID ${__ip_range_id} on ${__subnet_cidr}..."
        local __command=$(maas "${PROFILE}" iprange delete "${__ip_range_id}")

    done

}

function enable_dhcp_on_subnet() {
    declare __subnet_cidr=$1

    local __fabric_name=$(subnet_cidr_2_subnet_fabric_name "${__subnet_cidr}")
    local __fabric_id=$(fabric_name_2_fabric_id "${__fabric_name}")
    local __vlan_vid=$(subnet_cidr_2_subnet_vlan_vid "${__subnet_cidr}")

    echo "Providing DHCP on ${__subnet_cidr} (VLAN ${__vlan_vid} of ${__fabric_name})..."

    local __command=$(maas root vlan update "${__fabric_id}" "${__vlan_vid}" \
        primary_rack="$(get_maas_controller_system_id)" \
        dhcp_on=True)
}

function disable_dhcp_on_subnet() {
    declare __subnet_cidr=$1

    local __fabric_name=$(subnet_cidr_2_subnet_fabric_name "${__subnet_cidr}")
    local __fabric_id=$(fabric_name_2_fabric_id "${__fabric_name}")
    local __vlan_vid=$(subnet_cidr_2_subnet_vlan_vid "${__subnet_cidr}")

    echo "Disabling DHCP on ${__subnet_cidr} (VLAN ${__vlan_vid} of ${__fabric_name})..."

    local __command=$(maas root vlan update "${__fabric_id}" "${__vlan_vid}" \
        primary_rack="$(get_maas_controller_system_id)" \
        dhcp_on=False)
}

function set_dynamic_ip_range_on_subnet() {
    declare __subnet_cidr=$1
    declare __start_ip_address=$2
    declare __end_ip_address=$3
    echo "Creating dynamic IP range from ${__start_ip_address} to ${__end_ip_address} on ${__subnet_cidr}..."
    local __command=$(maas "${PROFILE}" ipranges create type=dynamic \
        start_ip="${__start_ip_address}" \
        end_ip="${__end_ip_address}" \
        subnet="$(subnet_cidr_2_subnet_id "${__subnet_cidr}")")
}

function set_dns_servers() {
    declare __subnet_cidr=$1
    declare __dns_servers=$2
    echo "Setting DNS servers ${__dns_servers} on subnet ${__subnet_cidr}..."
    local __command=$(maas "${PROFILE}" subnet update \
        "$(subnet_cidr_2_subnet_id "${__subnet_cidr}")" \
        dns_servers="${__dns_servers}")
}

function set_gateway_ip() {
    declare __subnet_cidr=$1
    declare __gateway_ip=$2
    echo "Setting gateway ${__gateway_ip} on subnet ${__subnet_cidr}..."
    local __command=$(maas "${PROFILE}" subnet update \
        "$(subnet_cidr_2_subnet_id "${__subnet_cidr}")" \
        gateway_ip="${__gateway_ip}")
}

# -----------------------------------------------

function configure_controller_interfaces() {

    # TODO: Verify that subnets from config exist in interfaces

    local __interface_id=""
    local __interface_id=""

    for __interface_id in $(get_interfaces_id "$(get_maas_controller_system_id)"); do

        local __controller_id=$(get_maas_controller_system_id)
        local __interface_fabric_name=$(interface_2_fabric_name "${__controller_id}" "${__interface_id}")
        local __interface_vid=$(interface_2_vid "${__controller_id}" "${__interface_id}")

        for __interface_link_id in $(get_interface_links_id "${__controller_id}" "${__interface_id}"); do

            local __interface_link_cidr=$(interface_link_2_subnet_cidr \
                "${__controller_id}" \
                "${__interface_id}" \
                "${__interface_link_id}")

            if [[ "$(subnet_cidr_exists_in_subnet_config "${__interface_link_cidr}")" = "false" ]]; then
                echo "Subnet ${__interface_link_cidr} does not exist in config!"
                exit 1
            fi

            local __subnet_fabric_name=$(subnet_cidr_2_fabric_name_from_config "${__interface_link_cidr}")
            local __subnet_vid=$(subnet_cidr_2_vid_from_config "${__interface_link_cidr}")
            if [[ "${__interface_fabric_name}" != "${__subnet_fabric_name}" \
                    || "${__interface_vid}" != "${__subnet_vid}" ]]; then
                move_interface_to_vlan "${__controller_id}" "${__interface_id}" "${__interface_link_cidr}"
            fi

        done

    done

}

function move_interface_to_vlan() {
    declare __machine_id=$1
    declare __interface_id=$2
    declare __subnet_cidr=$3

    local __fabric_name=$(subnet_cidr_2_subnet_fabric_name "${__subnet_cidr}")
    local __vid=$(subnet_cidr_2_subnet_vlan_vid "${__subnet_cidr}")
    echo "Moving interface ${__interface_id} of machine ${__machine_id} to ${__fabric_name} and VID ${__vid}..."

    local __vlan_id=$(subnet_cidr_2_subnet_vlan_id "${__subnet_cidr}")

    local __command=$(maas "${PROFILE}" interface update \
        "${__machine_id}" \
        "${__interface_id}" \
        vlan="${__vlan_id}")
}

# -----------------------------------------------

function delete_unused_fabrics() {
    local __fabric_name=""
    for __fabric_name in $(get_fabric_names); do
        if [[ "$(fabric_name_exists_in_subnet_config "${__fabric_name}")" = "false" ]]; then
            echo "Deleting ${__fabric_name}..."
            local __fabric_id=$(fabric_name_2_fabric_id "${__fabric_name}")
            local __command=$(maas "${PROFILE}" fabric delete "${__fabric_id}")
        fi
    done
}

# -----------------------------------------------

function wait_for_boot_resources_to_be_imported_and_synced() {
    while [[ "$(boot_resources_are_imported_and_synced)" = "false" ]]; do
        echo "Waiting for boot resources to be imported and synced."
        sleep 5
    done
    echo "Boot resources are imported and synced!"
}

function boot_resources_are_imported_and_synced() {
    local __maas_controller_system_id=$(get_maas_controller_system_id)
    local __command="$(maas "${PROFILE}" rack-controller list-boot-images "${__maas_controller_system_id}")"
    if [[ "$(echo "${__command}" | jq --raw-output '.status')" != "synced" ]]; then
        echo "false"
    else
        echo "true"
    fi
}

function start_machines() {

    echo "Starting machines for the first time."

    local __power_address=""

    for __power_address in $(get_power_addresses); do

        echo "Starting ${__power_address}..."

        local __power_type=$(power_address_2_power_type "${__power_address}")

        if [[ "${__power_type}" = "IPMI" ]]; then

            ipmipower --on \
                --hostname="${__power_address}" \
                --username=root \
                --password="${IDRAC_PASSWORD}" \
                --privilege-level=ADMIN \
                --driver-type=LAN_2_0 \
                --session-timeout=60000

        elif [[ "${__power_type}" = "AMT" ]]; then

            wake_up_amt_machine "${__power_address}"
            sleep "${SLEEP_AFTER_COMMAND}"
            echo "y" | amttool "${__power_address}" powerup pxe

        fi

        sleep "${SLEEP_AFTER_COMMAND}"

    done

}

function wait_for_machines_to_be_discovered() {
    local __discovery_timeout=600  # 10 minutes
    local __start_discovery_time=$(date +%s)
    while [[ "$(machines_discovered)" = "false" ]]; do
        local __now=$(date +%s)
        local __elapsed_time=$(( ${__now} - ${__start_discovery_time} ))
        if (( ${__elapsed_time} > ${__discovery_timeout} )); then
            echo "Discovery timeout reached!"
            reset_undiscovered_machines
            local __start_discovery_time=$(date +%s)
        fi
        echo "Waiting for machines to be discovered (timeout: ${__elapsed_time}s of ${__discovery_timeout}s)."
        sleep 5
    done
    echo "Machines discovered!"
}

function get_amt_machine_power_state() {
    declare __power_address=$1
    wake_up_amt_machine "${__power_address}"
    echo $(amttool "${__power_address}" info \
        | egrep --only-matching "Powerstate:[ ]+(S[0-9]).*" \
        | egrep --only-matching "(S[0-9])")
}

function wake_up_amt_machine() {
    declare __power_address=$1
    # We run amttool info two times because
    # it sometimes, it won't connect at first.
    amttool "${__power_address}" info >/dev/null 2>&1 && sleep 0.5
    amttool "${__power_address}" info >/dev/null 2>&1 && sleep 0.5
}

function machines_discovered() {
    local __power_address=""
    for __power_address in $(get_power_addresses); do
        if [[ "$(machine_discovered "${__power_address}")" = "false" ]]; then
            echo "false"
            return 0
        fi
    done
    echo "true"
}

function machine_discovered() {
    local __power_address=$1
    local __machine_system_id=$(power_address_2_system_id "${__power_address}")
    if [[ "${__machine_system_id}" = "" ]]; then
        echo "false"
        return 0
    elif [[ "$(maas "${PROFILE}" node read "${__machine_system_id}" | jq --raw-output '.status_name')" != "New" ]]; then
        echo "false"
        return 0
    fi
    echo "true"
}

function reset_undiscovered_machines() {

    local __power_address=""

    for __power_address in $(get_power_addresses); do

        if [[ "$(machine_discovered "${__power_address}")" = "true" ]]; then
            continue
        fi

        local __power_type=$(power_address_2_power_type "${__power_address}")

        if [[ "${__power_type}" = "IPMI" ]]; then

            echo "Resetting ${__power_address}..."

            ipmipower --reset --hostname=${__power_address} \
                --username=root --password=${IDRAC_PASSWORD} \
                --privilege-level=ADMIN --driver-type=LAN_2_0 --session-timeout=60000

        elif [[ "${__power_type}" = "AMT" && "$(get_amt_machine_power_state "${__power_address}")" = "S5" ]]; then

            echo "Resetting ${__power_address}..."
            wake_up_amt_machine "${__power_address}"
            echo "y" | amttool "${__power_address}" powerup pxe

        elif [[ "${__power_type}" = "AMT" && "$(get_amt_machine_power_state "${__power_address}")" = "S0" ]]; then

            echo "Resetting ${__power_address}..."
            wake_up_amt_machine "${__power_address}"
            echo "y" | amttool "${__power_address}" reset pxe

        else

            echo "Unable to reset ${__power_address}..."

        fi

        sleep "${SLEEP_AFTER_COMMAND}"

    done
}

function set_machines_hostname() {
    echo "Changing machines' hostname..."
    local __power_address=""
    for __power_address in $(get_power_addresses); do
        local __hostname=$(power_address_2_hostname "${__power_address}")
        local __machine_system_id=$(power_address_2_system_id "${__power_address}")
        echo "Changing hostname to ${__hostname} for ${__power_address} (id: ${__machine_system_id})."
        local __command=$(maas "${PROFILE}" machine update "${__machine_system_id}" hostname="${__hostname}")
        sleep "${SLEEP_AFTER_COMMAND}"
    done
}

function configure_machines_power_parameters() {

    echo "Filling power parameters for machines..."

    local __power_address=""

    for __power_address in $(get_power_addresses); do

        local __machine_system_id=$(power_address_2_system_id "${__power_address}")

        echo "Filling power parameters for machine $(power_address_2_hostname "${__power_address}")."

        if [[ "$(power_address_2_power_type "${__power_address}")" = "AMT" ]]; then

            local __command=$(maas "${PROFILE}" machine update "${__machine_system_id}" \
                power_type=amt \
                power_parameters_power_address="${__power_address}" \
                power_parameters_power_pass="${AMT_PASSWORD}")

        elif [[ "$(power_address_2_power_type "${__power_address}")" = "IPMI" ]]; then

            local __command=$(maas "${PROFILE}" machine update "${__machine_system_id}" \
                power_type=ipmi \
                power_parameters_power_address="${__power_address}" \
                power_parameters_mac_address="$(power_address_2_mac_address "${__power_address}")" \
                power_parameters_power_user="root" \
                power_parameters_power_pass="${IDRAC_PASSWORD}")

        fi

        sleep "${SLEEP_AFTER_COMMAND}"

    done

}

function commission_machines() {
    echo "Commissioning machines..."
    local __power_address=""
    for __power_address in $(get_power_addresses); do
        local __machine_system_id=$(power_address_2_system_id "${__power_address}")
        echo "Commissioning $(power_address_2_hostname "${__power_address}")."
        local __command=$(maas "${PROFILE}" machine commission \
            "${__machine_system_id}" \
            testing_scripts="${HARDWARE_TESTING_SCRIPTS}")
        sleep "${SLEEP_AFTER_COMMAND}"
    done
}

function wait_for_machines_to_be_commissioned() {
    while [[ "$(machines_commissioned)" = "false" ]]; do
        echo "Waiting for machines to be commissioned."
        recommission_failed
        sleep 5
    done
    echo "Machines are commissioned!"
}

function machines_commissioned() {
    local __machine_id=""
    for __machine_id in $(get_machines_id); do
        if [[ "$(machine_id_2_status_name "${__machine_id}")" != "Ready" ]]; then
            echo "false"
            return 0
        fi
    done
    echo "true"
}

function recommission_failed() {

    local __power_address=""

    for __power_address in $(get_power_addresses); do

        local __machine_system_id=$(power_address_2_system_id "${__power_address}")
        local __status_name=$(machine_id_2_status_name "${__machine_system_id}")

        if [[ "${__status_name}" = "Ready" ]]; then
            continue
        elif [[ "${__status_name}" = "Testing" ]]; then
            continue
        elif [[ "${__status_name}" = "Commissioning" ]]; then
            continue
        else
            echo "Recommissioning machine $(power_address_2_hostname "${__power_address}")."
            local __command=$(maas "${PROFILE}" machine commission \
                "${__machine_system_id}" \
                testing_scripts="${HARDWARE_TESTING_SCRIPTS}")
            sleep "${SLEEP_AFTER_COMMAND}"
        fi

    done

}

function configure_machines_storage() {

    echo "Configuring machines storage..."

    local __power_address=""

    for __power_address in $(get_power_addresses); do

        delete_existing_partitions "${__power_address}"

        create_partitions "${__power_address}"

    done

}

function delete_existing_partitions() {

    declare __power_address=$1

    local __hostname=$(power_address_2_hostname "${__power_address}")
    local __machine_id=$(hostname_2_machine_id "${__hostname}")

    local __block_device_id=""
    local __partition_id=""

    for __block_device_id in $(get_block_devices_id "${__machine_id}"); do

        for __partition_id in $(get_partitions_id "${__machine_id}" "${__block_device_id}"); do

            echo "Deleting partition ${__partition_id} in" \
                "block device ${__block_device_id} of" \
                "${__hostname}."

            local __command=$(maas "${PROFILE}" partition delete \
                "${__machine_id}" \
                "${__block_device_id}" \
                "${__partition_id}")

        done

    done

}

function create_partitions() {

    declare __power_address=$1

    local __hostname=$(power_address_2_hostname "${__power_address}")
    local __machine_id=$(hostname_2_machine_id "${__hostname}")

    local __block_device_name_from_config=""
    local __partition_name_from_config=""

    for __block_device_name_from_config in $(get_block_devices_name_from_config "${__power_address}"); do

        local __block_device_id=$(block_device_name_2_block_device_id \
            "${__machine_id}" \
            "${__block_device_name_from_config}")

        local __boot_disk_from_config=$(block_device_2_boot_disk_from_config \
            "${__power_address}" \
            "${__block_device_name_from_config}")

        if [[ "${__boot_disk_from_config,,}" = "true" ]]; then
            echo "Setting disk ${__block_device_name_from_config}" \
                "(id=${__block_device_id}) of" \
                "${__hostname} as boot disk."
            local __command=$(maas "${PROFILE}" block-device set-boot-disk \
                "${__machine_id}" \
                "${__block_device_id}")
        fi

        for __partition_name_from_config in $(get_partitions_name_from_config \
                                                "${__power_address}" \
                                                "${__block_device_name_from_config}"); do

            # Creating partition
            local __size_from_config=$(partition_2_size_from_config \
                "${__power_address}" \
                "${__block_device_name_from_config}" \
                "${__partition_name_from_config}")

            local __bootable_from_config=$(partition_2_bootable_from_config \
                "${__power_address}" \
                "${__block_device_name_from_config}" \
                "${__partition_name_from_config}")

            echo "Creating partition ${__partition_name_from_config}" \
                "(size=${__size_from_config}, bootable=${__bootable_from_config}) in" \
                "block device ${__block_device_name_from_config} of ${__hostname}."

            if [[ "${__size_from_config}" = "" ]]; then
                local __command=$(maas "${PROFILE}" partitions create \
                    "${__machine_id}" \
                    "${__block_device_id}" \
                    bootable="${__bootable_from_config,,}")
            else
                local __command=$(maas "${PROFILE}" partitions create \
                    "${__machine_id}" \
                    "${__block_device_id}" \
                    size="${__size_from_config}" \
                    bootable="${__bootable_from_config,,}")
            fi

            # Extracting newly created partition ID
            local __partition_id=$(echo "${__command}" \
                | jq --raw-output '.id')

            # Formatting partition
            local __fstype_from_config=$(partition_2_fstype_from_config \
                "${__power_address}" \
                "${__block_device_name_from_config}" \
                "${__partition_name_from_config}")

            echo "Formatting to ${__fstype_from_config}" \
                "partition ${__partition_name_from_config} (id=${__partition_id}) in" \
                "block device ${__block_device_name_from_config} of ${__hostname}."

            local __command=$(maas "${PROFILE}" partition format \
                "${__machine_id}" \
                "${__block_device_id}" \
                "${__partition_id}" \
                fstype="${__fstype_from_config}" \
                label="${__partition_name_from_config}")

            # Mounting partition
            local __mount_point_from_config=$(partition_2_mount_point_from_config \
                "${__power_address}" \
                "${__block_device_name_from_config}" \
                "${__partition_name_from_config}")

            if [[ "${__mount_point_from_config}" != "" ]]; then

                echo "Mounting to ${__mount_point_from_config}"\
                    "partition ${__partition_name_from_config} (id=${__partition_id}) in" \
                    "block device ${__block_device_name_from_config} of ${__hostname}."

                local __command=$(maas "${PROFILE}" partition mount \
                    "${__machine_id}" \
                    "${__block_device_id}" \
                    "${__partition_id}" \
                    mount_point="${__mount_point_from_config}")

            fi


        done

    done

}

# -----------------------------------------------

function get_maas_controller_system_id() {
    echo $(maas "${PROFILE}" nodes read hostname="${MAAS_CONTROLLER_HOSTNAME}" \
        | grep system_id -m 1 \
        | cut -d '"' -f 4)
}

# -----------------------------------------------

function subnet_cidr_2_subnet_info() {
    local __subnet_cidr=$1
    local __subnets_info=$(maas "${PROFILE}" subnets read)
    local __subnet_info=$(echo "${__subnets_info}" \
        | jq --raw-output --arg __subnet_cidr "${__subnet_cidr}" \
        '.[]
        | select((.cidr|tostring)==$__subnet_cidr)')
    echo "${__subnet_info}"
}

function subnet_cidr_2_subnet_id() {
    declare __subnet_cidr=$1
    local __subnet_info=$(subnet_cidr_2_subnet_info "${__subnet_cidr}")
    local __subnet_id=$(echo "${__subnet_info}" \
        | jq --raw-output '.id')
    echo "${__subnet_id}"
}

function subnet_cidr_2_subnet_vlan_vid() {
    declare __subnet_cidr=$1
    local __subnet_info=$(subnet_cidr_2_subnet_info "${__subnet_cidr}")
    local __subnet_vlan_vid=$(echo "${__subnet_info}" \
        | jq --raw-output '.vlan.vid')
    echo "${__subnet_vlan_vid}"
}

function subnet_cidr_2_subnet_vlan_id() {
    declare __subnet_cidr=$1
    local __subnet_info=$(subnet_cidr_2_subnet_info "${__subnet_cidr}")
    local __subnet_vlan_id=$(echo "${__subnet_info}" \
        | jq --raw-output '.vlan.id')
    echo "${__subnet_vlan_id}"
}

function subnet_cidr_2_subnet_fabric_name() {
    declare __subnet_cidr=$1
    local __subnet_info=$(subnet_cidr_2_subnet_info "${__subnet_cidr}")
    local __subnet_fabric_name=$(echo "${__subnet_info}" \
        | jq --raw-output '.vlan.fabric')
    echo "${__subnet_fabric_name}"
}

function subnet_cidr_2_subnet_fabric_id() {
    declare __subnet_cidr=$1
    local __subnet_info=$(subnet_cidr_2_subnet_info "${__subnet_cidr}")
    local __subnet_fabric_id=$(echo "${__subnet_info}" \
        | jq --raw-output '.vlan.fabric_id')
    echo "${__subnet_fabric_id}"
}

function subnet_cidr_2_subnet_managed() {
    declare __subnet_cidr=$1
    local __subnet_info=$(subnet_cidr_2_subnet_info "${__subnet_cidr}")
    local __subnet_managed=$(echo "${__subnet_info}" \
        | jq --raw-output '.managed')
    echo "${__subnet_managed}"
}

function subnet_cidr_2_subnet_dhcp_on() {
    declare __subnet_cidr=$1
    local __subnet_info=$(subnet_cidr_2_subnet_info "${__subnet_cidr}")
    local __subnet_dhcp_on=$(echo "${__subnet_info}" \
        | jq --raw-output '.vlan.dhcp_on')
    echo "${__subnet_dhcp_on}"
}

function subnet_cidr_2_subnet_dns_servers() {
    declare __subnet_cidr=$1
    local __subnet_info=$(subnet_cidr_2_subnet_info "${__subnet_cidr}")
    local __subnet_dns_servers=$(echo "${__subnet_info}" \
        | jq --raw-output '.dns_servers | sort | join(",")')
    echo "${__subnet_dns_servers}"
}

function subnet_cidr_2_subnet_gateway_ip() {
    declare __subnet_cidr=$1
    local __subnet_info=$(subnet_cidr_2_subnet_info "${__subnet_cidr}")
    local __subnet_gateway_ip=$(echo "${__subnet_info}" \
        | jq --raw-output '.gateway_ip')
    echo "${__subnet_gateway_ip}"
}

# -----------------------------------------------

function get_fabric_names() {
    local __fabrics_info=$(maas "${PROFILE}" fabrics read)
    local __fabric_name=$(echo "${__fabrics_info}" | \
        jq --raw-output '.[].name')
    echo "${__fabric_name}"
}

function fabric_name_2_fabric_info() {
    declare __fabric_name=$1
    local __fabrics_info=$(maas "${PROFILE}" fabrics read)
    local __fabric_info=$(echo "${__fabrics_info}" \
        | jq --raw-output --arg __fabric_name "${__fabric_name}" \
        '.[] | select((.name|tostring)==$__fabric_name)')
    echo "${__fabric_info}"
}

function fabric_name_2_fabric_id() {
    declare __fabric_name=$1
    local __fabric_info=$(fabric_name_2_fabric_info "${__fabric_name}")
    local __fabric_id=$(echo "${__fabric_info}" \
        | jq --raw-output '.id')
    echo "${__fabric_id}"
}

function fabric_name_and_vid_2_fabric_vlan_id() {
    declare __fabric_name=$1
    declare __vid=$2
    local __fabric_info=$(fabric_name_2_fabric_info "${__fabric_name}")
    local __fabric_vlan_id=$(echo "${__fabric_info}" \
        | jq --raw-output --arg __vid "${__vid}" \
        '.vlans[] | select((.vid|tostring)==$__vid) | .id')
    echo "${__fabric_vlan_id}"
}

# -----------------------------------------------

function get_interfaces_info() {
    declare __machine_id=$1
    local __interfaces_info=$(maas "${PROFILE}" interfaces read "${__machine_id}")
    echo "${__interfaces_info}"
}

function get_interface_info() {
    declare __machine_id=$1
    declare __interface_id=$2
    local __interfaces_info=$(get_interfaces_info "${__machine_id}")
    local __interface_info=$(echo "${__interfaces_info}" \
        | jq --raw-output --arg __interface_id "${__interface_id}" \
        '.[] | select((.id|tostring)==$__interface_id)')
    echo "${__interface_info}"
}

function get_interfaces_id() {
    declare __machine_id=$1
    local __interfaces_info=$(get_interfaces_info "${__machine_id}")
    local __interfaces_id=$(echo "${__interfaces_info}" | jq --raw-output '.[].id')
    echo "${__interfaces_id}"
}

function interface_2_fabric_name() {
    declare __machine_id=$1
    declare __interface_id=$2
    local __interface_info=$(get_interface_info "${__machine_id}" "${__interface_id}")
    local __fabric_name=$(echo "${__interface_info}" | jq --raw-output '.vlan.fabric')
}

function interface_2_vid() {
    declare __machine_id=$1
    declare __interface_id=$2
    local __interface_info=$(get_interface_info "${__machine_id}" "${__interface_id}")
    local __fabric_name=$(echo "${__interface_info}" | jq --raw-output '.vlan.vid')
}

function get_interface_links_info() {
    declare __machine_id=$1
    declare __interface_id=$2
    local __interface_info=$(get_interface_info "${__machine_id}" "${__interface_id}")
    local __interface_links_info=$(echo "${__interface_info}" | jq --raw-output '.links')
    echo "${__interface_links_info}"
}

function get_interface_links_id() {
    declare __machine_id=$1
    declare __interface_id=$2
    local __interface_links_info=$(get_interface_links_info "${__machine_id}" "${__interface_id}")
    local __interface_links_id=$(echo "${__interface_links_info}" | jq --raw-output '.[].id')
    echo "${__interface_links_id}"
}

function get_interface_link_info() {
    declare __machine_id=$1
    declare __interface_id=$2
    declare __interface_link_id=$3
    local __interface_links_info=$(get_interface_links_info "${__machine_id}" "${__interface_id}")
    local __interface_link_info=$(echo "${__interface_links_info}" \
        | jq --raw-output --arg __interface_link_id "${__interface_link_id}" \
        '.[] | select((.id|tostring)==$__interface_link_id)')
    echo "${__interface_link_info}"
}

function interface_link_2_subnet_cidr() {
    declare __machine_id=$1
    declare __interface_id=$2
    declare __interface_link_id=$3
    local __interface_link_info=$(get_interface_link_info "${__machine_id}" "${__interface_id}" "${__interface_link_id}")
    local __subnet_cidr=$(echo "${__interface_link_info}" | jq --raw-output '.subnet.cidr')
    echo "${__subnet_cidr}"
}

# -----------------------------------------------

function get_ip_ranges_info() {
    local __ip_ranges_info=$(maas ${PROFILE} ipranges read)
    echo "${__ip_ranges_info}"
}

function get_ip_range_info() {
    declare __ip_range_id=$1
    local __ip_range_info=$(echo "$(get_ip_ranges_info)" | \
        jq --raw-output --arg __ip_range_id "${__ip_range_id}" \
        '.[] | select((.id|tostring)==$__ip_range_id)')
    echo "${__ip_range_info}"
}

function get_ip_ranges_id() {
    local __ip_ranges_id=$(echo "$(get_ip_ranges_info)" | \
        jq --raw-output \
        '.[].id')
    echo "${__ip_ranges_id}"
}

function ip_range_id_2_start_ip() {
    declare __ip_range_id=$1
    local __ip_range_info=$(get_ip_range_info "${__ip_range_id}")
    local __start_ip=$(echo "${__ip_range_info}" | \
        jq --raw-output \
        '.start_ip')
    echo "${__start_ip}"
}

function ip_range_id_2_end_ip() {
    declare __ip_range_id=$1
    local __ip_range_info=$(get_ip_range_info "${__ip_range_id}")
    local __end_ip=$(echo "${__ip_range_info}" | \
        jq --raw-output \
        '.end_ip')
    echo "${__end_ip}"
}

function ip_range_id_2_subnet_cidr() {
    declare __ip_range_id=$1
    local __ip_range_info=$(get_ip_range_info "${__ip_range_id}")
    local __subnet_cidr=$(echo "${__ip_range_info}" | \
        jq --raw-output \
        '.subnet.cidr')
    echo "${__subnet_cidr}"
}

# -----------------------------------------------

function block_device_name_2_block_device_id() {
    declare __machine_id=$1
    declare __block_device_name=$2
    local __block_devices_info=$(get_block_devices_info "${__machine_id}")
    local __block_device_id=$(echo "${__block_devices_info}" | \
        jq --raw-output --arg __block_device_name "${__block_device_name}" \
        '.[] | select((.name|tostring)==$__block_device_name) | .id')
    echo "${__block_device_id}"
}

function get_partition_info() {
    declare __machine_id=$1
    declare __block_device_id=$2
    declare __partition_id=$3
    local __block_device_info=$(get_block_device_info \
        "${__machine_id}" \
        "${__block_device_id}")
    local __partition_info=$(echo "${__block_device_info}" | \
        jq --raw-output --arg __partition_id "${__partition_id}" \
        '.partitions[] | select((.id|tostring)==$__partition_id)')
    echo "${__partition_info}"
}

function get_partitions_id() {
    declare __machine_id=$1
    declare __block_device_id=$2
    local __block_device_info=$(get_block_device_info \
        "${__machine_id}" \
        "${__block_device_id}")
    local __partitions_id=$(echo "${__block_device_info}" | \
        jq --raw-output '.partitions[].id')
    echo "${__partitions_id}"
}

function get_partitions_info() {
    declare __machine_id=$1
    declare __block_device_id=$2
    local __block_device_info=$(get_block_device_info \
        "${__machine_id}" \
        "${__block_device_id}")
    local __partitions_info=$(echo "${__block_device_info}" | \
        jq --raw-output '.partitions')
    echo "${__partitions_info}"
}

function get_block_device_info() {
    declare __machine_id=$1
    declare __block_device_id=$2
    local __machine_info=$(get_machine_info "${__machine_id}")
    local __block_device_info=$(echo "${__machine_info}" | \
        jq --raw-output --arg __block_device_id "${__block_device_id}" \
        '.blockdevice_set[] | select((.id|tostring)==$__block_device_id)')
    echo "${__block_device_info}"
}

function get_block_devices_id() {
    declare __machine_id=$1
    local __machine_info=$(get_machine_info "${__machine_id}")
    local __block_devices_id=$(echo "${__machine_info}" | \
        jq --raw-output '.blockdevice_set[].id')
    echo "${__block_devices_id}"
}

function get_block_devices_info() {
    declare __machine_id=$1
    local __machine_info=$(get_machine_info "${__machine_id}")
    local __block_devices_info=$(echo "${__machine_info}" | \
        jq --raw-output '.blockdevice_set')
    echo "${__block_devices_info}"
}

# -----------------------------------------------

function machine_id_2_status_name() {
    declare __machine_id=$1
    local __status_name=$(echo "$(get_machine_info "${__machine_id}")" \
        | jq --raw-output '.status_name')
    echo "${__status_name}"
}

function machine_id_2_hostname() {
    declare __machine_id=$1
    local __hostname=$(echo "$(get_machine_info "${__machine_id}")" \
        | jq --raw-output '.hostname')
    echo "${__hostname}"
}

function hostname_2_machine_id() {
    declare __hostname=$1
    local __machine_id=$(echo "$(get_machines_info)" \
        | jq --raw-output --arg __hostname "${__hostname}" \
        '.[] | select((.hostname|tostring)==$__hostname) | .system_id')
    echo "${__machine_id}"
}

function get_machines_id() {
    local __machines_id=$(echo "$(get_machines_info)" | \
        jq --raw-output '.[].system_id')
    echo "${__machines_id}"
}

function get_machine_info() {
    declare __machine_id=$1
    local __machine_info=$(maas "${PROFILE}" machine read "${__machine_id}")
    echo "${__machine_info}"
}

function get_machines_info() {
    local __machines_info=$(maas "${PROFILE}" machines read)
    echo "${__machines_info}"
}

# -----------------------------------------------

function get_power_addresses() {
    local __machines_config=$(cat "${MACHINES_FILE_PATH}")
    local __power_addresses=$(echo "${__machines_config}" | jq --raw-output '.[].power_address')
    echo "${__power_addresses}"
}

function power_address_2_mac_address() {
    declare __power_address=$1
    local __machines_config=$(cat "${MACHINES_FILE_PATH}")
    local __mac_address=$(echo "${__machines_config}" \
        | jq --raw-output --arg __power_address "${__power_address}" \
        '.[]
        | select((.power_address|tostring)==$__power_address)
        | .mac_address')
    local __mac_address=$(echo "${__mac_address,,}")  # Make lowercase
    echo "${__mac_address}"
}

function power_address_2_system_id() {
    local __power_address=$1
    local __nodes_info=$(maas "${PROFILE}" nodes read)
    local __mac_address=$(power_address_2_mac_address "${__power_address}")
    local __machine_system_id=$(echo "${__nodes_info}" \
        | jq --raw-output --arg __mac_address "${__mac_address}" \
        '.[].boot_interface
        | select((.mac_address|tostring)==$__mac_address)
        | .system_id')
    echo "${__machine_system_id}"
}

function power_address_2_power_type() {
    local __power_address=$1
    local __machines_config=$(cat "${MACHINES_FILE_PATH}")
    local __power_type=$(echo "${__machines_config}" \
        | jq --raw-output --arg __power_address "${__power_address}" \
        '.[]
        | select((.power_address|tostring)==$__power_address)
        | .power_type')
    echo "${__power_type}"
}

function power_address_2_hostname() {
    local __power_address=$1
    local __machines_config=$(cat "${MACHINES_FILE_PATH}")
    local __hostname=$(echo "${__machines_config}" \
        | jq --raw-output --arg __power_address "${__power_address}" \
        '.[]
        | select((.power_address|tostring)==$__power_address)
        | .hostname')
    echo "${__hostname}"
}

function get_machine_info_from_config() {
    declare __power_address=$1
    local __machine_info_from_config=$(cat "${MACHINES_FILE_PATH}" \
        | jq --raw-output --arg __power_address "${__power_address}" \
        '.[] | select((.power_address|tostring)==$__power_address)')
    echo "${__machine_info_from_config}"
}

function get_block_devices_name_from_config() {
    declare __power_address=$1
    local __machine_info_from_config=$(get_machine_info_from_config "${__power_address}")
    local __block_devices_name_from_config=$(echo "${__machine_info_from_config}" \
        | jq --raw-output '.block_devices[].name')
    echo "${__block_devices_name_from_config}"
}

function get_block_device_info_from_config() {
    declare __power_address=$1
    declare __block_device_name=$2
    local __machine_info_from_config=$(get_machine_info_from_config "${__power_address}")
    local __block_device_info_from_config=$(echo "${__machine_info_from_config}" \
        | jq --raw-output --arg __block_device_name "${__block_device_name}" \
        '.block_devices[] | select((.name|tostring)==$__block_device_name)')
    echo "${__block_device_info_from_config}"
}

function get_partitions_name_from_config() {
    declare __power_address=$1
    declare __block_device_name=$2
    local __block_device_info_from_config=$(get_block_device_info_from_config \
        "${__power_address}" \
        "${__block_device_name}")
    local __partitions_name_from_config=$(echo "${__block_device_info_from_config}" \
        | jq --raw-output \
        '.partitions[].name')
    echo "${__partitions_name_from_config}"
}

function get_partition_info_from_config() {
    declare __power_address=$1
    declare __block_device_name=$2
    declare __partition_name=$3
    local __block_device_info_from_config=$(get_block_device_info_from_config \
        "${__power_address}" \
        "${__block_device_name}")
    local __partition_info_from_config=$(echo "${__block_device_info_from_config}" \
        | jq --raw-output --arg __partition_name "${__partition_name}" \
        '.partitions[] | select((.name|tostring)==$__partition_name)')
    echo "${__partition_info_from_config}"
}

function block_device_2_boot_disk_from_config() {
    declare __power_address=$1
    declare __block_device_name=$2
    local __block_device_info_from_config=$(get_block_device_info_from_config \
        "${__power_address}" \
        "${__block_device_name}")
    local __boot_disk_from_config=$(echo "${__block_device_info_from_config}" \
        | jq --raw-output '.boot_disk')
    echo "${__boot_disk_from_config}"
}

function partition_2_fstype_from_config() {
    declare __power_address=$1
    declare __block_device_name=$2
    declare __partition_name=$3
    local __partition_info_from_config=$(get_partition_info_from_config \
        "${__power_address}" \
        "${__block_device_name}" \
        "${__partition_name}")
    local __fstype_from_config=$(echo "${__partition_info_from_config}" \
        | jq --raw-output '.fstype')
    echo "${__fstype_from_config}"
}

function partition_2_size_from_config() {
    declare __power_address=$1
    declare __block_device_name=$2
    declare __partition_name=$3
    local __partition_info_from_config=$(get_partition_info_from_config \
        "${__power_address}" \
        "${__block_device_name}" \
        "${__partition_name}")
    local __size_from_config=$(echo "${__partition_info_from_config}" \
        | jq --raw-output '.size')
    echo "${__size_from_config}"
}

function partition_2_mount_point_from_config() {
    declare __power_address=$1
    declare __block_device_name=$2
    declare __partition_name=$3
    local __partition_info_from_config=$(get_partition_info_from_config \
        "${__power_address}" \
        "${__block_device_name}" \
        "${__partition_name}")
    local __mount_point_from_config=$(echo "${__partition_info_from_config}" \
        | jq --raw-output '.mount_point')
    echo "${__mount_point_from_config}"
}

function partition_2_bootable_from_config() {
    declare __power_address=$1
    declare __block_device_name=$2
    declare __partition_name=$3
    local __partition_info_from_config=$(get_partition_info_from_config \
        "${__power_address}" \
        "${__block_device_name}" \
        "${__partition_name}")
    local __bootable_from_config=$(echo "${__partition_info_from_config}" \
        | jq --raw-output '.bootable')
    echo "${__bootable_from_config}"
}

# -----------------------------------------------

function get_subnets_cidr_from_config() {
    local __subnets_cidr=$(cat "${SUBNETS_FILE_PATH}" \
        | jq --raw-output \
        '.[].cidr')
    echo "${__subnets_cidr}"
}

function get_subnet_from_config() {
    declare __subnet_cidr=$1
    local __subnet=$(cat "${SUBNETS_FILE_PATH}" \
        | jq --raw-output --arg __subnet_cidr "${__subnet_cidr}" \
        '.[] | select((.cidr|tostring)==$__subnet_cidr)')
    echo "${__subnet}"
}

function subnet_cidr_2_fabric_name_from_config() {
    declare __subnet_cidr=$1
    local __subnet_from_config=$(get_subnet_from_config "${__subnet_cidr}")
    local __fabric_name=$(echo "${__subnet_from_config}" \
        | jq --raw-output \
        '.fabric_name')
    echo "${__fabric_name}"
}

function subnet_cidr_2_vid_from_config() {
    declare __subnet_cidr=$1
    local __subnet_from_config=$(get_subnet_from_config "${__subnet_cidr}")
    local __vid=$(echo "${__subnet_from_config}" \
        | jq --raw-output \
        '.vid')
    echo "${__vid}"
}

function subnet_cidr_2_managed_from_config() {
    declare __subnet_cidr=$1
    local __subnet_from_config=$(get_subnet_from_config "${__subnet_cidr}")
    local __managed=$(echo "${__subnet_from_config}" \
        | jq --raw-output \
        '.managed')
    echo "${__managed}"
}

function subnet_cidr_2_dhcp_from_config() {
    declare __subnet_cidr=$1
    local __subnet_from_config=$(get_subnet_from_config "${__subnet_cidr}")
    local __dhcp=$(echo "${__subnet_from_config}" \
        | jq --raw-output \
        '.dhcp')
    echo "${__dhcp}"
}

function subnet_cidr_2_start_ip_address_from_config() {
    declare __subnet_cidr=$1
    local __subnet_from_config=$(get_subnet_from_config "${__subnet_cidr}")
    local __start_ip_address=$(echo "${__subnet_from_config}" \
        | jq --raw-output \
        '.start_ip_address')
    echo "${__start_ip_address}"
}

function subnet_cidr_2_end_ip_address_from_config() {
    declare __subnet_cidr=$1
    local __subnet_from_config=$(get_subnet_from_config "${__subnet_cidr}")
    local __end_ip_address=$(echo "${__subnet_from_config}" \
        | jq --raw-output \
        '.end_ip_address')
    echo "${__end_ip_address}"
}

function subnet_cidr_2_dns_servers_from_config() {
    declare __subnet_cidr=$1
    local __subnet_from_config=$(get_subnet_from_config "${__subnet_cidr}")
    local __dns_servers=$(echo "${__subnet_from_config}" \
        | jq --raw-output \
        '.dns_servers | sort | join(",")')
    echo "${__dns_servers}"
}

function subnet_cidr_2_gateway_from_config() {
    declare __subnet_cidr=$1
    local __subnet_from_config=$(get_subnet_from_config "${__subnet_cidr}")
    local __gateway=$(echo "${__subnet_from_config}" \
        | jq --raw-output \
        '.gateway')
    echo "${__gateway}"
}

function subnet_cidr_exists_in_subnet_config() {
    declare __subnet_cidr=$1
    for __subnet_cidr_in_config in $(get_subnets_cidr_from_config); do
        if [[ "${__subnet_cidr}" = "${__subnet_cidr_in_config}" ]]; then
            echo "true"
            return 0
        fi
    done
    echo "false"
}

function fabric_name_exists_in_subnet_config() {
    declare __fabric_name=$1
    for __subnet_cidr_in_config in $(get_subnets_cidr_from_config); do
        local __fabric_name_from_config=$(subnet_cidr_2_fabric_name_from_config "${__subnet_cidr_in_config}")
        if [[ "${__fabric_name_from_config}" = "${__fabric_name}" ]]; then
            echo "true"
            return 0
        fi
    done
    echo "false"
}

# -----------------------------------------------

main
