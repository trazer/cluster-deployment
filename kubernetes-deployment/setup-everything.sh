#!/usr/bin/env bash

source ../ask-sudo-password.sh
export ROOT_PASSWORD=$(ask_for_password_until_correct | tail -1)

./install-juju.sh && \
./add-maas-cloud.sh && \
./juju-bootstrap.sh && \
./prepare-machines.sh && \
./deploy-kubernetes.sh && \
./deploy-ceph.sh && \
./install-kubectl.sh
