#!/usr/bin/env bash

if [[ -z ${ROOT_PASSWORD+x} ]]; then
    source ../ask-sudo-password.sh
    export ROOT_PASSWORD=$(ask_for_password_until_correct | tail -1)
fi

# https://docs.jujucharms.com/2.4/en/reference-install
echo $ROOT_PASSWORD | sudo -S snap install juju --classic
