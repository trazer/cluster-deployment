#!/usr/bin/env bash

if [[ -z ${ROOT_PASSWORD+x} ]]; then
    source ../ask-sudo-password.sh
    export ROOT_PASSWORD=$(ask_for_password_until_correct | tail -1)
fi

while [[ "$(juju show-status --format=json kubernetes-master/0 | jq -r '.applications."kubernetes-master"."application-status".current')" != "active" ]]
do
	echo "kubernetes-master not ready..."
	sleep 60
done
echo "kubernetes-master ready..."

mkdir -p ~/.kube

juju scp kubernetes-master/0:config ~/.kube/config

echo $ROOT_PASSWORD | sudo -S snap install kubectl --classic
