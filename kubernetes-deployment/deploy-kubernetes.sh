#!/usr/bin/env bash

# https://docs.jujucharms.com/2.4/en/charms-deploying-advanced#deploying-to-specific-machines

# easyrsa
juju deploy --to 0/lxd/0 "cs:~containers/easyrsa-117"

# etcd
juju deploy --to 0 "cs:~containers/etcd-209"
juju add-unit --to 1 etcd
juju add-unit --to 2 etcd

# flannel
juju deploy "cs:~containers/flannel-146"

# kubeapi-load-balancer
juju deploy --to 0/lxd/1 "cs:~containers/kubeapi-load-balancer-162"
juju add-unit --to 2/lxd/1 kubeapi-load-balancer
juju expose kubeapi-load-balancer

# kubernetes-master
juju deploy --to 1/lxd/0 "cs:~containers/kubernetes-master-219"
juju add-unit --to 2/lxd/0 kubernetes-master

# kubernetes-worker
juju deploy --to 3 "cs:~containers/kubernetes-worker-239"
juju add-unit --to 4 kubernetes-worker
juju expose kubernetes-worker

# Relations
juju add-relation kubernetes-master:kube-api-endpoint kubeapi-load-balancer:apiserver
juju add-relation kubernetes-master:loadbalancer kubeapi-load-balancer:loadbalancer
juju add-relation kubernetes-master:kube-control kubernetes-worker:kube-control
juju add-relation kubernetes-master:certificates easyrsa:client
juju add-relation etcd:certificates easyrsa:client
juju add-relation kubernetes-master:etcd etcd:db
juju add-relation kubernetes-worker:certificates easyrsa:client
juju add-relation kubernetes-worker:kube-api-endpoint kubeapi-load-balancer:website
juju add-relation kubeapi-load-balancer:certificates easyrsa:client
juju add-relation flannel:etcd etcd:db
juju add-relation flannel:cni kubernetes-master:cni
juju add-relation flannel:cni kubernetes-worker:cni
