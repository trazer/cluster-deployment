#!/usr/bin/env bash

if [[ -z ${ROOT_PASSWORD+x} ]]; then
    source ../ask-sudo-password.sh
    export ROOT_PASSWORD=$(ask_for_password_until_correct | tail -1)
fi

juju destroy-controller -y --destroy-all-models --destroy-storage testmaas

juju remove-cloud testmaas

echo $ROOT_PASSWORD | sudo -S snap remove juju

echo $ROOT_PASSWORD | sudo -S snap remove kubectl

rm --recursive ~/.kube
