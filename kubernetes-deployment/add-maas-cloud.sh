#!/usr/bin/env bash

# https://docs.jujucharms.com/2.4/en/clouds-maas
juju add-cloud --replace testmaas maas-clouds.yaml


# https://docs.jujucharms.com/2.4/en/credentials
juju add-credential --replace testmaas -f creds.yaml
