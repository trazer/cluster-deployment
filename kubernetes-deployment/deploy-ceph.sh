#!/usr/bin/env bash

# ceph-mon
juju deploy --to 5/lxd/0 "cs:ceph-mon-29"
juju add-unit --to 6/lxd/0 ceph-mon
juju add-unit --to 7/lxd/0 ceph-mon

# ceph-osd
juju deploy --to 5 "cs:ceph-osd-271"
juju add-unit --to 6 ceph-osd
juju add-unit --to 7 ceph-osd

# Relations
juju add-relation ceph-mon ceph-osd

# Wait for deployment of Ceph to settle
while [[ "$(juju show-status --format=json | jq -r '.applications."ceph-mon"."application-status".current')" != "active" ]]
do
    echo "ceph-mon not ready..."
    sleep 30
done
echo "ceph-mon ready..."

# Run actions
juju run-action ceph-osd/0 zap-disk devices=/dev/sda i-really-mean-it=True
juju run-action ceph-osd/0 add-disk osd-devices=/dev/sda

juju run-action ceph-osd/1 zap-disk devices=/dev/sda i-really-mean-it=True
juju run-action ceph-osd/1 add-disk osd-devices=/dev/sda

juju run-action ceph-osd/2 zap-disk devices=/dev/sda i-really-mean-it=True
juju run-action ceph-osd/2 add-disk osd-devices=/dev/sda
