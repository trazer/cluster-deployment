#!/usr/bin/env bash

EASYRSA_CONSTRAINTS="root-disk=16G"
KUBEAPI_LOAD_BALANCER_CONSTRAINTS="root-disk=16G"
KUBERNETES_MASTER_CONSTRAINTS="root-disk=16G"

juju add-machine hp8300-1.maas  # 0 (etcd/0)
juju add-machine --constraints "$EASYRSA_CONSTRAINTS" lxd:0  # 0/lxd/0 (easyrsa/0)
juju add-machine --constraints "$KUBEAPI_LOAD_BALANCER_CONSTRAINTS" lxd:0  # 0/lxd/1 (kubeapi-load-balancer/0)

juju add-machine hp8300-2.maas  # 1 (etcd/1)
juju add-machine --constraints "$KUBERNETES_MASTER_CONSTRAINTS" lxd:1  # 1/lxd/0 (kubernetes-master/0)

juju add-machine hp8300-3.maas  # 2 (etcd/2)
juju add-machine --constraints "$KUBERNETES_MASTER_CONSTRAINTS" lxd:2  # 2/lxd/0 (kubernetes-master/1)
juju add-machine --constraints "$KUBEAPI_LOAD_BALANCER_CONSTRAINTS" lxd:2  # 2/lxd/1 (kubeapi-load-balancer/1)

juju add-machine dell-r610-1.maas  # 3 (kubernetes-worker/0)

juju add-machine dell-r610-2.maas  # 4 (kubernetes-worker/1)

juju add-machine hp8300-4.maas  # 5 (ceph-osd/0)
juju add-machine lxd:5  # 5/lxd/0 (ceph-mon/0)

juju add-machine hp8300-5.maas  # 6 (ceph-osd/1)
juju add-machine lxd:6  # 6/lxd/0 (ceph-mon/1)

juju add-machine hp8300-6.maas  # 7 (ceph-osd/2)
juju add-machine lxd:7  # 7/lxd/0 (ceph-mon/2)
