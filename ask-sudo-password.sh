#!/usr/bin/env bash

# We use >/dev/stderr for new line since these functions are used
# as password=$(get_password). We don't want to echo anything else in stdout.


function get_password () {
	declare __username=$1
	local __password1="1"
	local __password2="2"
	while [[ "${__password1}" != "${__password2}" ]]; do
		read -s -p "Enter password for ${__username}: " __password1 && echo " " >/dev/stderr
		read -s -p "Re-enter password for ${__username}: " __password2 && echo " " >/dev/stderr
	done
	echo ${__password1}
}

function ask_for_password_until_correct() {
	declare __username=$1
	local __password=""
	while true;	do
		__password=$(get_password "${__username}" | tail -1)
		sudo -k  # make sure to ask for password on next sudo
		if echo "${__password}" | sudo -S true; then
			echo "${__username} password is valid!" >/dev/stderr
			break
		else
			echo "Invalid ${__username} password, please enter again." >/dev/stderr
		fi
	done
	echo "${__password}"
}
